BPMNtoPetriNetPlugin
=========


Installation (command line use)
---
```sh
ant compile jar
```


Usage (command line use)
-----
```sh
java -jar build/jar/BPMNtoPetriNet.jar -h
```
It will print:
```sh
usage: BPMNtoPetriNet
 -b <file>          Input file that contains the BPMN
 -c                 Check deadlock
 -d <dir>           Specify the directory on which to perform the test
 -h                 Print this help
 -lang <language>   Specifies the language to use {en, it} (default: en)
 -p <file>          Output file that will contain the Petri net in the
                    format ll_net
 -svg               Use this option to produce an image of the Petri net
                    (SVG format)
```


Example (command line use)
-----
If you use:
```sh
java -jar build/jar/BPMNtoPetriNet.jar -b BPMNModels/xml/processes/process_1.bpmn -c -svg
```
you'll get:
```sh
-======================================================================================
Process:                 BPMNModels/xml/processes/process_1.bpmn
Petri net (svg):         /Users/marco/Desktop/test/process_1.bpmn.ll_net.svg
Cunf exit value:         0
Cna2 exit value:         0
Deadlock?                The Petri net has no deadlock!
Places mapping:          6
Transitions mapping:     6
Time execution (ms):     296
Time verification (ms):  158
-======================================================================================
```

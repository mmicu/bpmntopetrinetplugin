#!/bin/bash

if [ $# -eq 0 ]; then
    echo "Usage: $0 <ll_net file>"
    exit
fi

pep2dot $1 > $1.dot
dot -T svg < $1.dot > $1.dot.svg

#!/bin/bash

if [ $# -eq 0 ]; then
    echo "Usage: $0 <ll_net file>"
    exit
fi

if [ -f a.cuf ]; then
    rm a.cuf
fi

cunf -o a.cuf $1
cna -d a.cuf

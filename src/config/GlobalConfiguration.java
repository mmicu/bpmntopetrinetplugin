/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package config;

/**
 * Configurazioni globali del programma.
 * 
 * @author Marco
 *
 */
public class GlobalConfiguration
{
    /**
     * Se vogliamo eseguire/esportare il plugin il valore deve essere settato a "true" 
     * (utilizzata principalmente per ottenere l'input stream di un file)
     */
    public static boolean PLUGIN_ACTIVE = true;
    
    
    /**
     * Numero massimo di caratteri utilizzati per il nome delle piazze 
     */
    public static int MAX_CHARACTERS_NAME_PLACE = 50;
    
    
    /**
     * Numero massimo di caratteri utilizzati per il nome delle transizioni 
     */
    public static int MAX_CHARACTERS_NAME_TRANSITION = 50;
}

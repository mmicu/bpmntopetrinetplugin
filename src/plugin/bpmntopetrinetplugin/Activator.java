/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package plugin.bpmntopetrinetplugin;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle.
 * La classe e' stata generata automaticamente da Eclipse durante la creazione del progetto di tipo "plugin".
 */
public class Activator extends AbstractUIPlugin 
{
	/**
	 * The plug-in ID
	 */
	public static final String PLUGIN_ID = "BPMNtoPetriNetPlugin"; //$NON-NLS-1$

	
	/**
	 * The shared instance
	 */
	private static Activator plugin;
	
	
	
	/**
	 * The constructor
	 */
	public Activator () 
	{
	}

	
	/*
	 *
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start (BundleContext context) throws Exception 
	{
		super.start (context);
		plugin = this;
	}

	
	/*
	 *
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop (BundleContext context) throws Exception 
	{
		plugin = null;
		super.stop (context);
	}

	
	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault () 
	{
		return plugin;
	}

	
	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor (String path) 
	{
		return imageDescriptorFromPlugin (PLUGIN_ID, path);
	}
}

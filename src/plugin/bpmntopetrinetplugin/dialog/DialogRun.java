/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package plugin.bpmntopetrinetplugin.dialog;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;

import mapping.exceptions.ExceptionMapping;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PartInitException;
import org.xml.sax.SAXException;

import command.FormatCommand;
import command.Cna;
import command.Cunf;
import utils.FileUtils;
import utils.GenericUtils;
import utils.PluginUtils;

/**
 * Questa classe produce la finestra di dialogo nel caso in cui l'utente decida di eseguire l'analisi
 * del processo BPMN selezionato.
 * 
 * @author Marco
 */
public class DialogRun 
{
	/**
	 * PATH del file delle impostazioni 
	 */
	private static final String PATH_SETTINGS_FILE = "resources/properties/settings/settings.properties";
	
	
	/**
	 * Shell
	 */
	private Shell shlBpmntopetrinetPlugin;
	
	
	/**
	 * Oggetto "text" percorso del file BPMN
	 */
	private Text selected_bpmn_file;
	
	
	/**
	 * Valore del percorso del file BPMN
	 */
	private String selected_bpmn_file_text;
	
	
	/**
	 * "File" di lingua
	 */
	private Locale lang;
	
	
	/**
	 * Lingua selezionata dall'utente
	 */
	private String language;
	
	

	/**
	 * Apertura della finestra di dialogo
	 * @wbp.parser.entryPoint
	 */
	public void open () 
	{
		// Creazione della Shell
		this.shlBpmntopetrinetPlugin = new Shell (SWT.CLOSE | SWT.TITLE | SWT.MIN );
		// Modifica dimensione Shell
		this.shlBpmntopetrinetPlugin.setSize (720, 477);
		
		// Center a shell - http://www.java2s.com/Tutorial/Java/0280__SWT/Centerashellontheprimarymonitor.htm
		Display display = Display.getDefault ();
		Monitor primary = display.getPrimaryMonitor ();
		Rectangle bounds = primary.getBounds ();
		Rectangle rect = this.shlBpmntopetrinetPlugin.getBounds ();
		this.shlBpmntopetrinetPlugin.setLocation (
		    bounds.x + (bounds.width - rect.width) / 2, 
		    bounds.y + (bounds.height - rect.height) / 2
		);
		
		// Determino la lingua da utilizzare
		try {
			this.language = GenericUtils.getLanguage ();
			
			if (this.language.equals ("Italiano"))
				lang = new Locale ("it");
			else// if (language.equals ("English"))
				lang = new Locale ("en");
		} 
		catch (IOException e) {
			ErrorDialog ed = new ErrorDialog ();
    		
			ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
			ed.setErrorMessage ("IOException. " + GenericUtils._l (this.lang, "message") + ": " + e.getMessage ());
    		
    		ed.open (this.shlBpmntopetrinetPlugin);
		}
		
		
		// Chiamata del metodo createContents () che si occupa di creare i vari elementi
		// che compongono la finetra di dialogo
		this.createContents ();
	    
		
		// Apertura e ciclo di vita della finestra
		this.shlBpmntopetrinetPlugin.open ();
		this.shlBpmntopetrinetPlugin.layout ();
		while (!this.shlBpmntopetrinetPlugin.isDisposed ())
			if (!display.readAndDispatch ())
				display.sleep ();
	}

	
	/**
	 * Questo metodo si occupa di creare i vari elementi che compongono la finestra di dialogo
	 */
	protected void createContents () 
	{
		// Imposto il titolo della finestra
		this.shlBpmntopetrinetPlugin.setText (GenericUtils._l (this.lang, "button_dialog_run_window_name"));
		
		// Creazione della label per la checkbox per esportare la rete di Petri
		Label labelCheckBoxPetriNet = new Label (this.shlBpmntopetrinetPlugin, SWT.NONE);
		labelCheckBoxPetriNet.setBounds (10, 65, 154, 17);
		labelCheckBoxPetriNet.setText (GenericUtils._l (this.lang, "checkbox_petri_net"));
		
		// Creazione della checkbox per esportare la rete di Petri
		final Button checkboxExportPetriNet = new Button (this.shlBpmntopetrinetPlugin, SWT.CHECK);
		checkboxExportPetriNet.setBounds (170, 64, 94, 18);
		
		// Creazione della label per il path della rete di Petri
		Label labelPathPetriNet = new Label (this.shlBpmntopetrinetPlugin, SWT.NONE);
		labelPathPetriNet.setBounds (10, 95, 154, 18);
		labelPathPetriNet.setText (GenericUtils._l (this.lang, "text_path_petri_net"));
		
		// Creazione della casella "text" per il path della rete di Petri
		final Text textPathPetriNet = new Text (this.shlBpmntopetrinetPlugin, SWT.BORDER);
		textPathPetriNet.setBounds (170, 92, 439, 21);
		
		// Creazione del bottone utilizzato per selezionare il path in cui salvare la rete di Petri
		final Button savePetriNetButton = new Button (this.shlBpmntopetrinetPlugin, SWT.NONE);
		savePetriNetButton.setBounds (616, 87, 94, 31);
		savePetriNetButton.setText (GenericUtils._l (this.lang, "button_save_in"));
		
		// Creazione della label per la checkbox per esportare la rete di Petri nel formato SVG
		Label labelCheckBoxSVGPetriNet = new Label (this.shlBpmntopetrinetPlugin, SWT.NONE);
		labelCheckBoxSVGPetriNet.setBounds (10, 145, 154, 24);
		labelCheckBoxSVGPetriNet.setText (GenericUtils._l (this.lang, "checkbox_svg_petri_net"));
		
		// Creazione della label per il path della rete di Petri nel formato SVG
		Label labelSVGPetriNet = new Label (this.shlBpmntopetrinetPlugin, SWT.NONE);
		labelSVGPetriNet.setBounds (10, 175, 154, 18);
		labelSVGPetriNet.setText (GenericUtils._l (this.lang, "text_svg_petri_net"));
		
		// Creazione della casella "text" per il path della rete di Petri nel formato SVG
		final Text textPathSVGPetriNet = new Text (this.shlBpmntopetrinetPlugin, SWT.BORDER);
		textPathSVGPetriNet.setBounds (170, 172, 439, 21);
		
		// Creazione della checkbox per esportare la rete di Petri nel formato SVG
		final Button checkboxExportSVGPetriNet = new Button (this.shlBpmntopetrinetPlugin, SWT.CHECK);
		checkboxExportSVGPetriNet.setBounds (170, 144, 94, 18);
		
		// Creazione del bottone utilizzato per selezionare il path in cui salvare la rete di Petri nel formato SVG
		final Button savePetriNetSVGButton = new Button (this.shlBpmntopetrinetPlugin, SWT.NONE);
		savePetriNetSVGButton.setBounds (616, 167, 94, 31);
		savePetriNetSVGButton.setText (GenericUtils._l (this.lang, "button_save_in"));
		
		// Creazione del bottone "annulla"
		Button cancelButton = new Button (this.shlBpmntopetrinetPlugin, SWT.NONE);
		cancelButton.setBounds (515, 411, 94, 30);
		cancelButton.setText (GenericUtils._l (this.lang, "button_cancel"));
		
		// Creazione del bottone "run"
		Button runButton = new Button (this.shlBpmntopetrinetPlugin, SWT.NONE);
		runButton.setBounds (616, 411, 94, 30);
		runButton.setText (GenericUtils._l (this.lang, "button_run"));
		
		// Creazione della casella "text" per il path del BPMN
		selected_bpmn_file = new Text (this.shlBpmntopetrinetPlugin, SWT.BORDER);
		selected_bpmn_file.setBounds (170, 22, 502, 21);
		selected_bpmn_file.setText (this.selected_bpmn_file_text);
		
		// Creazione della label per il path del BPMN
		Label labelPathBPMN = new Label (this.shlBpmntopetrinetPlugin, SWT.NONE);
		labelPathBPMN.setBounds (10, 25, 154, 18);
		labelPathBPMN.setText (GenericUtils._l (this.lang, "text_path_bpmn"));
    	
    	// Creazione della label per la checkbox che controlla l'eventuale deadlock presente nel BPMN
    	Label labelCheckboxDeadlock = new Label (this.shlBpmntopetrinetPlugin, SWT.NONE);
    	labelCheckboxDeadlock.setBounds (10, 385, 154, 17);
    	labelCheckboxDeadlock.setText (GenericUtils._l (this.lang, "checkbox_deadlock"));
    	
    	// Creazione della checkbox che controlla l'eventuale deadlock presente nel BPMN
    	final Button checkboxDeadlock = new Button (this.shlBpmntopetrinetPlugin, SWT.CHECK);
    	checkboxDeadlock.setSelection (true);
    	checkboxDeadlock.setBounds (170, 384, 94, 18);
    	
    	// Creazione della label per la checkbox dell'unfolding della rete di Petri
    	Label labelCheckboxSVGUnfolding = new Label (this.shlBpmntopetrinetPlugin, SWT.NONE);
    	labelCheckboxSVGUnfolding.setBounds (10, 305, 154, 14);
    	labelCheckboxSVGUnfolding.setText (GenericUtils._l (this.lang, "checkbox_unfolding"));
    	
    	// Creazione della label per l'input text del path dell'unfolding
    	Label labelInputTextSVGUnfolding = new Label (this.shlBpmntopetrinetPlugin, SWT.NONE);
    	labelInputTextSVGUnfolding.setBounds (10, 335, 154, 14);
    	labelInputTextSVGUnfolding.setText (GenericUtils._l (this.lang, "text_path_svg_unfolding"));
    	
    	// Creazione della checkbox per produrre l'unfolding
    	Button checkboxExportSVGUnfolding = new Button (this.shlBpmntopetrinetPlugin, SWT.CHECK);
    	checkboxExportSVGUnfolding.setBounds (170, 303, 94, 18);
    	
    	// Creazione dell'input text del path dell'unfolding
    	final Text textPathSVGUnfolding = new Text (this.shlBpmntopetrinetPlugin, SWT.BORDER);
    	textPathSVGUnfolding.setEditable(false);
    	textPathSVGUnfolding.setBounds (170, 332, 439, 21);
    	
    	// Creazione del bottone per scegliere il path dell'unfolding (SVG)
    	final Button saveSVGUnfolding = new Button (this.shlBpmntopetrinetPlugin, SWT.NONE);
    	saveSVGUnfolding.setBounds (616, 330, 94, 31);
    	saveSVGUnfolding.setText (GenericUtils._l (this.lang, "button_save_in"));
        
        // Label della checkbox per esportare l'unfolding nel formato ll_net
        Label labelCheckboxUnfolding = new Label (shlBpmntopetrinetPlugin, SWT.NONE);
        labelCheckboxUnfolding.setBounds (10, 225, 154, 14);
        labelCheckboxUnfolding.setText (GenericUtils._l (this.lang, "checkbox_unfolding"));
        
        // Label dell'input text dell'unfolding nel formato ll_net
        Label labelInputTextUnfolding = new Label (shlBpmntopetrinetPlugin, SWT.NONE);
        labelInputTextUnfolding.setBounds (10, 255, 154, 14);
        labelInputTextUnfolding.setText (GenericUtils._l (this.lang, "text_path_unfolding"));
        
        // Creazione della checkbox per esportare l'unfolding nel formato ll_net
        Button checkboxExportUnfolding = new Button (shlBpmntopetrinetPlugin, SWT.CHECK);
        checkboxExportUnfolding.setBounds (170, 224, 94, 18);
        
        // Creazione dell'input text per l'unfolding (ll_net)
        final Text textPathUnfolding = new Text (shlBpmntopetrinetPlugin, SWT.BORDER);
        textPathUnfolding.setEditable (false);
        textPathUnfolding.setBounds (170, 252, 439, 21);
        
        // Creazione del bottone per scegliere il path dell'unfolding (ll_net)
        final Button saveUnfolding = new Button (shlBpmntopetrinetPlugin, SWT.NONE);
        saveUnfolding.setEnabled (false);
        saveUnfolding.setBounds (616, 248, 94, 31);
        saveUnfolding.setText (GenericUtils._l (this.lang, "button_save_in"));
        
        
        // All'apertura della finestra di dialogo, non e' possibile modificare questi campi:
        textPathPetriNet.setEditable (false);
        savePetriNetButton.setEnabled (false);
        textPathSVGPetriNet.setEditable (false);
        savePetriNetSVGButton.setEnabled (false);
        saveUnfolding.setEnabled (false);
        saveSVGUnfolding.setEnabled (false);
    	
    	
		/*
		 * Listener per la checkbox della rete di Petri (formato ll_net) (checkboxExportPetriNet)
		 */
    	checkboxExportPetriNet.addSelectionListener (new SelectionAdapter ()
		{
		    @Override
		    public void widgetSelected (SelectionEvent e)
		    {
		    	// b = true, checkbox "checked"
		    	// b = false, checkbox non "checked"
		    	boolean b = ((Button)(e.widget)).getSelection ();
		    	
		    	// E' possibile modificare il path della rete di Petri se "b = false"
		    	textPathPetriNet.setEditable (b);
		    	// E' possibile cliccare il bottone per selezionare il path della rete di Petri se "b = false"
		    	savePetriNetButton.setEnabled (b);
		    	
		    	// Se la checkbox non e' selezionata, resetto il valore della "text" che contiene il path della rete di Petri
		    	if (b == false)
		    		textPathPetriNet.setText ("");
		    }
		});
		
    	
    	/*
    	 * Listener per la checkbox della rete di Petri (formato SVG) (checkboxExportSVGPetriNet)
    	 */
		checkboxExportSVGPetriNet.addSelectionListener(new SelectionAdapter()
		{
		    @Override
		    public void widgetSelected (SelectionEvent e)
		    {
		    	// b = true, checkbox "checked"
		    	// b = false, checkbox non "checked"
		    	boolean b = ((Button)(e.widget)).getSelection ();
		    	
		    	// E' possibile modificare il path della rete di Petri se "b = false"
		    	textPathSVGPetriNet.setEditable (b);
		    	// E' possibile cliccare il bottone per selezionare il path della rete di Petri se "b = false"
		    	savePetriNetSVGButton.setEnabled (b);
		    	
		    	// Se la checkbox non e' selezionata, resetto il valore della "text" che contiene il path della rete di Petri
		    	if (b == false)
		    		textPathSVGPetriNet.setText ("");
		    }
		});
		
		
		/*
         * Listener per la checkbox della rete di Petri (formato SVG) (checkboxExportSVGPetriNet)
         */
		checkboxExportUnfolding.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected (SelectionEvent e)
            {
                // b = true, checkbox "checked"
                // b = false, checkbox non "checked"
                boolean b = ((Button)(e.widget)).getSelection ();
                
                // E' possibile modificare il path della rete di Petri se "b = false"
                textPathUnfolding.setEditable (b);
                // E' possibile cliccare il bottone per selezionare il path della rete di Petri se "b = false"
                saveUnfolding.setEnabled (b);
                
                // Se la checkbox non e' selezionata, resetto il valore della "text" che contiene il path della rete di Petri
                if (b == false)
                    textPathUnfolding.setText ("");
            }
        });
        
        
        /*
         * Listener per la checkbox della rete di Petri (formato SVG) (checkboxExportSVGPetriNet)
         */
		checkboxExportSVGUnfolding.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected (SelectionEvent e)
            {
                // b = true, checkbox "checked"
                // b = false, checkbox non "checked"
                boolean b = ((Button)(e.widget)).getSelection ();
                
                // E' possibile modificare il path della rete di Petri se "b = false"
                textPathSVGUnfolding.setEditable (b);
                // E' possibile cliccare il bottone per selezionare il path della rete di Petri se "b = false"
                saveSVGUnfolding.setEnabled (b);
                
                // Se la checkbox non e' selezionata, resetto il valore della "text" che contiene il path della rete di Petri
                if (b == false)
                    textPathSVGUnfolding.setText ("");
            }
        });
		
		
		/*
		 * Listener per il bottone "annulla" (buttonCancel)
		 */
		cancelButton.addSelectionListener (new SelectionAdapter ()
		{
		    @Override
		    public void widgetSelected (SelectionEvent e)
		    {
		    	shlBpmntopetrinetPlugin.close ();
		    }
		});
		
		
		/*
		 * Listener per il bottone utilizzato per selezionare il path in cui salvare 
		 * la rete di Petri (formato ll_net) (savePetriNetButton)
		 */
		savePetriNetButton.addSelectionListener (new SelectionAdapter ()
		{
		    @Override
		    public void widgetSelected (SelectionEvent e)
		    {
		    	// File browser
		    	String pathPetriNet = PluginUtils.openFileBrowser (shlBpmntopetrinetPlugin, "", new String [] {"*.ll_net"}, lang);
		    	
		    	textPathPetriNet.setText (pathPetriNet == null ? "" : pathPetriNet);
		    }
		});
		
		
		/*
		 * Listener per il bottone utilizzato per selezionare il path in cui salvare 
		 * la rete di Petri (formato SVG) (savePetriNetSVGButton)
		 */
		savePetriNetSVGButton.addSelectionListener (new SelectionAdapter ()
		{
		    @Override
		    public void widgetSelected (SelectionEvent e)
		    {
		    	// File browser
		    	String pathSVGPetriNet = PluginUtils.openFileBrowser (shlBpmntopetrinetPlugin, "", new String [] {"*.svg"}, lang);
		    	
		    	textPathSVGPetriNet.setText (pathSVGPetriNet == null ? "" : pathSVGPetriNet);
		    }
		});
		
		
		/*
         * Listener per il bottone utilizzato per selezionare il path
         * in cui salvare  l'unfolding (formato ll_net) (saveUnfolding)
         */
		saveUnfolding.addSelectionListener (new SelectionAdapter ()
        {
            @Override
            public void widgetSelected (SelectionEvent e)
            {
                // File browser
                String pathUnfolding = PluginUtils.openFileBrowser (shlBpmntopetrinetPlugin, "", new String [] {"*.ll_net"}, lang);
                
                textPathUnfolding.setText (pathUnfolding == null ? "" : pathUnfolding);
            }
        }); 
        
        
		/*
         * Listener per il bottone utilizzato per selezionare il path
         * in cui salvare  l'unfolding (formato SVG) (saveSVGUnfolding)
         */
		saveSVGUnfolding.addSelectionListener (new SelectionAdapter ()
        {
            @Override
            public void widgetSelected (SelectionEvent e)
            {
                // File browser
                String pathSVGUnfolding = PluginUtils.openFileBrowser (shlBpmntopetrinetPlugin, "", new String [] {"*.svg"}, lang);
                
                textPathSVGUnfolding.setText (pathSVGUnfolding == null ? "" : pathSVGUnfolding);
            }
        }); 
		
		
		/*
		 * Listener per il bottone "run" (buttonRun)
		 */
		runButton.addSelectionListener (new SelectionAdapter ()
		{
		    @Override
		    public void widgetSelected (SelectionEvent e)
		    {
		    	// checkboxExportPetriNet
		    	boolean exportPetriNet = checkboxExportPetriNet.getSelection ();
		    	
		    	// checkboxExportSVGPetriNet
		    	boolean exportSVGPetriNet = checkboxExportSVGPetriNet.getSelection ();
		    	
		    	// checkboxDeadlock
		    	boolean checkDeadlock = checkboxDeadlock.getSelection ();
		    	
		    	// Aggiorno il valore del path del BPMN
		    	selected_bpmn_file_text = selected_bpmn_file.getText ();
		    	
		    	// Controllo che il path del BPMN sia diverso da "null"
		    	if (selected_bpmn_file_text != null) {
		    		// Se l'utente ha deciso di esportare la rete di Petri (formato ll_net), controllo che il path esista
		    		if (exportPetriNet) {
			    		// Controllo che il path della file della rete di Petri esista. Ignoro il file perche', se non esiste, verra'
			    		// comunque creato dal listener
			    		
			    		// Esempio: /Users/dir/Petri_net.ll_net
				    	String pathPetriNet = textPathPetriNet.getText ();
				    	if (pathPetriNet != null) {
				    		// Esempio: /Users/dir
				    		String pathFilePetriNet = FileUtils.getPathFileByFilePath (pathPetriNet);
				    		
				    		// Controllo che il path della file della rete di Petri esista. Ignoro il file perche', se non esiste, verra'
				    		// comunque creato dal listener di "buttonRun"
				    		if (!FileUtils.pathExists (pathFilePetriNet)) {
				    			ErrorDialog ed = new ErrorDialog ();
					    		
				    			ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
				    			ed.setErrorMessage (GenericUtils._l (lang, "path_petri_net_not_exists") + "!");
					    		
					    		ed.open (shlBpmntopetrinetPlugin);
					    		
					    		return;
				    		}
				    		// Controllo che il path sia stato inserito
				    		else if (pathPetriNet == null || pathPetriNet.equals ("")) {
				    			ErrorDialog ed = new ErrorDialog ();
					    		
				    			ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
				    			ed.setErrorMessage (GenericUtils._l (lang, "path_svg_petri_net_not_valid") + "!");
					    		
					    		ed.open (shlBpmntopetrinetPlugin);
					    		
					    		return;
				    		}
				    	}
		    		}
		    		
		    		// Se l'utente ha deciso di esportare la rete di Petri (formato SVG), controllo che il path esista
		    		if (exportSVGPetriNet) {
			    		// Controllo che il path della file della rete di Petri esista. Ignoro il file perche', se non esiste, verra'
			    		// comunque creato dal listener
			    		
			    		// Esempio: /Users/dir/Petri_net.ll_net
				    	String pathSVGPetriNet = textPathSVGPetriNet.getText ();
				    	if (pathSVGPetriNet != null) {
				    		// Esempio: /Users/dir
				    		String pathFileSVGPetriNet = FileUtils.getPathFileByFilePath (pathSVGPetriNet);
				    		
				    		// Controllo che il path della file della rete di Petri esista. Ignoro il file perche', se non esiste, verra'
				    		// comunque creato dal listener di "buttonRun"
				    		if (!FileUtils.pathExists (pathFileSVGPetriNet)) {
				    			ErrorDialog ed = new ErrorDialog ();
					    		
				    			ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
				    			ed.setErrorMessage (GenericUtils._l (lang, "path_svg_petri_net_not_exists") + "!");
					    		
					    		ed.open (shlBpmntopetrinetPlugin);
					    		
					    		return;
				    		}
				    		// Controllo che il path sia stato inserito
				    		else if (pathSVGPetriNet == null || pathSVGPetriNet.equals ("")) {
				    			ErrorDialog ed = new ErrorDialog ();
					    		
				    			ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
				    			ed.setErrorMessage (GenericUtils._l (lang, "path_svg_petri_net_not_valid") + "!");
					    		
					    		ed.open (shlBpmntopetrinetPlugin);
					    		
					    		return;
				    		}
				    	}
		    		}
		    		
		    		// Se arrivo qui, se e' stato selezionato il path della rete di Petri significa che il path e' valido.
		    		// Discorso analogo per il path della rete di Petri nel formato SVG. Ora devo controllare che anche quello del
		    		// BPMN esista
		    		// Controllo che il path del BPMN esista
		    		if (FileUtils.fileExists (selected_bpmn_file_text)) {
		    			String pathPetriNet = null, pathPetriNetDot = null, pathPetriNetSVG = null;
		    			
		    			// Imposto path della rete di Petri
		    			if (exportPetriNet)
		    				pathPetriNet = textPathPetriNet.getText (); // Ho gia' controllato sopra se il path esiste
		    			else {
		    				String pathTempFile;
							
							try {
								pathTempFile = GenericUtils.getValueProperty (DialogRun.PATH_SETTINGS_FILE, "path_temp_file");
								pathPetriNet = pathTempFile + File.separator + FileUtils.getFileNameByPath (selected_bpmn_file_text) + ".ll_net";
							} 
							catch (IOException e1) {
								System.out.println (e1.getClass ().getSimpleName () + ". " + GenericUtils._l (lang, "message") + ": " + e1.getMessage ());
								
								System.exit (-1);
							}
		    			}
		    			
		    			// Imposto path della rete di Petri nei formati dot e svg
		    			if (exportSVGPetriNet) {
		    				pathPetriNetSVG = textPathSVGPetriNet.getText ();
		    				pathPetriNetDot = pathPetriNet + ".dot";
		    			}
		    			
		    			
		    			try {
		    				if (exportPetriNet || exportSVGPetriNet || checkDeadlock) {
								GenericUtils.applyMapping (
									selected_bpmn_file_text, // pathBPMN 
									pathPetriNet,            // pathPetriNet 
									pathPetriNetDot,         // pathPetriNetDOT
									pathPetriNetSVG          // pathPetriNetSVG
								);
								
								try {
									PluginUtils.addTextToView (
											GenericUtils._l (lang, "level_info"), 
											GenericUtils._l (lang, "text_path_bpmn") + ": " + selected_bpmn_file_text);
									
									if (exportPetriNet)
										PluginUtils.addTextToView (
												GenericUtils._l (lang, "level_info"), 
												GenericUtils._l (lang, "path_petri_net_pep") + ": " + pathPetriNet);
									
									if (exportSVGPetriNet)
										PluginUtils.addTextToView (
												GenericUtils._l (lang, "level_info"), 
												GenericUtils._l (lang, "path_petri_net_svg") + ": " + pathPetriNetSVG);
								} 
								catch (PartInitException e1) {
									ErrorDialog ed = new ErrorDialog ();
						    		
									ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
									ed.setErrorMessage ("PartInitException. " + GenericUtils._l (lang, "message") + ": " + e1.getMessage ());
						    		
						    		ed.open (shlBpmntopetrinetPlugin);
								}
		    				}
							
							// Eseguo analisi della rete di Petri
							if (checkDeadlock) {
								try {
									String pathCunfFile = GenericUtils.getValueProperty (DialogRun.PATH_SETTINGS_FILE, "path_temp_file") + 
											              File.separator + FileUtils.getFileNameByPath (pathPetriNet) + ".cuf";
									
									FormatCommand commandCunf = Cunf.execute (pathPetriNet, pathCunfFile);
									
									FormatCommand commandCna = Cna.execute (pathCunfFile);
									
									try {
										PluginUtils.addTextToView (
												GenericUtils._l (lang, "level_info"), 
												GenericUtils._l (lang, "cunf_exit_value") + ": " + commandCunf.getExitValue ());
										
										PluginUtils.addTextToView (
											GenericUtils._l (lang, "level_info"), 
											GenericUtils._l (lang, "cna_exit_value") + ": " + commandCna.getExitValue ());
										
										String deadlockMessage = GenericUtils.getAnswerCNA (commandCna.getOutput ());
										String trace = null;
										
										if (deadlockMessage.length () > 0 && deadlockMessage.charAt (0) == 'N')
											deadlockMessage = GenericUtils._l (lang, "deadlock_free");
										else if (deadlockMessage.length () > 0 && deadlockMessage.charAt (0) == 'Y') {
											deadlockMessage = GenericUtils._l (lang, "no_deadlock_free");
											
											trace = GenericUtils.getTraceCNA (commandCna.getOutput ());
										}
										else
											deadlockMessage = null;
										
										if (deadlockMessage != null)
											PluginUtils.addTextToView (
													GenericUtils._l (lang, "level_info"), 
													deadlockMessage + "!");
										
										if (trace != null)
											PluginUtils.addTextToView (
													GenericUtils._l (lang, "level_info"), 
													GenericUtils._l (lang, "trace") + ": " + trace);
										
										System.out.println ("\ndeadlockMessage = " + deadlockMessage);
										System.out.println ("\ntrace           = " + trace);
									} 
									catch (PartInitException e1) {
										ErrorDialog ed = new ErrorDialog ();
							    		
										ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
										ed.setErrorMessage ("PartInitException. " + GenericUtils._l (lang, "message") + ": " + e1.getMessage ());
							    		
							    		ed.open (shlBpmntopetrinetPlugin);
									}
								} 
								catch (InterruptedException e1) {
									System.out.println (e1.getClass ().getSimpleName () + ". " + GenericUtils._l (lang, "message") + ": " + e1.getMessage ());
									
									System.exit (-1);
								}
							}
						} 
		    			catch (IOException | ExceptionMapping | ParserConfigurationException | SAXException | InterruptedException e1) {
		    				System.out.println (e.getClass ().getSimpleName () + ". " + GenericUtils._l (lang, "message") + ": " + e1.getMessage ());
		    			}
		    			finally {
		    				// Chiusura della finestra
		    				shlBpmntopetrinetPlugin.close ();
		    			}
		    		}
		    		else {
		    			ErrorDialog ed = new ErrorDialog ();
			    		
		    			ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
		    			ed.setErrorMessage (GenericUtils._l (lang, "bpmn_not_exist") + "!");
			    		
			    		ed.open (shlBpmntopetrinetPlugin);
		    		}
		    	}
		    	// Il valore del path del BPMN e' "null"
		    	else {
		    		ErrorDialog ed = new ErrorDialog ();
		    		
		    		ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
		    		ed.setErrorMessage (GenericUtils._l (lang, "bpmn_not_exist") + "!");
		    		
		    		ed.open (shlBpmntopetrinetPlugin);
		    	}
		    }
		});
	}
	
	
	/**
	 * @param path
	 */
	public void setFileBPMN (String path)
	{
		this.selected_bpmn_file_text = path;
	}
}

/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package plugin.bpmntopetrinetplugin.dialog;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;

import mapping.data.structure.PEP;
import mapping.data.structure.PEP2DOT;
import mapping.exceptions.ExceptionMapping;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.ui.PartInitException;
import org.xml.sax.SAXException;

import command.Cuf2pep;
import command.Cna;
import command.FormatCommand;
import command.Cunf;
import command.Dot;
import utils.FileUtils;
import utils.GenericUtils;
import utils.PluginUtils;

/**
 * Questa classe produce la finestra di dialogo nel caso in cui l'utente decida di produrre la rete di Petri
 * del processo BPMN selezionato.
 * 
 * @author Marco
 */
public class DialogProducePetriNet 
{
    /**
     * PATH del file delle impostazioni 
     */
    private static final String PATH_SETTINGS_FILE = "resources/properties/settings/settings.properties";
    
    
    /**
     * Variabile di debug
     */
    public static final boolean debug = true;
    
    
	/**
	 * Shell
	 */
	private Shell shlBpmntopetrinetPlugin;
	
	
	/**
	 * "File" di lingua
	 */
	private Locale lang;
	
	
	/**
	 * Valore del percorso del file BPMN
	 */
	private String selected_bpmn_file_text;
	
	
	/**
	 * Lingua selezionata dall'utente
	 */
	private String language;
	
	

	/**
	 * Apertura della finestra di dialogo
	 * @wbp.parser.entryPoint
	 */
	public void open () 
	{
		// Creazione della Shell
		this.shlBpmntopetrinetPlugin = new Shell (SWT.CLOSE | SWT.TITLE | SWT.MIN);
		// Modifica dimensione Shell
		this.shlBpmntopetrinetPlugin.setSize (720, 238);
		
		// Center a shell - http://www.java2s.com/Tutorial/Java/0280__SWT/Centerashellontheprimarymonitor.htm
		Display display = Display.getDefault ();
		Monitor primary = display.getPrimaryMonitor ();
		Rectangle bounds = primary.getBounds ();
		Rectangle rect = shlBpmntopetrinetPlugin.getBounds ();
		this.shlBpmntopetrinetPlugin.setLocation (
		    bounds.x + (bounds.width - rect.width) / 2, 
		    bounds.y + (bounds.height - rect.height) / 2
		);
		
		// Determino la lingua da utilizzare
		try {
			this.language = GenericUtils.getLanguage ();
			
			if (this.language.equals ("Italiano"))
				lang = new Locale ("it");
			else// if (language.equals ("English"))
				lang = new Locale ("en");
		} 
		catch (IOException e) {
			ErrorDialog ed = new ErrorDialog ();
    		
			ed.setWindowTitle (GenericUtils._l (this.lang, "window_error_title"));
    		ed.setErrorMessage ("IOException. " + GenericUtils._l (this.lang, "message") + ": " + e.getMessage ());
    		
    		ed.open (this.shlBpmntopetrinetPlugin);
		}
		
		
		// Chiamata del metodo createContents () che si occupa di creare i vari elementi
		// che compongono la finetra di dialogo
		this.createContents ();
	    
		
		// Apertura e ciclo di vita della finestra
		this.shlBpmntopetrinetPlugin.open ();
		this.shlBpmntopetrinetPlugin.layout ();
		while (!this.shlBpmntopetrinetPlugin.isDisposed ())
			if (!display.readAndDispatch ())
				display.sleep ();
	}

	
	/**
	 * Questo metodo si occupa di creare i vari elementi che compongono la finestra di dialogo
	 */
	protected void createContents () 
	{
		// Imposto il titolo della finestra
		this.shlBpmntopetrinetPlugin.setText (GenericUtils._l (this.lang, "button_dialog_produce_petri_net_window_name"));
		
		// Creazione della label per il path del BPMN
		Label labelPathBPMN = new Label (this.shlBpmntopetrinetPlugin, SWT.NONE);
		labelPathBPMN.setBounds (10, 35, 153, 31);
		labelPathBPMN.setText (GenericUtils._l (this.lang, "text_path_bpmn"));
		
		// Creazione della casella "text" per il path del BPMN
		final Text textBPMN = new Text (this.shlBpmntopetrinetPlugin, SWT.BORDER);
		textBPMN.setBounds (169, 32, 541, 21);
		textBPMN.setText (this.selected_bpmn_file_text);
		
		// Creazione della label per il path della rete di Petri
		Label labelPetriNet = new Label (this.shlBpmntopetrinetPlugin, SWT.NONE);
		labelPetriNet.setBounds (10, 85, 153, 37);
		labelPetriNet.setText (GenericUtils._l (this.lang, "text_path_petri_net"));
		
		// Creazione della casella "text" per il path della rete di Petri
		final Text textPetriNet = new Text (this.shlBpmntopetrinetPlugin, SWT.BORDER);
		textPetriNet.setBounds (169, 82, 441, 21);
		
		// Creazione del bottone utilizzato per selezionare il path in cui salvare la rete di Petri
		Button buttonSavePetriNet = new Button (this.shlBpmntopetrinetPlugin, SWT.NONE);
		buttonSavePetriNet.setBounds (616, 77, 94, 31);
		buttonSavePetriNet.setText (GenericUtils._l (this.lang, "button_save_in"));
		
		// Creazione del bottone "annulla"
		Button buttonCancel = new Button (this.shlBpmntopetrinetPlugin, SWT.NONE);
		buttonCancel.setBounds (516, 177, 94, 31);
		buttonCancel.setText (GenericUtils._l (this.lang, "button_cancel"));
		
		// Creazione del bottone "run"
		Button buttonRun = new Button (this.shlBpmntopetrinetPlugin, SWT.NONE);
		buttonRun.setBounds (616, 177, 94, 31);
		buttonRun.setText (GenericUtils._l (this.lang, "button_run"));
		
		// Creazione della label dell'unfolding
		Label labelUnfolding = new Label (this.shlBpmntopetrinetPlugin, SWT.NONE);
		labelUnfolding.setBounds (10, 135, 153, 37);
		labelUnfolding.setText (GenericUtils._l (this.lang, "text_path_unfolding"));
		
		// Creazione della casella "text" per il path dell'unfolding
		final Text textUnfolding = new Text (this.shlBpmntopetrinetPlugin, SWT.BORDER);
		textUnfolding.setBounds (169, 133, 441, 21);
		
		// Creazione del bottone utilizzato per selezionare il path in cui salvare l'unfolding
		Button buttonSaveInUnfolding = new Button (this.shlBpmntopetrinetPlugin, SWT.NONE);
		buttonSaveInUnfolding.setBounds (616, 128, 94, 31);
		buttonSaveInUnfolding.setText (GenericUtils._l (this.lang, "button_save_in"));
		
		
		/*
		 * Listener per il bottone utilizzato per selezionare il path in cui salvare la rete di Petri (buttonSavePetriNet)
		 */
		buttonSavePetriNet.addSelectionListener (new SelectionAdapter ()
		{
		    @Override
		    public void widgetSelected (SelectionEvent e)
		    {
		    	// Esempio: /Users/dir/Petri_net.ll_net
		    	String pathPetriNet = PluginUtils.openFileBrowser (shlBpmntopetrinetPlugin, "", new String [] {"*.ll_net"}, lang);
		    	
		    	textPetriNet.setText (pathPetriNet == null ? "" : pathPetriNet);
		    }
		});
		
		
		/*
         * Listener per il bottone utilizzato per selezionare il path in cui salvare l'unfolding (buttonSavePetriNet)
         */
		buttonSaveInUnfolding.addSelectionListener (new SelectionAdapter ()
        {
            @Override
            public void widgetSelected (SelectionEvent e)
            {
                // Esempio: /Users/dir/Petri_net.ll_net
                String pathUnfolding = PluginUtils.openFileBrowser (shlBpmntopetrinetPlugin, "", new String [] {"*.ll_net"}, lang);
                
                textUnfolding.setText (pathUnfolding == null ? "" : pathUnfolding);
            }
        });
		
		
		/*
		 * Listener per il bottone "annulla" (buttonCancel)
		 */
		buttonCancel.addSelectionListener (new SelectionAdapter ()
		{
		    @Override
		    public void widgetSelected (SelectionEvent e)
		    {
		    	// Chiusura della finestra
		    	shlBpmntopetrinetPlugin.close ();
		    }
		});
		
		
		/*
		 * Listener per il bottone "run" (buttonRun)
		 */
		buttonRun.addSelectionListener (new SelectionAdapter ()
		{
		    @Override
		    public void widgetSelected (SelectionEvent e)
		    {
		        String errorMessage = "";
		        
		    	// Controllo che il path della rete di Petri sia stato inserito (sotto controllo che esiste)
		    	if (textPetriNet.getText () == null || textPetriNet.getText ().equals (""))
		    	    errorMessage += GenericUtils._l (lang, "path_petri_net_not_exists") + "!";
		    	
		    	// Controllo che il path dell'unfolding sia stato inserito (sotto controllo che esiste)
		    	if (textUnfolding.getText () == null || textUnfolding.getText ().equals ("")) {
		    	    errorMessage += errorMessage.length () > 0 ? "\n" : "";
		    	    errorMessage += GenericUtils._l (lang, "path_unfolding_not_exists") + "!";
		    	}
		    	
		    	if (errorMessage.length () > 0) {
                    ErrorDialog ed = new ErrorDialog ();

                    ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
                    ed.setErrorMessage (errorMessage);
                    
                    ed.open (shlBpmntopetrinetPlugin);
                    
                    return;
		    	}
			    
		    	// Aggiorno il valore del path del BPMN
		    	selected_bpmn_file_text = textBPMN.getText ();
		    	
		    	// Controllo che il path del BPMN sia diverso da "null"
		    	if (selected_bpmn_file_text != null) {
		    		// Controllo che il path della file della rete di Petri esista. Ignoro il file perche', se non esiste, verra'
		    		// comunque creato dal listener
		    		
		    		// Esempio: /Users/dir/Petri_net.ll_net
			    	String pathPetriNet = textPetriNet.getText ();
			    	String pathUnfolding = textUnfolding.getText ();
			    	
			    	if (pathPetriNet != null && pathUnfolding != null) {
			    		// Esempio: /Users/dir
			    		String pathFilePetriNet = FileUtils.getPathFileByFilePath (pathPetriNet);
			    		String pathFileUnfolding = FileUtils.getPathFileByFilePath (pathUnfolding);
			    		
			    		// Controllo che il path della file della rete di Petri esista. Ignoro il file perche', se non esiste, verra'
			    		// comunque creato dal listener di "buttonRun". Stesso discorso per il percorso dell'unfolding
			    		errorMessage = "";
			    		
			    		if (!FileUtils.pathExists (pathFilePetriNet))
			    		    errorMessage += (GenericUtils._l (lang, "path_petri_net_not_exists") + "!");
			    		
			    		if (!FileUtils.pathExists (pathFileUnfolding)) {
			    		    errorMessage += errorMessage.length () > 0 ? "\n" : "";
                            errorMessage += (GenericUtils._l (lang, "path_unfolding_not_exists") + "!");
			    		}
			    		
			    		if (errorMessage.length () > 0) {
			    			ErrorDialog ed = new ErrorDialog ();
				    		
			    			ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
			    			ed.setErrorMessage (GenericUtils._l (lang, "path_petri_net_not_exists") + "!");
				    		
				    		ed.open (shlBpmntopetrinetPlugin);
				    		
				    		return;
			    		}
			    		// Il path della rete di Petri e dell'unfolding esistono!
			    		
			    		// Controllo che il path del BPMN esista, in caso positivo eseguo il mapping
			    		if (FileUtils.fileExists (selected_bpmn_file_text)) {
			    			// A questo punto il path della rete di Petri e quello del BPMN esistono.
			    			// Posso applicare il mapping
			    			try {
			    			    // Applico il mapping
								GenericUtils.applyMapping (
									selected_bpmn_file_text, // pathBPMN 
									pathPetriNet,            // pathPetriNet 
									pathPetriNet + ".dot",   // pathPetriNetDOT
									pathPetriNet + ".svg"    // pathPetriNetSVG
								);
								
								// Genero l'ulfolding della rete di Petri
								String pathCunfFile = GenericUtils.getValueProperty (DialogProducePetriNet.PATH_SETTINGS_FILE, "path_temp_file") + 
                                        File.separator + FileUtils.getFileNameByPath (pathPetriNet) + ".cuf";
                  
								// Genero il file .cuf
								FormatCommand commandCunf = Cunf.execute (pathPetriNet, pathCunfFile);
								
								// Eseguo CNA per ottenere l'eventuale trace di deadlock
								FormatCommand commandCna = Cna.execute (pathCunfFile);
								String trace       = GenericUtils.getTraceCNA (commandCna.getOutput ());
								
								// Se c'e' una traccia di deadlock, ottengo gli elementi di questa traccia
								ArrayList<String> elementsTrace = new ArrayList<String> ();
								if (trace.length () > 0)
								    elementsTrace = GenericUtils.getElementsTrace (trace);
								
								// Genero il file .ll_net dal file .cuf => pathUnfolding rete di Petri nel formato ll_net
								FormatCommand commandCuf2Pep = Cuf2pep.execute (pathCunfFile, pathUnfolding);
								
								String pathPetriNetUnfoldingDOT = pathUnfolding + ".dot";
								String pathPetriNetUnfoldingSVG = pathUnfolding + ".svg";
								PEP petriNetUnfolding  = GenericUtils.getPEPformatFromPEPfile (pathUnfolding);
								
								// File .dot dell'unfolding della rete di Petri
								FileUtils.writeIntoFile (
								        pathPetriNetUnfoldingDOT, 
								        PEP2DOT.getDotFormat (petriNetUnfolding, elementsTrace)
								);
								
								// Ottengo l'svg dell'unfolding
								FormatCommand commandDot = Dot.execute (pathPetriNetUnfoldingDOT, pathPetriNetUnfoldingSVG);
								
								if (DialogProducePetriNet.debug) {
                                    System.out.println ("Execute Cunf for unfolding. Exit value: (" + commandCunf.getExitValue () + ")");
                                    System.out.println ("Execute cuf2pep for unfolding. Exit value: (" + commandCuf2Pep.getExitValue () + ")");
                                    System.out.println ("Execute dot for unfolding. Exit value: (" + commandDot.getExitValue () + ")");
                                    System.out.println ("Execute dot for unfolding. Output: (" + commandDot.getOutput () + ")");
                                    System.out.println ("Execute dot for unfolding. Command: (" + commandDot.getCommand () + ")");
                                    System.out.println ("Execute dot for unfolding. Error: (" + commandDot.getError () + ")");
                                }
								
								
								try {
									PluginUtils.addTextToView (
									        GenericUtils._l (lang, "level_info"), 
									        GenericUtils._l (lang, "text_path_bpmn") + ": " + selected_bpmn_file_text);
									
									PluginUtils.addTextToView (
									        GenericUtils._l (lang, "level_info"), 
									        GenericUtils._l (lang, "path_petri_net_pep") + ": " + pathPetriNet);
									
									PluginUtils.addTextToView (
									        GenericUtils._l (lang, "level_info"), 
									        GenericUtils._l (lang, "path_petri_net_svg") + ": " + pathPetriNet + ".svg");
								} 
								catch (PartInitException e1) {
									ErrorDialog ed = new ErrorDialog ();
						    		
									ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
									ed.setErrorMessage ("PartInitException. " + GenericUtils._l (lang, "message") + ": " + e1.getMessage ());
						    		
						    		ed.open (shlBpmntopetrinetPlugin);
								}
								
								/*
								if (DialogProducePetriNet.debug)
								    if (resDot != null && resDot.size () >= 3)
								        System.out.println ("Execute dot exit value (" + resDot.get (2) + ")");
								        */
							} 
			    			catch (IOException | ExceptionMapping | ParserConfigurationException | SAXException | InterruptedException e1) {
			    				System.out.println (e.getClass ().getSimpleName () + ". " + GenericUtils._l (lang, "message") + ": " + e1.getMessage ());
			    			}
			    			finally {
			    				// Chiusura della finestra
			    				shlBpmntopetrinetPlugin.close ();
			    			}
			    		}
			    		// Il path del BPMN non esiste
			    		else {
			    			ErrorDialog ed = new ErrorDialog ();
				    		
			    			ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
			    			ed.setErrorMessage (GenericUtils._l (lang, "bpmn_not_exist") + "!");
				    		
				    		ed.open (shlBpmntopetrinetPlugin);
			    		}
		    		}
		    	}
		    	// Il valore del path del BPMN e' "null"
		    	else {
		    		ErrorDialog ed = new ErrorDialog ();
		    		
		    		ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
		    		ed.setErrorMessage (GenericUtils._l (lang, "bpmn_not_exist") + "!");
		    		
		    		ed.open (shlBpmntopetrinetPlugin);
		    	}
	    	}
		});
	}
	
	
	/**
	 * Modifica del valore dell'attributo selected_bpmn_file_text
	 * 
	 * @param path Nuovo valore dell'attributo selected_bpmn_file_text
	 */
	public void setFileBPMN (String path)
	{
		this.selected_bpmn_file_text = path;
	}
}

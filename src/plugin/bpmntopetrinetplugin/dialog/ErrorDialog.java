/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package plugin.bpmntopetrinetplugin.dialog;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;

/**
 * Questa classe produce la finestra di dialogo nel caso in cui il plugin genera un errore.
 * 
 * @author Marco
 */
public class ErrorDialog 
{
	/**
	 * Titolo della finestra
	 */
	private String windowTitle;
	
	
	/**
	 * Messaggio di errore
	 */
	private String errorMessage;
	
	
	
	/**
	 * Costruttore di default 
	 */
	public ErrorDialog ()
	{
		// Imposto il titolo di default per la finestra
		this.windowTitle  = "BPMNtoPetriNet Plugin ~ Error";
		// Imposto il messaggio di default per la finestra
		this.errorMessage = "Error!";
	}
	
	
	/**
	 * Costruttore
	 * 
	 * @param windowTitle  Titolo della finestra
	 * @param errorMessage Messaggio di errore
	 */
	public ErrorDialog (String windowTitle, String errorMessage)
	{
		this.windowTitle  = windowTitle;
		this.errorMessage = errorMessage;
	}
	
	
	/**
	 * Apertura della finestra di errore
	 * 
	 * @param shell Shell "parent"
	 */
	public void open (Shell shell) 
	{
		new MessageDialog (shell, this.windowTitle, null, this.errorMessage, MessageDialog.ERROR, new String[] { "Ok" }, 0).open ();
	}
	
	
	/**
	 * Modifica del valore dell'attributo errorMessage
	 * 
	 * @param errorMessage Nuovo valore dell'attributo errorMessage
	 */
	public void setErrorMessage (String errorMessage)
	{
		this.errorMessage = errorMessage;
	}
	
	
	/**
	 * Modifica del valore dell'attributo windowTitle
	 * 
	 * @param windowTitle Nuovo valore dell'attributo windowTitle
	 */
	public void setWindowTitle (String windowTitle)
	{
		this.windowTitle = windowTitle;
	}
}

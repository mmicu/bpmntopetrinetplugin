/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package plugin.bpmntopetrinetplugin.dialog;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.util.Locale;
import java.util.Properties;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Text;

import utils.FileUtils;
import utils.PluginUtils;
import utils.GenericUtils;

/**
 * Questa classe produce la finestra di dialogo nel caso in cui l'utente decida di modificare
 * le impostazioni del plugin.
 * 
 * @author Marco
 */
public class DialogSettings 
{
    /**
	 * Path del file "properties" denominato "settings.properties"
	 */
	private static final String PATH_SETTINGS_FILE = "resources/properties/settings/settings.properties";
	
	
	/**
	 * Shell
	 */
	private Shell shlBpmntopetrinetPlugin;
	
	
	/**
	 * Lingue disponibili per il plugin
	 */
	private String[] languages = { "English", "Italiano" };
	
	
	/**
	 * "File" di lingua
	 */
	private Locale lang;
	
	
	/**
	 * Lingua selezionata dall'utente
	 */
	private String language;

	
	
	/**
	 * Apertura della finestra di dialogo
	 * @wbp.parser.entryPoint
	 */
	public void open () 
	{
		// Creazione della Shell
		this.shlBpmntopetrinetPlugin = new Shell ();//(SWT.CLOSE | SWT.TITLE); //| SWT.MIN );
		// Modifica dimensione Shell
		this.shlBpmntopetrinetPlugin.setSize (595, 366);
		
		// Center a shell - http://www.java2s.com/Tutorial/Java/0280__SWT/Centerashellontheprimarymonitor.htm
		Display display = Display.getDefault ();
		Monitor primary = display.getPrimaryMonitor ();
		Rectangle bounds = primary.getBounds ();
		Rectangle rect = this.shlBpmntopetrinetPlugin.getBounds ();
		this.shlBpmntopetrinetPlugin.setLocation (
		    bounds.x + (bounds.width - rect.width) / 2, 
		    bounds.y + (bounds.height - rect.height) / 2
		);
		
		// Determino la lingua da utilizzare
		try {
			this.language = GenericUtils.getLanguage ();
			
			if (this.language.equals ("Italiano"))
				lang = new Locale ("it");
			else// if (language.equals ("English"))
				lang = new Locale ("en");
		} 
		catch (IOException e) {
			ErrorDialog ed = new ErrorDialog ();
    		
			ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
			ed.setErrorMessage ("IOException. " + GenericUtils._l (this.lang, "message") + ": " + e.getMessage ());
    		
    		ed.open (this.shlBpmntopetrinetPlugin);
		}

		
		// Chiamata del metodo createContents () che si occupa di creare i vari elementi
		// che compongono la finetra di dialogo
	    createContents ();
	    
	    
		// Apertura e ciclo di vita della finestra
	    this.shlBpmntopetrinetPlugin.open ();
	    this.shlBpmntopetrinetPlugin.layout ();
		while (!this.shlBpmntopetrinetPlugin.isDisposed ())
			if (!display.readAndDispatch ())
				display.sleep ();
	}

	
	/**
	 * Create contents of the window.
	 */
	protected void createContents () 
	{
		// Imposto il titolo della finestra
		this.shlBpmntopetrinetPlugin.setText (GenericUtils._l (this.lang, "button_dialog_settings_window_name"));
		
		// Creazione della label per la combo (select) delle lingue disponibili per il plugin
		Label labelComboLanguage = new Label (this.shlBpmntopetrinetPlugin, SWT.NONE);
		labelComboLanguage.setBounds (20, 30, 90, 29);
		labelComboLanguage.setText (GenericUtils._l (this.lang, "combo_language"));
		
		// Creazione della combo (select) delle lingue disponibili per il plugin
		final CCombo comboLanguage = new CCombo (this.shlBpmntopetrinetPlugin, SWT.BORDER);
		comboLanguage.setEditable (false);
		comboLanguage.setBounds (127, 26, 151, 21);
		// Imposto il valore di ogni elemento della combo (select)
		for (int k = 0; k < languages.length; k++)
			comboLanguage.add (languages[k]);
		
		// Creazione del bottone "annulla"
		Button buttonCancel = new Button (this.shlBpmntopetrinetPlugin, SWT.NONE);
		buttonCancel.setBounds (389, 302, 94, 30);
		buttonCancel.setText (GenericUtils._l (this.lang, "button_cancel"));
		
		// Creazione del bottone "salva"
		Button buttonSave = new Button (this.shlBpmntopetrinetPlugin, SWT.NONE);
		buttonSave.setBounds (489, 302, 94, 30);
		buttonSave.setText (GenericUtils._l (this.lang, "button_save"));
		
		// Creazione della label per il path dell'eseguibile di cunf
		Label labelPathCunf = new Label (this.shlBpmntopetrinetPlugin, SWT.NONE);
		labelPathCunf.setBounds (20, 80, 90, 18);
		labelPathCunf.setText (GenericUtils._l (this.lang, "text_path_cunf"));
		
		// Creazione della text per il path dell'eseguibile di cunf
		final Text textCunf = new Text (this.shlBpmntopetrinetPlugin, SWT.BORDER);
		textCunf.setBounds (127, 77, 458, 21);
		
		// Creazione della label per il path dell'eseguibile di dot
		Label labelPathDot = new Label (this.shlBpmntopetrinetPlugin, SWT.NONE);
		labelPathDot.setBounds (20, 180, 90, 29);
		labelPathDot.setText (GenericUtils._l (this.lang, "text_path_dot"));
	    
		// Creazione della text per il path dell'eseguibile di dot
	    final Text textDot = new Text (this.shlBpmntopetrinetPlugin, SWT.BORDER);
	    textDot.setBounds (127, 175, 458, 21);
	    
	    // Creazione della label per il path dell'eseguibile di cna
	    Label labelPathCna = new Label (this.shlBpmntopetrinetPlugin, SWT.NONE);
	    labelPathCna.setBounds (20, 130, 90, 34);
	    labelPathCna.setText (GenericUtils._l (this.lang, "text_path_cna"));
	    
	    // Creazione della text per il path dell'eseguibile di cna
	    final Text textCna = new Text (this.shlBpmntopetrinetPlugin, SWT.BORDER);
	    textCna.setBounds (127, 127, 458, 21);
	    
	    // Creazione della label per il path dei file temporanei generati dal plugin
	    Label labelTempFile = new Label (this.shlBpmntopetrinetPlugin, SWT.NONE);
	    labelTempFile.setBounds (20, 220, 101, 29);
	    labelTempFile.setText (GenericUtils._l (this.lang, "text_temp_file"));
	    
	    // Creazione della text per il path dei file temporanei generati dal plugin
	    final Text textTempFile = new Text (this.shlBpmntopetrinetPlugin, SWT.BORDER);
	    textTempFile.setBounds (127, 217, 458, 21);
	    
	    // Creazione della label per il path dei file temporanei generati dal plugin
	    Label labelCuf2Pep = new Label (this.shlBpmntopetrinetPlugin, SWT.NONE);
	    labelCuf2Pep.setBounds (20, 260, 101, 14);
	    labelCuf2Pep.setText (GenericUtils._l (this.lang, "text_cuf2pep"));
	    
	    // Creazione della text per il path dell'eseguibile cuf2pep
	    final Text textCuf2Pep = new Text (this.shlBpmntopetrinetPlugin, SWT.BORDER);
	    textCuf2Pep.setBounds (127, 255, 458, 21);

		
	    // Imposto il valore di ogni text, in base al valore letto nel file di "properties" denominato "settings.properties"
		try {
			textCunf.setText (GenericUtils.getValueProperty (DialogSettings.PATH_SETTINGS_FILE, "path_cunf"));
			textDot.setText (GenericUtils.getValueProperty (DialogSettings.PATH_SETTINGS_FILE, "path_dot"));
			textCna.setText (GenericUtils.getValueProperty (DialogSettings.PATH_SETTINGS_FILE, "path_cna"));
			textTempFile.setText (GenericUtils.getValueProperty (DialogSettings.PATH_SETTINGS_FILE, "path_temp_file"));
			textCuf2Pep.setText (GenericUtils.getValueProperty (DialogSettings.PATH_SETTINGS_FILE, "path_cuf2pep"));
			
			for (int k = 0; k < languages.length; k++)
				if (languages[k].equals (this.language))
					comboLanguage.select (k);
		} 
		catch (IOException e) {
			ErrorDialog ed = new ErrorDialog ();
    		
			ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
			ed.setErrorMessage ("IOException. " + GenericUtils._l (this.lang, "message") + ": " + e.getMessage ());
    		
    		ed.open (shlBpmntopetrinetPlugin);
		}
		
		
		/*
		 * Listener per il bottone "annulla" (buttonCancel)
		 */
		buttonCancel.addSelectionListener (new SelectionAdapter ()
		{
		    @Override
		    public void widgetSelected (SelectionEvent e)
		    {
		    	shlBpmntopetrinetPlugin.close ();
		    }
		});
		
		
		/*
		 * Listener per il bottone "salva" (buttonSave)
		 */
		buttonSave.addSelectionListener (new SelectionAdapter ()
		{
		    @Override
		    public void widgetSelected (SelectionEvent e)
		    {
		    	// Determino il valore di ogni text
		    	String valueLanguage     = comboLanguage.getText ();
		    	String valuePathCunf     = textCunf.getText ();
		    	String valuePathDot      = textDot.getText ();
		    	String valuePathCna      = textCna.getText ();
		    	String valuePathTempFile = textTempFile.getText ();
		    	String valueCuf2Pep      = textTempFile.getText ();
		    	
		    	// Variabile contenente i messaggi di errore.
		    	String errorMessage = "";
		    	
		    	// Se il file eseguibile o il path dei file temporanei non esiste, concateno il messaggio di errore
		    	// alla variabile errorMessage
		    	errorMessage += (!FileUtils.fileExists (valuePathCunf))     ? GenericUtils._l (lang, "cunf_not_exist") + "!\n" : "";
		    	errorMessage += (!FileUtils.fileExists (valuePathDot))      ? GenericUtils._l (lang, "dot_not_exist") + "!\n" : "";
		    	errorMessage += (!FileUtils.fileExists (valuePathCna))      ? GenericUtils._l (lang, "cna_not_exist") + "!\n" : "";
		    	errorMessage += (!FileUtils.pathExists (valuePathTempFile)) ? GenericUtils._l (lang, "temp_not_exist") + "!\n" : "";
		    	errorMessage += (!FileUtils.pathExists (valueCuf2Pep))      ? GenericUtils._l (lang, "cuf2pep_not_exist") + "!\n" : "";
		    	
		    	// Se la lunghezza del messaggio di errore e' maggiore di 0, significa che c'e' almeno un errore
		    	// quindi genero la finestra di dialogo di errore
		    	if (errorMessage.length () > 0) {
		    		ErrorDialog ed = new ErrorDialog ();
		    		
		    		ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
		    		ed.setErrorMessage (errorMessage);
		    		
		    		ed.open (shlBpmntopetrinetPlugin);
		    		
		    		return;
		    	}
		    	
		    	
		    	// I file eseguibili e la cartella dei file temporanei esistono. Quindi posso modificare il file
		    	// di "properties" denominato "settings.properties"
		    	Properties prop = new Properties ();
		    	OutputStream output;
		    	
		    	try {
					output = PluginUtils.getOutputStreamFromPathFile (DialogSettings.PATH_SETTINGS_FILE);
					
					prop.setProperty ("language", valueLanguage);
		    		prop.setProperty ("path_cunf", valuePathCunf);
		    		prop.setProperty ("path_dot", valuePathDot);
		    		prop.setProperty ("path_cna", valuePathCna);
		    		prop.setProperty ("path_temp_file", valuePathTempFile);
		    		prop.setProperty ("path_cuf2pep", valuePathTempFile);
		     
		    		prop.store (output, null);
		    		
		    		shlBpmntopetrinetPlugin.close ();
				} 
		    	catch (URISyntaxException e1) {
		    		ErrorDialog ed = new ErrorDialog ();
		    		
		    		ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
		    		ed.setErrorMessage ("URISyntaxException. " + GenericUtils._l (lang, "message") + ": " + e1.getMessage ());
		    		
		    		ed.open (shlBpmntopetrinetPlugin);
				} 
		    	catch (IOException e1) {
		    		ErrorDialog ed = new ErrorDialog ();
		    		
		    		ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
		    		ed.setErrorMessage ("IOException. " + GenericUtils._l (lang, "message") + ": " + e1.getMessage ());
		    		
		    		ed.open (shlBpmntopetrinetPlugin);
				}
		    }
		});
	}
}

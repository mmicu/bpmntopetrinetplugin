/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package plugin.bpmntopetrinetplugin.handlers;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;

import plugin.bpmntopetrinetplugin.dialog.DialogRun;
import plugin.bpmntopetrinetplugin.dialog.ErrorDialog;
import utils.FileUtils;
import utils.GenericUtils;

/**
 * Handler per l'analisi del BPMN
 * 
 * @author Marco
 */
public class RunHandler extends AbstractHandler 
{
	/*
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute (ExecutionEvent event) throws ExecutionException 
	{
		IWorkbench workbench      = PlatformUI.getWorkbench ();
		IWorkbenchWindow window   = workbench == null ? null : workbench.getActiveWorkbenchWindow ();
		IWorkbenchPage activePage = window == null ? null : window.getActivePage ();
		IEditorPart editor        = activePage == null ? null : activePage.getActiveEditor ();
		IEditorInput input        = editor == null ? null : editor.getEditorInput ();
		IPath path                = input instanceof FileEditorInput ? ((FileEditorInput) input).getPath (): null;
		Locale lang               = null;
		String language;
		
		// Determino la lingua da utilizzare
		try {
			language = GenericUtils.getLanguage ();
			
			if (language.equals ("Italiano"))
				lang = new Locale ("it");
			else// if (language.equals ("English"))
				lang = new Locale ("en");
		} 
		catch (IOException e) {
			// Non posso utilizzare _l, in quanto lang = null
			ErrorDialog ed = new ErrorDialog ();
    		
			ed.setErrorMessage ("IOException. Message: " + e.getMessage ());
    		
    		ed.open (window.getShell ());
		}
		
		// Controllo che le impostazioni siano corrette
	    boolean correctSettings;
	    
    	try {
			correctSettings = GenericUtils.correctSettings ();
			
			if (!correctSettings) {
				ErrorDialog ed = new ErrorDialog ();
	    		
				ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
				ed.setErrorMessage (GenericUtils._l (lang, "incorrect_settings_analyze_bpmn") + "!");
	    		
	    		ed.open (window.getShell ());
	    		
	    		return null;
			}
		} 
    	catch (IOException e) {
			ErrorDialog ed = new ErrorDialog ();
    		
			ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
			ed.setErrorMessage ("Exception. " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
    		
    		ed.open (window.getShell ());
		}
    	
    	IWorkbenchPart workbenchPart = PlatformUI.getWorkbench ().getActiveWorkbenchWindow ().getActivePage ().getActivePart (); 
    	IFile file = (IFile) workbenchPart.getSite ().getPage ().getActiveEditor ().getEditorInput ().getAdapter (IFile.class);
    	
    	/*
    	if (file != null)
    	    System.out.println ("FILE=" + file);
    	else
            System.out.println ("FILE=null");
    	
    	if (path != null)
            System.out.println ("PATH=" + path);
        else
            System.out.println ("PATH=null");
            */
    	
    	//
    	//
    	//System.out.println ("PATH:  " + path == null ? "null!" : path.toOSString ());
		//
    	//
    	
    	
		if (path == null) {
			ErrorDialog ed = new ErrorDialog ();
    		
			ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
    		ed.setErrorMessage (GenericUtils._l (lang, "message_error") + "! " + GenericUtils._l (lang, "select_bpmn_file") + "!");
    		
    		ed.open (window.getShell ());
		}
		else {
			try {
				DialogRun dialog = new DialogRun ();
				File f           = new File (path.toOSString ());
				
				// Se il file selezionato non ha estensione "bpmn", genero la finestra di dialogo di errore
				if (!FileUtils.getExtension (f.getName ()).toLowerCase ().equals ("bpmn")) {
					ErrorDialog ed = new ErrorDialog ();
		    		
					ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
		    		ed.setErrorMessage (GenericUtils._l (lang, "message_error") + "! " + GenericUtils._l (lang, "select_bpmn_file") + "!");
		    		
		    		ed.open (window.getShell ());
				}
				else {
					// Imposto il file BPMN nella text della finestra di dialogo
					dialog.setFileBPMN (path.toOSString ());
					
					// Apro la finestra di dialogo per l'analisi del BPMN (DialogRun)
					dialog.open ();
				}
			} 
			catch (Exception e) {
				ErrorDialog ed = new ErrorDialog ();
	    		
				ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
				ed.setErrorMessage ("Exception. " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
	    		
	    		ed.open (window.getShell ());
			}
		}
		
		
		// Forzo l'apertura della view. Durante la fase di testing, per aprire la view bisognava accedere alla "text"
		// "Quick Access" di Eclipse e cercare il nome della view.
		// In questo modo evito di dover far eseguire questo procedimento all'utente
		try {
			PlatformUI.getWorkbench ().getActiveWorkbenchWindow ().getActivePage ().showView ("BPMNtoPetriNetPlugin.BPMNtoPetriNetPluginView");
		} 
		catch (PartInitException e) {
			ErrorDialog ed = new ErrorDialog ();
    		
			ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
			ed.setErrorMessage ("PartInitException. " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
    		
    		ed.open (window.getShell ());
		}
		
		return null;
	}
}

/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package plugin.bpmntopetrinetplugin.handlers;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;

import plugin.bpmntopetrinetplugin.dialog.DialogProducePetriNet;
import plugin.bpmntopetrinetplugin.dialog.ErrorDialog;
import utils.FileUtils;
import utils.GenericUtils;

/**
 * Handler per generare la rete di Petri.
 * 
 * @author Marco
 */
public class ProducePetriNetHandler extends AbstractHandler 
{
	/*
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute (ExecutionEvent event) throws ExecutionException 
	{
		IWorkbench workbench      = PlatformUI.getWorkbench ();
		IWorkbenchWindow window   = workbench == null ? null : workbench.getActiveWorkbenchWindow ();
		IWorkbenchPage activePage = window == null ? null : window.getActivePage ();
		IEditorPart editor        = activePage == null ? null : activePage.getActiveEditor ();
		IEditorInput input        = editor == null ? null : editor.getEditorInput ();
		IPath path                = input instanceof FileEditorInput ? ((FileEditorInput) input).getPath (): null;
		Locale lang               = null;
		String language;
		
		// Determino la lingua da utilizzare
		try {
			language = GenericUtils.getLanguage ();
			
			if (language.equals ("Italiano"))
				lang = new Locale ("it");
			else// if (language.equals ("English"))
				lang = new Locale ("en");
		} 
		catch (IOException e) {
			// Non posso utilizzare _l, in quanto lang = null
			ErrorDialog ed = new ErrorDialog ();
    		
			ed.setErrorMessage ("IOException. Message: " + e.getMessage ());
    		
    		ed.open (window.getShell ());
		}
		
		// Controllo che le impostazioni siano corrette
	    boolean correctSettings;
	    
    	try {
    		// Visto che non utilizzo Cunf e Cna per produrre la rete di Petri, controllo soltato
    		// se esiste il path di dot e quello dei file temporanei
			correctSettings = GenericUtils.existsPathDot () && GenericUtils.existsPathTempFile ();
			
			if (!correctSettings) {
				ErrorDialog ed = new ErrorDialog ();
	    		
				ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
				ed.setErrorMessage (GenericUtils._l (lang, "incorrect_settings_produce_petri_net") + "!");
	    		
	    		ed.open (window.getShell ());
	    		
	    		return null;
			}
		} 
    	catch (IOException e) {
			ErrorDialog ed = new ErrorDialog ();
    		
			ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
			ed.setErrorMessage ("Exception. " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
    		
    		ed.open (window.getShell ());
		}
    	
		
		if (path == null) {
			ErrorDialog ed = new ErrorDialog ();
    		
			ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
    		ed.setErrorMessage (GenericUtils._l (lang, "message_error") + "! " + GenericUtils._l (lang, "select_bpmn_file") + "!");
    		
    		ed.open (window.getShell ());
		}
		else {
			try {
				DialogProducePetriNet dialog = new DialogProducePetriNet ();
				File f                       = new File (path.toOSString ());
				
				// Se il file selezionato non ha estensione "bpmn", genero la finestra di dialogo di errore
				if (!FileUtils.getExtension (f.getName ()).toLowerCase ().equals ("bpmn")) {
					ErrorDialog ed = new ErrorDialog ();
		    		
					ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
		    		ed.setErrorMessage (GenericUtils._l (lang, "message_error") + "! " + GenericUtils._l (lang, "select_bpmn_file") + "!");
		    		
		    		ed.open (window.getShell ());
				}
				else {
					// Imposto il file BPMN nella text della finestra di dialogo
					dialog.setFileBPMN (path.toOSString ());
					
					// Apro la finestra di dialogo per l'analisi del BPMN (DialogProducePetriNet)
					dialog.open ();
				}
			} 
			catch (Exception e) {
				ErrorDialog ed = new ErrorDialog ();
	    		
				ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
				ed.setErrorMessage ("Exception. " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
	    		
	    		ed.open (window.getShell ());
			}
		}
		
		
		// Forzo l'apertura della view. Durante la fase di testing, per aprire la view bisognava accedere alla "text"
		// "Quick Access" di Eclipse e cercare il nome della view.
		// In questo modo evito di dover far eseguire questo procedimento all'utente
		try {
			PlatformUI.getWorkbench ().getActiveWorkbenchWindow ().getActivePage ().showView ("BPMNtoPetriNetPlugin.BPMNtoPetriNetPluginView");
		} 
		catch (PartInitException e) {
			ErrorDialog ed = new ErrorDialog ();
    		
			ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
			ed.setErrorMessage ("PartInitException. " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
    		
    		ed.open (window.getShell ());
		}
		
		return null;
	}
}

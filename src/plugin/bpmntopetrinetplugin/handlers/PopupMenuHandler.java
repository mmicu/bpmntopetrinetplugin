/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package plugin.bpmntopetrinetplugin.handlers;

import java.io.IOException;
import java.util.Locale;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IResource;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;

import utils.FileUtils;
import utils.GenericUtils;
import plugin.bpmntopetrinetplugin.dialog.DialogRun;
import plugin.bpmntopetrinetplugin.dialog.ErrorDialog;
 
/**
 * Handler per il PopupMenu (Package Explorer-->Tasto destro su file XML).
 * 
 * @author Marco
 */
public class PopupMenuHandler extends AbstractHandler 
{
	/*
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute (ExecutionEvent event) throws ExecutionException 
	{
		IWorkbench workbench    = PlatformUI.getWorkbench ();
		IWorkbenchWindow window = workbench == null ? null : workbench.getActiveWorkbenchWindow ();
		Locale lang             = null;
		String language;
		
		// Determino la lingua da utilizzare
		try {
			language = GenericUtils.getLanguage ();
			
			if (language.equals ("Italiano"))
				lang = new Locale ("it");
			else// if (language.equals ("English"))
				lang = new Locale ("en");
		} 
		catch (IOException e) {
			// Non posso utilizzare _l, in quanto lang = null
			ErrorDialog ed = new ErrorDialog ();
    		
			ed.setErrorMessage ("IOException. Message: " + e.getMessage ());
    		
    		ed.open (window.getShell ());
		}
		
		Shell shell                    = HandlerUtil.getActiveShell (event);
	    ISelection sel                 = HandlerUtil.getActiveMenuSelection (event);
	    IStructuredSelection selection = (IStructuredSelection) sel;
	    
	    // Controllo che le impostazioni siano corrette
	    boolean correctSettings;
	    
    	try {
			correctSettings = GenericUtils.correctSettings ();
			
			if (!correctSettings) {
				ErrorDialog ed = new ErrorDialog ();
	    		
				ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
				ed.setErrorMessage (GenericUtils._l (lang, "incorrect_settings_analyze_bpmn") + "!");
	    		
	    		ed.open (window.getShell ());
	    		
	    		return null;
			}
		} 
    	catch (IOException e) {
			ErrorDialog ed = new ErrorDialog ();
    		
			ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
			ed.setErrorMessage ("Exception. " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
    		
    		ed.open (window.getShell ());
		}
	    

	    Object firstElement = selection.getFirstElement ();
    	try {
			DialogRun dialog       = new DialogRun ();
			IResource cu           = (IResource) firstElement;
		    String path            = cu.getLocation ().toOSString ();
			
		    // Se il file selezionato non ha estensione "bpmn", genero la finestra di dialogo di errore
			if (!FileUtils.getExtension (path).toLowerCase ().equals ("bpmn")) {
				ErrorDialog ed = new ErrorDialog ();
	    		
				ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
	    		ed.setErrorMessage (GenericUtils._l (lang, "message_error") + "! " + GenericUtils._l (lang, "select_bpmn_file") + "!");
	    		
	    		ed.open (shell);
			}
			else {
				// Imposto il file BPMN nella text della finestra di dialogo
				dialog.setFileBPMN (path);
				
				// Apro la finestra di dialogo per l'analisi del BPMN (DialogRunButton)
				dialog.open ();
			}
		} 
    	catch (Exception e) {
    		ErrorDialog ed = new ErrorDialog ();
    		
			ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
			ed.setErrorMessage ("Exception. " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
    		
    		ed.open (window.getShell ());
		}
    	
    	
    	// Forzo l'apertura della view. Durante la fase di testing, per aprire la view bisognava accedere alla "text"
		// "Quick Access" di Eclipse e cercare il nome della view.
		// In questo modo evito di dover far eseguire questo procedimento all'utente
		try {
			PlatformUI.getWorkbench ().getActiveWorkbenchWindow ().getActivePage ().showView ("BPMNtoPetriNetPlugin.BPMNtoPetriNetPluginView");
		} 
		catch (PartInitException e) {
			ErrorDialog ed = new ErrorDialog ();
    		
			ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
			ed.setErrorMessage ("PartInitException. " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
    		
    		ed.open (window.getShell ());
		}
	    
	    return null;
	}
}
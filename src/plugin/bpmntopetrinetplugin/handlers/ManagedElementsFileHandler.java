/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package plugin.bpmntopetrinetplugin.handlers;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Locale;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.ide.IDE;

import plugin.bpmntopetrinetplugin.dialog.ErrorDialog;
import utils.GenericUtils;
import utils.PluginUtils;

/**
 * Handler per l'apertura del file "managedElements.xml".
 * 
 * @author Marco
 */
public class ManagedElementsFileHandler extends AbstractHandler 
{
	/*
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute (ExecutionEvent event) throws ExecutionException 
	{
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked (event);
		IWorkbenchPage page     = PlatformUI.getWorkbench ().getActiveWorkbenchWindow ().getActivePage ();
		Locale lang             = null;
		IFileStore fileStore;
		String language;
		
		// Determino la lingua da utilizzare
		try {
			language = GenericUtils.getLanguage ();
			
			if (language.equals ("Italiano"))
				lang = new Locale ("it");
			else// if (language.equals ("English"))
				lang = new Locale ("en");
		} 
		catch (IOException e) {
			// Non posso utilizzare _l, in quanto lang = null
			ErrorDialog ed = new ErrorDialog ();
    		
			ed.setErrorMessage ("IOException. Message: " + e.getMessage ());
    		
    		ed.open (window.getShell ());
		}
		

		// Ottengo il file "managedElements.xml" interno al plugin
		try {
			fileStore = PluginUtils.getIFileStoreFromPathFile ("resources/managedElements.xml");
			
			try {
				// Apro il file in Eclipse
				IDE.openEditorOnFileStore (page, fileStore);
				
				PluginUtils.addTextToView (GenericUtils._l (lang, "level_info"), GenericUtils._l (lang, "open_managed_elements_xml") + "!");
			} 
			catch (PartInitException e) {
				ErrorDialog ed = new ErrorDialog ();
	    		
				ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
				ed.setErrorMessage ("PartInitException. " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
	    		
	    		ed.open (window.getShell ());
			}
		} 
		catch (IOException | URISyntaxException e) {
			ErrorDialog ed = new ErrorDialog ();
    		
			ed.setWindowTitle (GenericUtils._l (lang, "window_error_title"));
			ed.setErrorMessage (e.getClass ().getSimpleName () + ". " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
    		
    		ed.open (window.getShell ());
		}
		
		return null;
	}
}

/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package plugin.bpmntopetrinetplugin.view;

import java.io.IOException;
import java.util.Locale;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;

import plugin.bpmntopetrinetplugin.dialog.ErrorDialog;
import utils.GenericUtils;

/**
 * View del plugin. In questa view vengono stampati messaggi di Info/Warning/Errore
 * 
 * @author Marco
 */
public class BPMNtoPetriNetView extends ViewPart 
{
	/**
	 * Contenitore dei messaggi sulla view
	 */
	private Tree tree;
	
	
	/**
	 * "File" di lingua
	 */
	private Locale lang;
	
	
	
	/**
	 * Costruttore
	 */
	public BPMNtoPetriNetView () 
	{
		super ();
    }
    
     
	/*
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	public void createPartControl (final Composite parent) 
	{
		String language;
		
		// Determino la lingua da utilizzare
		try {
			language = GenericUtils.getLanguage ();
			
			if (language.equals ("Italiano"))
				lang = new Locale ("it");
			else// if (language.equals ("English"))
				lang = new Locale ("en");
		} 
		catch (IOException e) {
			// Non posso utilizzare _l, in quanto lang = null
			ErrorDialog ed = new ErrorDialog ();
    		
			ed.setErrorMessage ("IOException. Message: " + e.getMessage ());
    		
    		ed.open (this.getSite ().getShell ());
		}
		
		tree = new Tree (parent, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		
		tree.addMouseListener (new MouseAdapter () {
			@Override
			public void mouseDown (MouseEvent e) 
			{
				Menu popupMenu = new Menu (tree);
			    MenuItem clearItem = new MenuItem (popupMenu, SWT.CASCADE);
			    clearItem.setText (GenericUtils._l (lang, "clear"));
			    
			    clearItem.addListener (SWT.Selection, new Listener () {
			    	public void handleEvent (Event e) 
			    	{
			    		switch (e.type) {
			    			case SWT.Selection:
			    				tree.clearAll (true);
			    				tree.removeAll ();
			    				TreeItem[] t = tree.getItems ();
			    				for (int k = 0; k < t.length; k++) {
			    					t[k].clearAll(true);
			    					t[k].clear(k, true);
			    					t[k].removeAll();
			    				}
			    			
			    				break;
			    		}
			        }
			    });
			    
			    tree.setMenu(popupMenu);
			}
		});
		
		tree.setHeaderVisible(true);
		
		addColumn (GenericUtils._l (lang, "column_1_title"), 150, SWT.CENTER);
		addColumn (GenericUtils._l (lang, "column_2_title"), 250, SWT.CENTER);
		addColumn (GenericUtils._l (lang, "column_3_title"), 250, SWT.CENTER);
	}

	
	/**
	 * Metodo che aggiunge una colonna alla view
	 * 
	 * @param title Titolo della colonna
	 * @param width Larghezza colonna
	 * @param style Posizione colonna
	 * @return      Istanza della colonna creata (TreeColumn)
	 */
	private TreeColumn addColumn (String title, int width, int style)
	{
		TreeColumn c = new TreeColumn (tree, style);
		
	    c.setText (title);
	    c.setWidth (width);
	    
	    return c;
	}
	

	/**
	 * Aggiunge una riga alla view
	 * 
	 * @param level   "Livello" del messaggio (prima colonna della view)
	 * @param message Messaggio da stampare nella view (seconda colonna della view)
	 */
	public void addItem (String level, String message)
	{
		TreeItem item = new TreeItem (tree, SWT.LEFT);
		
		item.setText (new String[] { level, message, GenericUtils.now ("dd.MM.yyyy ~ HH:mm:ss") });
	}
	
	
	/*
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus () 
	{
	}
}
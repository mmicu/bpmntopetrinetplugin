/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package mapping.parser.xml;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import mapping.rules.*;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import utils.FileUtils;
import utils.StringUtils;
import mapping.data.structure.MappingRule;

/**
 * Parsing del file "rules.xml", da cui ricavo tutte le regole di mapping gestite dal plugin.
 * Le regole di mapping possono essere applicati a certi elementi BPMN, da cui si possono ricavare
 * elementi della rete di Petri come:
 *     - Piazze;
 *     - Transizioni;
 *     - Collegamenti PT (Place to Transition);
 *     - Collegamenti TP (Transition to Place).
 * 
 * @author Marco
 *
 */
public class XMLMappingRules 
{
	/**
	 * File XML su cui eseguire il parsing (in questo caso puo' essere solamente il file "rules.xml")
	 */
	public String fileName;
	
	
	/**
	 * Se il valore di questo attributo e' settato a "true", durante l'esecuzione del programma, verranno
	 * stampate diverse variabili
	 */
	private boolean debug;
	
	
	/**
	 * Risultato del parsing: regole di mapping
	 */
	private ArrayList<MappingRule> mappingRules;
	
	
	/**
	 * Istanza della i-esima regola trovata
	 */
	private MappingRule rI;
	
	
	/**
	 * Istanza della i-esima regola trovata di tipo PT
	 */
	PTRule ptRuleI;
	
	
	/**
	 * Istanza della i-esima regola trovata di tipo TP
	 */
	TPRule tpRuleI;
	
	
	/**
	 * Per i collegamenti con i place degli elementi BPMN collegati al BPMN in esame, attraverso questo attributo
	 * possiamo specificare la transizione iniziale per questi collegamenti.
	 * Esempio: se l'elemento BPMN in esame ha 5 transizione e "initialTransition = 3", significa che i collegamenti
	 *          TP inizieranno dalla terza transizione
	 */
	private String initialTransition;
	
	
	/**
	 * Se il valore di questo attributo e' "true", nei collegamenti TP seleziono sempre la transizione in base
	 * al valore di "initialTransition" (ad esempio il parallel gateway seleziona sempre la transizione numero 1)
	 */
	private boolean isUnique;
	
	
	/**
	 * Sono all'interno del tag <element>. Quindi posso memorizzare valori
	 */
	private boolean isTagElement;
	
	
	
	/**
	 * Costruttore
	 * 
	 * @param fileName File XML su cui eseguire il parsing (in questo caso puo' essere solamente il file "rules.xml") 
	 */
	public XMLMappingRules (String fileName)
	{
		this.fileName          = fileName;
		this.mappingRules      = new ArrayList<MappingRule> ();
		this.initialTransition = "0";
		this.isUnique          = false;
		this.debug             = false;
		this.isTagElement      = false;
		this.ptRuleI           = null;
		this.tpRuleI           = null;
	}
	
	
	/**
	 * Costruttore
	 * 
	 * @param fileName File XML su cui eseguire il parsing (in questo caso puo' essere solamente il file "rules.xml") 
	 * @param debug    "true" se si vogliono stampare messaggi di debug, "false" altrimenti
	 */
	public XMLMappingRules (String fileName, boolean debug)
	{
		this (fileName);
		
		this.debug = debug;
	}
	
	
	/**
	 * Metodo che esegue il parsing del file XML (this.fileName)
	 * 
	 * @return                               ArrayList dei tag XML, degli elementi del BPMN, che vengono mappati dal plugin
	 * @throws ParserConfigurationException  Eccezione generata dal metodo newSAXParser ()
	 * @throws SAXException                  Eccezione generata dal metodo newSAXParser ()
	 * @throws IOException                   Eccezione generata dal metodo parse (...)
	 */
	public ArrayList<MappingRule> parse () throws ParserConfigurationException, SAXException, IOException
	{
		SAXParserFactory factory = SAXParserFactory.newInstance ();
		SAXParser saxParser = factory.newSAXParser ();
		
	 
		DefaultHandler handler = new DefaultHandler () 
		{
			public void startElement (String uri, String localName, String qName, Attributes attributes) throws SAXException 
			{
				if (qName.equals ("rule"))
					rI = new MappingRule (attributes.getValue ("name") != null ? attributes.getValue ("name") : "");
				
				else if (qName.equals ("element"))
					isTagElement = true;
				
				else if (qName.equals ("place")) {
					PlaceRule pRule = new PlaceRule (
						attributes.getValue ("id"),
						attributes.getValue ("n")
					);
					
					rI.setPrule (pRule);
				}
				
				
				else if (qName.equals ("transition")) {
					TransitionRule tRule = new TransitionRule (
						attributes.getValue ("id"),
						attributes.getValue ("n")
					);
					
					rI.setTrule (tRule);
				}
				
				else if (qName.equals ("pt")) {
					if (ptRuleI == null)
						ptRuleI = new PTRule ();
					
					ptRuleI.addKeywordPlace (attributes.getValue ("place"));
					ptRuleI.addKeywordTransition (attributes.getValue ("transition"));
					
					if (attributes.getValue ("type") != null)
						ptRuleI.addType (attributes.getValue ("type"));
					else
						ptRuleI.addType ("$one_to_one");
				}
				
				else if (qName.equals ("tp")) {
					if (tpRuleI == null)
						tpRuleI = new TPRule ();
					
					tpRuleI.addKeywordTransition (attributes.getValue ("transition"));
					tpRuleI.addKeywordPlace (attributes.getValue ("place"));
					
					if (attributes.getValue ("type") != null)
						tpRuleI.addType (attributes.getValue ("type"));
					else
						tpRuleI.addType ("$one_to_one");
				}
				
				else if (qName.equals ("mapping")) {
					if (attributes.getValue ("initialTransition") != null)
						initialTransition = attributes.getValue ("initialTransition");
					else
						initialTransition = "0";
					
					if (attributes.getValue ("isUnique") != null)
						isUnique = (StringUtils.__trim (attributes.getValue ("isUnique"))).equals ("true");
					else
						isUnique = false;
					
					if (rI != null) {
						rI.setInitialTransition (initialTransition);
						rI.setUnique (isUnique);
					}
				}
			}
			
			
			public void endElement (String uri, String localName, String qName) throws SAXException 
			{
				if (qName.equals ("elements"))
					isTagElement = false;
				
				else if (qName.equals ("rule")) {
					rI.setPTrule (ptRuleI);
					rI.setTPrule (tpRuleI);
					
					mappingRules.add (rI);
					isTagElement = false;
					
					ptRuleI = null;
					tpRuleI = null;
					
					if (debug)
						System.out.println (rI.toString ());
				}
				
				else if (qName.equals ("mapping")) {
					initialTransition = "0";
					isUnique          = false;
				}
			}
			
			
			public void characters (char ch[], int start, int length) throws SAXException 
			{
				if (isTagElement) {
					rI.addElementMapped (new String (ch, start, length));
					isTagElement = false;
				}
			}
		};
		
		saxParser.parse (FileUtils.getInputStreamFromPathFile (this.fileName), handler);
	    
		
        // In alcuni file BPMN generati con BPMN2Modeler trovati su internet, non viene aggiunto
		// il namespace bpmn2. Quindi controllo che gli elementi mappati abbiano il namespace bpmn2.
		// Se questo namespace e' presente, aggiungo anche il tag senza namespace.
		// Esempio: trovo "bpmn2:startEvent", aggiungo anche "startEvent"
	    for (int k = 0; k < mappingRules.size (); k++) {
	    	ArrayList<String> elementsMapped           = mappingRules.get (k).getElementsMapped ();
	    	ArrayList<String> elementsWithoutNamespace = new ArrayList<String> ();
	    	
	    	for (int j = 0; j < elementsMapped.size (); j++) {
	    		if (elementsMapped.get (j).matches ("^bpmn2:.*")) {
	    			elementsWithoutNamespace.add (elementsMapped.get (j).substring (6, elementsMapped.get (j).length ()));
	    			elementsWithoutNamespace.add ("ns6:" + elementsMapped.get (j).substring (6, elementsMapped.get (j).length ()));
	    		}
	    	}
	    	
	    	for (int j = 0; j < elementsWithoutNamespace.size (); j++)
	    		if (!mappingRules.get (k).getElementsMapped ().contains (elementsWithoutNamespace.get (j)))
	    			mappingRules.get (k).addElementMapped (elementsWithoutNamespace.get (j));
	    		
	    }
	    
	    return mappingRules;
	}
}

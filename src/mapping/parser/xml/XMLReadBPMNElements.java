/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package mapping.parser.xml;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import utils.GenericUtils;
import utils.FileUtils;
import utils.StringUtils;
import mapping.data.structure.BPMNElement;
import mapping.data.structure.Pair;

/**
 * Parser XML per leggere tutti gli elementi BPMN che possiamo mappare in una rete di Petri.
 * Per conoscere gli elementi che il plugin puo' mappare, e' sufficiente richiamare la classe XMLMappedElements
 * che preleva tutti i tag XML di elementi BPMN che hanno una regola di mapping
 * 
 * @author Marco
 *
 */
public class XMLReadBPMNElements 
{
	/**
	 * Keyword per "event definitions". Se un evento ha una di queste keyword nella sua dichiarazione,
	 * viene aggiunto il valore della keyword nel suo id
	 */
	private static final String[] KEYWORD_EVENT_DEFINITIONS = 
	{ 
		"bpmn2:conditionalEventDefinition", 
		"bpmn2:timerEventDefinition",
		"bpmn2:signalEventDefinition",
		"bpmn2:messageEventDefinition",
		"bpmn2:escalationEventDefinition",
		"bpmn2:compensateEventDefinition",
		"bpmn2:errorEventDefinition",
		"bpmn2:terminateEventDefinition",
		"conditionalEventDefinition", 
		"timerEventDefinition",
		"signalEventDefinition",
		"messageEventDefinition",
		"escalationEventDefinition",
		"compensateEventDefinition",
		"errorEventDefinition",
		"terminateEventDefinition"
	};
	
	
	/**
	 * File BPMN su cui eseguire il parsing
	 */
	private String fileName;
	
	
	/**
	 * Se il valore di questo attributo e' settato a "true", durante l'esecuzione del programma, verranno
	 * stampate diverse variabili
	 */
	private boolean debug;
	
	
	/**
	 * Elementi BPMN che possono essere mappati in una rete di Petri
	 */
	private ArrayList<BPMNElement> bpmnElements;
	
	
	/**
	 * Istanza i-esima dell'elemento BPMN
	 */
	private BPMNElement b;
	
	
	/**
	 * Sono all'interno del tag <incoming>. Quindi posso memorizzare valori di questo tag
	 */
	private boolean insideIncoming;
	
	
	/**
	 * Sono all'interno del tag <outgoing>. Quindi posso memorizzare valori di questo tag
	 */
	private boolean insideOutgoing;
	
	
	/**
	 * Sono all'interno del tag <participantRef>. Quindi posso memorizzare valori di questo tag
	 */
	private boolean insideParticipantRef;
	
	
	/**
     * Tipo di "event definition"
     */
	private String eventDefinition;
	
	
	
	/**
	 * Costruttore
	 * 
	 * @param fileName File BPMN su cui eseguire il parsing
	 */
	public XMLReadBPMNElements (String fileName)
	{
		this.fileName             = fileName;
		this.bpmnElements         = new ArrayList<BPMNElement> ();
		this.b                    = null;
		this.insideIncoming       = false;
		this.insideOutgoing       = false;
		this.insideParticipantRef = false;
		this.eventDefinition      = null;
	}
	
	
	/**
	 * Costruttore
	 * 
	 * @param fileName File BPMN su cui eseguire il parsing
	 * @param debug    "true" se si vogliono stampare messaggi di debug, "false" altrimenti
	 */
	public XMLReadBPMNElements (String fileName, boolean debug)
	{
		this (fileName);
		
		this.debug = debug;
	}
	
	
	/**
	 * Metodo che esegue il parsing del file XML (this.fileName)
	 * 
	 * @return                               ArrayList dei tag XML, degli elementi del BPMN, che vengono mappati dal plugin
	 * 
	 * @throws ParserConfigurationException  Eccezione generata dal metodo newSAXParser () e XMLMappedElements (...).parse ()
	 * @throws SAXException                  Eccezione generata dal metodo newSAXParser () e XMLMappedElements (...).parse ()
	 * @throws IOException                   Eccezione generata dal metodo parse (...) e XMLMappedElements (...).parse ()
	 */
	public ArrayList<BPMNElement> parse (final ArrayList<String> mappedElements) throws ParserConfigurationException, SAXException, IOException
	{
		// Primo elemento della coppia: "id" participant
		// Secondo elemento della coppia: "name" participant
		final ArrayList<Pair<String, String>> participantI = new ArrayList<Pair<String, String>> ();
		
		SAXParserFactory factory = SAXParserFactory.newInstance ();
		SAXParser saxParser = factory.newSAXParser ();
	 
		DefaultHandler handler = new DefaultHandler () 
		{
			public void startElement (String uri, String localName, String qName, Attributes attributes) throws SAXException 
			{				
				if (mappedElements.contains (qName)) {
					b = new BPMNElement (qName);
					
					b.setId (attributes.getValue ("id"));
					b.setRenamedID (attributes.getValue ("id"));
					b.setName (attributes.getValue ("name"));
					
					if (debug)
						System.out.println ("BPMNElement: " + b.toString ());
				}
				
				else if ((qName.equals ("bpmn2:incoming") || qName.equals ("ns6:incoming") || qName.equals ("incoming")) && b != null)
					insideIncoming = true;
					
				else if ((qName.equals ("bpmn2:outgoing") || qName.equals ("ns6:outgoing") || qName.equals ("outgoing")) && b != null)
					insideOutgoing = true;
				
				else if ((qName.equals ("bpmn2:participantRef") || qName.equals ("ns6:participantRef") || qName.equals ("participantRef")) && b != null)
					insideParticipantRef = true;
				
				else if ((qName.equals ("bpmn2:participant") || qName.equals ("ns6:participant") || qName.equals ("participant"))) {
					Pair<String, String> p = new Pair<String, String> ();
					
					if (attributes.getValue ("id") != null)
						p.setFirstElement (attributes.getValue ("id"));
					
					if (attributes.getValue ("name") != null)
						p.setSecondElement (attributes.getValue ("name"));
					else
						p.setSecondElement (p.getFirstElement ()); // name = id
					
					participantI.add (p);
				}
				else
					for (int k = 0; k < KEYWORD_EVENT_DEFINITIONS.length; k++)
						if (qName.equals (KEYWORD_EVENT_DEFINITIONS[k]))
						    eventDefinition = qName;
			}
			
			
			public void endElement (String uri, String localName, String qName) throws SAXException 
			{
				if (b != null && qName.equals (b.getType ())) {
				    if (eventDefinition != null)
				        b.setEventDefinition (eventDefinition);
				    
					bpmnElements.add (b);
					b = null;
					eventDefinition = null;
				}
			}
			
			
			public void characters (char ch[], int start, int length) throws SAXException 
			{
				if (insideIncoming) {
					b.addsequenceFlowIncoming (new String (ch, start, length));
					insideIncoming = false;
				}
				else if (insideOutgoing) {
					b.addsequenceFlowOutgoing (new String (ch, start, length));
					insideOutgoing = false;
				}
				else if (insideParticipantRef) {
					// Aggiungo l'id del "participant". Una volta finito il parsing, sostituisco 
					// l'id del "participant" con il valore dell'attributo "name" che e' memorizzato
					// nella variabile participantI (primo elemento coppia: id "participant", secondo elemento
					// della coppia: name "participant"
					b.addParticipant (new String (ch, start, length));
					insideParticipantRef = false;
				}
			}
		};
		
		saxParser.parse (FileUtils.getInputStreamFromPathFile (this.fileName, true), handler);
		
		// Sostituisco l'id del "participant" con il suo nome
		int size_b  = bpmnElements.size ();
		int size_pi = participantI.size ();
		
		for (int k = 0; k < size_b; k++) {
			ArrayList<String> participantRef    = bpmnElements.get (k).getParticipantName ();
			ArrayList<String> newParticipantRef = new ArrayList<String> ();
				
			for (int j = 0; j < size_pi; j++) {
				String id   = participantI.get (j).getFirstElement ();	
				String name = StringUtils.__trim (participantI.get (j).getSecondElement ());
				
				int size_pr = participantRef.size ();
				
				for (int i = 0; i < size_pr; i++)
					if (participantRef.get (i).equals (id))
						newParticipantRef.add (name);
			}
			
			bpmnElements.get (k).setParticipantName (newParticipantRef);
		}
		
		// L'editor ADO, non dichiara i tag <bpmn2:incoming> e <bpmn2:outgoing> all'interno del tag dell'elemento BPMN.
		// Quindi occorre controllare i tag <sequenceFlow> per ottenere i collegamenti.
		
		if (GenericUtils.isADOmodel (FileUtils.getInputStreamFromPathFile (this.fileName, true))) {
    		DefaultHandler handlerADO = new DefaultHandler () 
            {
                public void startElement (String uri, String localName, String qName, Attributes attributes) throws SAXException 
                {
                    if (qName.equals ("bpmn2:sequenceFlow") || qName.equals ("sequenceFlow")) {
                        String idSequence   = attributes.getValue ("id");
                        String idBpmnSource = attributes.getValue ("sourceRef");
                        String idBpmnTarget = attributes.getValue ("targetRef");
    
                        if (idSequence != null && idBpmnSource != null && idBpmnTarget != null) {
                            boolean complete_half = false; // Ricerca completata a meta'
                            
                            for (int k = 0, size_b = bpmnElements.size (); k < size_b; k++) {
                                String bpmnId = bpmnElements.get (k).getId ();
                                
                                // BPMN source => all'elemento BPMN va aggiunto il sequence flow in uscita
                                if (bpmnId.equals (idBpmnSource)) {
                                    bpmnElements.get (k).addsequenceFlowOutgoing (idSequence);
                                    
                                    if (complete_half)
                                        break;
                                    
                                    complete_half = true;
                                }
                                
                                // BPMN target => all'elemento BPMN va aggiunto il sequence flow in ingresso
                                else if (bpmnId.equals (idBpmnTarget)) {
                                    bpmnElements.get (k).addsequenceFlowIncoming (idSequence);
                                    
                                    if (complete_half)
                                        break;
                                    
                                    complete_half = true;
                                }
                            }
                        }
                    }
                }
            };
            
            saxParser.parse (FileUtils.getInputStreamFromPathFile (this.fileName, true), handlerADO);
		}
		
	    return bpmnElements;
	}
}

/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package mapping.parser.xml;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import utils.FileUtils;
import mapping.data.structure.BPMNElement;
import mapping.data.structure.DataAssociation;
import mapping.data.structure.DataObject;
import mapping.data.structure.SubElements;

/**
 * Parser XML per i Data Object, realizzato sul file BPMN
 * 
 * @author Marco
 *
 */
public class XMLDataObjects 
{
	/**
	 * File BPMN su cui eseguire il parsing
	 */
	private String fileName;
	
	
	/**
	 * Se il valore di questo attributo e' settato a "true", durante l'esecuzione del programma, verranno
	 * stampate diverse variabili
	 */
	private boolean debug;
	
	
	/**
	 * I-esima "dataAssociation"
	 */
	private DataAssociation dataAssociationI;
	
	
	/**
	 * Istanza di appoggio, utilizzata durante il parsing
	 */
	private DataAssociation dataAssociationIApp;
	
	
	/**
	 * Ogni "dataAssociation" rilevata, viene memorizzata in questo ArrayList
	 */
	private ArrayList<DataAssociation> dataAssociations;
	
	
	/**
	 * Sono all'interno del tag "dataAssociation" che puo' essere di input o output
	 */
	private boolean isDataAssociation;
	
	
	/**
	 * Sono all'interno del tag "bpmn2:sourceRef"
	 */
	private boolean isSourceRefDataAssociation;
	
	
	/**
	 * Sono all'interno del tag "bpmn2:targetRef"
	 */
	private boolean isTargetRefDataAssociation;
	
	
	/**
	 * Tag XML di un elemento BPMN che puo' essere mappato (questi valori sono contenuti all'interno dei tag <element>
	 * del file (rules.xml)
	 */
	private String qNameI;
	
	
	/**
	 * Una volta trovati tutti i "data association" , creo l'istanza "Data Object" che andro' successivamente
	 * ad inserire nell'attributo successivo (dataObjects)
	 */
	private DataObject dataObjectI;
	
	
	/**
	 * ArrayList che conterra' tutti i "Data Object" dichiarati all'intenro del file BPMN
	 */
	private ArrayList<DataObject> dataObjects;
	
	
	
	/**
	 * Costruttore
	 * 
	 * @param fileName File BPMN su cui eseguire il parsing
	 */
	public XMLDataObjects (String fileName)
	{
		this.debug = false;
		
		// Attributi primo parsing
		this.fileName                   = fileName;
		this.dataAssociations           = new ArrayList<DataAssociation> ();
		this.isDataAssociation          = false;
		this.isSourceRefDataAssociation = false;
		this.isTargetRefDataAssociation = false;
		this.dataAssociationI           = null;
		this.dataAssociationIApp        = null;
		this.qNameI                     = null;
		
		// Attributi secondo parsing
		this.dataObjects = new ArrayList<DataObject> ();
		this.dataObjectI = null;
	}

	
	/**
	 * Costruttore
	 * 
	 * @param fileName File BPMN su cui eseguire il parsing
	 * @param debug    "true" se si vogliono stampare messaggi di debug, "false" altrimenti
	 */
	public XMLDataObjects (String fileName, boolean debug)
	{
		this (fileName);
		
		this.debug = debug;
	}
	
	
	/**
	 * .
	 * 
	 * @return
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 */
	public void parse (ArrayList<BPMNElement> b, ArrayList<SubElements> subProcesses, ArrayList<String> mappedElements) throws ParserConfigurationException, SAXException, IOException
	{
		final ArrayList<String> mappedElementsCopy = new ArrayList<String> ();
		
		// Creo una copia per evitare la modifica del parametro
		int size_m = mappedElements.size ();
		for (int k = 0; k < size_m; k++)
			mappedElementsCopy.add (mappedElements.get (k));
		
		// I data object possono essere collegati a SubProcess, adHocSubProcess
		mappedElementsCopy.add ("bpmn2:subProcess");
		mappedElementsCopy.add ("bpmn2:adHocSubProcess");
				
		// Ottengo tutti i SubProcess del modello inserendo in un apposito ArrayList di tipo SubProcess
		SAXParserFactory factory = SAXParserFactory.newInstance ();
		SAXParser saxParser = factory.newSAXParser ();
	 
		DefaultHandler handler = new DefaultHandler () 
		{
			public void startElement (String uri, String localName, String qName, Attributes attributes) throws SAXException 
			{
				if (qName.equals ("bpmn2:dataInputAssociation") || qName.equals ("bpmn2:dataOutputAssociation")) {
					isDataAssociation = true;
					
					dataAssociationI = new DataAssociation (qName);
				}
				
				else if (qName.equals ("bpmn2:sourceRef") && isDataAssociation)
					isSourceRefDataAssociation = true;
				
				else if (qName.equals ("bpmn2:targetRef") && isDataAssociation)
					isTargetRefDataAssociation = true;
				
				else if (mappedElementsCopy.contains (qName)) {
					dataAssociationIApp = new DataAssociation ();
					
					dataAssociationIApp.setRootTag (qName);
					
					if (attributes.getValue ("id") != null)
						dataAssociationIApp.setRootId (attributes.getValue ("id"));
					
					if (attributes.getValue ("name") != null)
						dataAssociationIApp.setRootName (attributes.getValue ("name"));
					
					qNameI = qName;
				}
			}
			
			
			public void endElement (String uri, String localName, String qName) throws SAXException 
			{
				if (qName.equals ("bpmn2:dataInputAssociation") || qName.equals ("bpmn2:dataOutputAssociation")) {
					if (dataAssociationIApp != null) {
						dataAssociationI.setRootTag (dataAssociationIApp.getRootTag ());
						dataAssociationI.setRootId (dataAssociationIApp.getRootId ());
						dataAssociationI.setRootName (dataAssociationIApp.getRootName ());
					}
					
					dataAssociations.add (dataAssociationI);
					
					dataAssociationI  = null;
					isDataAssociation = false;
				}
				
				else if (qName.equals (qNameI))
					dataAssociationIApp = null;
				
				isSourceRefDataAssociation = (qName.equals ("bpmn2:sourceRef") && isDataAssociation) ? false : isSourceRefDataAssociation;
				isTargetRefDataAssociation = (qName.equals ("bpmn2:targetRef") && isDataAssociation) ? false : isTargetRefDataAssociation;
			}
			
			
			public void characters (char ch[], int start, int length) throws SAXException 
			{
				if (isSourceRefDataAssociation && dataAssociationI != null)
					dataAssociationI.setIdSource (new String (ch, start, length));
				
				if (isTargetRefDataAssociation && dataAssociationI != null)
					dataAssociationI.setIdTarget (new String (ch, start, length));
			}
		};
		
		saxParser.parse (FileUtils.getInputStreamFromPathFile (this.fileName, true), handler);
		
	    
	    
	    
	    
	    
	    /* SECONDO PARSING */
	    
	    // Per ottenere i nomi reali dei dataObject, devo contollare il tag "bpmn2:ioSpecification.
	    // Se il dataAssociation e' un "input" l'id di referenza e' presente nel tag "sourceRef"
	    // Se il dataAssociation e' un "output" l'id di referenza e' presente nel tag "targetRef"
	    final ArrayList<String> refercesForRealDataObject = new ArrayList<String> ();
	    for (int k = 0; k < dataAssociations.size (); k++)
	    	refercesForRealDataObject.add (dataAssociations.get (k).isInputAssociation ()
	    									? dataAssociations.get (k).getIdSource ()
	    									: dataAssociations.get (k).getIdTarget ()
	    	);
	    
		// Ottengo tutti i SubProcess del modello inserendo in un apposito ArrayList di tipo SubProcess
		factory = SAXParserFactory.newInstance ();
		saxParser = factory.newSAXParser ();
	 
		handler = new DefaultHandler () 
		{
			public void startElement (String uri, String localName, String qName, Attributes attributes) throws SAXException 
			{
				if (attributes.getValue ("id") != null && refercesForRealDataObject.contains (attributes.getValue ("id"))) {
					dataObjectI = new DataObject (qName);
					
					dataObjectI.setId (attributes.getValue ("id"));
					
					if (attributes.getValue ("name") != null)
						dataObjectI.setName (attributes.getValue ("name"));
					
					if (attributes.getValue ("dataObjectRef") != null)
						dataObjectI.setDataObjectRefId (attributes.getValue ("dataObjectRef"));
					
					dataObjects.add (dataObjectI);
					
					int index = refercesForRealDataObject.lastIndexOf (attributes.getValue ("id"));
					
					if (index != -1)
						dataAssociations.get (index).setRefDataObject (dataObjectI);
				}
				
				else if (qName.equals ("bpmn2:dataState") || qName.equals ("dataState"))
					if (dataObjectI != null && attributes.getValue ("name") != null)
						dataObjectI.setState (attributes.getValue ("name"));
			}
		};
		
		saxParser.parse (FileUtils.getInputStreamFromPathFile (this.fileName, true), handler);
	    
		
	    
	    
	    /*
|dataAssociations| = 8
DataAssociation [rootTag=bpmn2:subProcess, rootId=SubProcess_1, rootName=Sub Process 1, tag=bpmn2:dataInputAssociation, idSource=DataObject_3, idTarget=DataInput_5, isInput=true]
DataAssociation [rootTag=bpmn2:subProcess, rootId=SubProcess_1, rootName=Sub Process 1, tag=bpmn2:dataInputAssociation, idSource=DataObject_4, idTarget=DataInput_6, isInput=true]
DataAssociation [rootTag=bpmn2:subProcess, rootId=SubProcess_1, rootName=Sub Process 1, tag=bpmn2:dataOutputAssociation, idSource=DataOutput_4, idTarget=DataOutput_3, isInput=false]
DataAssociation [rootTag=bpmn2:task, rootId=Task_2, rootName=Task 2, tag=bpmn2:dataInputAssociation, idSource=DataInput_3, idTarget=DataInput_4, isInput=true]
DataAssociation [rootTag=bpmn2:task, rootId=Task_1, rootName=Task 1, tag=bpmn2:dataInputAssociation, idSource=DataObject_1, idTarget=DataInput_1, isInput=true]
DataAssociation [rootTag=bpmn2:task, rootId=Task_1, rootName=Task 1, tag=bpmn2:dataInputAssociation, idSource=_DataStoreReference_2, idTarget=DataInput_2, isInput=true]
DataAssociation [rootTag=bpmn2:task, rootId=Task_1, rootName=Task 1, tag=bpmn2:dataOutputAssociation, idSource=DataOutput_1, idTarget=DataObject_2, isInput=false]
DataAssociation [rootTag=bpmn2:task, rootId=Task_1, rootName=Task 1, tag=bpmn2:dataOutputAssociation, idSource=DataOutput_2, idTarget=_DataObjectReference_4, isInput=false]
|dataObjects| = 8
DataObject [tag=bpmn2:dataInput, id=DataInput_3, name=datainput3]
DataObject [tag=bpmn2:dataOutput, id=DataOutput_4, name=output1]
DataObject [tag=bpmn2:dataOutput, id=DataOutput_1, name=output1]
DataObject [tag=bpmn2:dataOutput, id=DataOutput_2, name=output2]
DataObject [tag=bpmn2:dataObject, id=DataObject_1, name=Data Object 2]
DataObject [tag=bpmn2:dataStoreReference, id=_DataStoreReference_2, name=yoooooo]
DataObject [tag=bpmn2:dataObject, id=DataObject_3, name=maaaa]
DataObject [tag=bpmn2:dataObject, id=DataObject_4, name=Data Object 6]
	     */
	    
	    /*
StartEvent_1: [], []
StartEvent_2: [], []
Task_2: [DataObject [tag=bpmn2:dataInput, id=DataInput_3, name=datainput3]], []
EndEvent_3: [], []
Task_1: [DataObject [tag=bpmn2:dataObject, id=DataObject_1, name=Data Object 2], DataObject [tag=bpmn2:dataStoreReference, id=_DataStoreReference_2, name=yoooooo]], [DataObject [tag=bpmn2:dataOutput, id=DataOutput_1, name=output1], DataObject [tag=bpmn2:dataOutput, id=DataOutput_2, name=output2]]
EndEvent_2: [], []
	     */
	    /*
	     * 
	     * le dimensioni di dataAssociations e dataObject DEVONO essere le stesse
	     * Come gestire una dimensione diversa????
	     * 
	     */
	    
	    
	   
	    int size_b  = b.size ();
	    int size_da = dataAssociations.size ();
	    int size_do = dataObjects.size ();
	    
	    /*
	    |dataAssociations| = 4
	    		DataAssociation [rootTag=bpmn2:task, rootId=Task_1, rootName=Task 1, tag=bpmn2:dataInputAssociation, idSource=DataObject_1, idTarget=DataInput_1]
	    		DataAssociation [rootTag=bpmn2:task, rootId=Task_1, rootName=Task 1, tag=bpmn2:dataInputAssociation, idSource=_DataObjectReference_5, idTarget=DataInput_8]
	    		DataAssociation [rootTag=bpmn2:task, rootId=Task_2, rootName=Task 2, tag=bpmn2:dataInputAssociation, idSource=DataObject_2, idTarget=DataInput_2]
	    		DataAssociation [rootTag=bpmn2:task, rootId=Task_3, rootName=Task 3, tag=bpmn2:dataInputAssociation, idSource=DataObject_3, idTarget=DataInput_3]
	    		|dataObjects| = 4
	    		DataObject [tag=bpmn2:dataObject, id=DataObject_1, name=Data Object 2, dataObjectRefId=null, dataObjectRef=null]
	    		DataObject [tag=bpmn2:dataObject, id=DataObject_2, name=Data Object 3, dataObjectRefId=null, dataObjectRef=null]
	    		DataObject [tag=bpmn2:dataObject, id=DataObject_3, name=Data Object 4, dataObjectRefId=null, dataObjectRef=null]
	    		DataObject [tag=bpmn2:dataObjectReference, id=_DataObjectReference_5, name=Reference to Data Object 2, dataObjectRefId=DataObject_1, dataObjectRef=null]
	    				*/
	    
	    // Aggiungo il collegamento al do
	    for (int k = 0; k < size_do; k++) {
	    	// Il Data Object e' referenziato ad un altro Data Object
	    	if (dataObjects.get (k).getDataObjectRefId () != null) {
	    		for (int j = 0; j < size_do; j++) {
	    			if (k != j && dataObjects.get (j).getId ().equals (dataObjects.get (k).getId ())) {
	    				dataObjects.get (k).setDataObjectRef (dataObjects.get (k));
	    				
	    				System.out.println (dataObjects.get (k).getId() + " riferito a " + dataObjects.get (j).getId());
	    			}
	    		}
	    	}
	    }
	    
	    for (int k = 0; k < size_da; k++) {
	    	DataObject d = dataAssociations.get (k).getRefDataObject ();
	    	
	    	// Il Data Object e' referenziato ad un altro Data Object
	    	if (d.getDataObjectRefId () != null) {
	    		for (int j = 0; j < size_do; j++) {
	    			if (d.getDataObjectRefId ().equals (dataObjects.get (j).getId ())) {
	    				d.setDataObjectRef (dataObjects.get (j));
	    				dataAssociations.get (k).setRefDataObject (d);
	    				
	    				if (debug)
	    					System.out.println (d.getName () + " riferito a " + dataObjects.get (j).getName ());
	    			}
	    		}
	    	}
	    }

	    
	    // I data object possono essere direttamente collegati ad un SubElement (subProcess o adHocSubProcess).
	    // Quindi, in questo caso particolare, il data object andra' collegato (in ingresso o in uscita) a tutti
	    // gli elementi del SubElement (end event escluso)
	    for (int k = 0; k < size_b; k++) {
	    	// Se l'elemento fa parte di un SubElement collego tutti i data object collegati al SubElement con
	    	// il k-esimo elemento BPMN
	    	if (b.get (k).getidSubElement () != null) {
	    		for (int j = 0; j < size_da; j++) {
	    			if ((dataAssociations.get (j).getRootTag ().equals ("bpmn2:subProcess") || dataAssociations.get (j).getRootTag ().equals ("bpmn2:adHocSubProcess") ||
	    				 dataAssociations.get (j).getRootTag ().equals ("subProcess") || dataAssociations.get (j).getRootTag ().equals ("adHocSubProcess"))
	    				&& dataAssociations.get (j).getRootId ().equals (b.get (k).getidSubElement ())
	    				&& !b.get (k).getType ().equals ("bpmn2:endEvent")) {
	    					if (dataAssociations.get (j).isInputAssociation ())
			    				b.get (k).addIncomingDataFlow (dataAssociations.get (j).getRefDataObject ());
			    			else
			    				b.get (k).addOutgoingDataFlow (dataAssociations.get (j).getRefDataObject ());
	    			}
	    		}
	    	}

	    	// Il k-esimo elemento BPMN, non fa parte di un SubElement. Tuttavia, se trovo un data object
	    	// collegato con esso, lo aggiungo all'oggetto BPMN
	    	for (int j = 0; j < size_da; j++) {
	    		if (b.get (k).getId ().equals (dataAssociations.get (j).getRootId ())) {
	    			if (dataAssociations.get (j).isInputAssociation ())
	    				b.get (k).addIncomingDataFlow (dataAssociations.get (j).getRefDataObject ());
	    			else
	    				b.get (k).addOutgoingDataFlow (dataAssociations.get (j).getRefDataObject ());
	    		}
	    	}
	    }
	    
	    /*
	     * 
	     * I DATA OBJECT POSSONO STARE SU SUBPROCESS E ALTRA ROBA (SCRITTO SOPRA). COME GESTIRLI???
	     * 
	     * 
	     */
	    
	    System.out.println ("|dataAssociations| = " + dataAssociations.size ());
	    for (int k = 0; k < dataAssociations.size (); k++)
	    	System.out.println (dataAssociations.get (k).toString ());
	    
	    System.out.println ("|dataObjects| = " + dataObjects.size ());
	    for (int k = 0; k < dataObjects.size (); k++)
	    	System.out.println (dataObjects.get (k).toString ());
		
		System.out.println ("\n\n|BPMNElements| = " + b.size ());
		for (int k = 0; k < size_b; k++) {
			System.out.println (b.get (k).getId () + ": ");
			ArrayList<DataObject> a_1 = b.get (k).getIncomingDataFlow();
			ArrayList<DataObject> a_2 = b.get (k).getOutgoingDataFlow ();
			String app = "";
			for (int j = 0; j < a_1.size (); j++)
				app += a_1.get(j).getName() + (j != a_1.size()-1 ? ", " : ".");
			System.out.println ("Incoming flow data: " + app);
			
			app = "";
			for (int j = 0; j < a_2.size (); j++)
				app += a_2.get(j).getName() + (j != a_2.size()-1 ? ", " : ".");
			System.out.println ("Outgoing flow data: " + app);
			
			System.out.print ("\n\n");
			
		}
		
		System.out.println ("\n\n|BPMNElements| = " + b.size ());
		for (int k = 0; k < b.size (); k++)
			System.out.println (b.get (k).getId () + ": " + b.get (k).getIncomingDataFlow().size() + ", " + b.get (k).getOutgoingDataFlow ().size());
	}
}

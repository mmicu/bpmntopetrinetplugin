/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package mapping.parser.xml;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import utils.GenericUtils;
import utils.FileUtils;
import utils.StringUtils;
import mapping.data.structure.BPMNElement;
import mapping.data.structure.MappingRule;
import mapping.data.structure.Pair;

/**
 * Questa classe esegue il parsing del file BPMN per conoscere il prefisso di ogni elemento BPMN.
 * Il prefisso di ogni elemento BPMN, e' assegnato in base all'elemento "padre" dell'elemento BPMN.
 * Se ad esempio, l'elemento BPMN "startEvent", si trova all'iterno di un "lane", l'id dello
 * "startEvent" avra' come prefisso l'id del "lane".
 *
 * Vista una possibile ricorsione nei tag:
 *     - subProcess;
 *     - adHocSubProcess;
 *     - subChoreography;
 * questi elementi vengono gestiti dalla classe XMLSubElements che, oltre al normale prefisso,
 * memorizza gli elementi che fanno parte di questi elementi "padri" per risolvere i collegamenti.
 * 
 * @author Marco
 *
 */
public class XMLPrefixNameElements 
{
	/**
	 * File BPMN su cui eseguire il parsing
	 */
	private String fileName;
	
	
	/**
	 * ArrayList dei prefissi. Se l'elemento BPMN non e' contenuto in uno degli elementi specificati
	 * sopra, viene assegnato un prefisso di default
	 */
	private ArrayList<String> prefixNames;
	
	
	/**
	 * ArrayList di lane. Vengono gestiti in modo particolare nel file XML del BPMN, quindi bisogna
	 * per forza creare questo ArrayList
	 */
	private ArrayList<String> lanesElements;
	
	
	/**
	 * Prefisso di default: attributo "name" del tag "bpmn2:process"
	 */
	private String defaultPrefix;
	
	
	/**
	 * Processo di default: attributo "name" del tag "bpmn2:process"
	 */
	private String defaultProcess;
	
	
	/**
	 * Questo attributo ha valore "true" se siamo all'interno del tag "flowNodeRef", "false" altrimenti
	 */
	private boolean isFlowNodeRef;
	
	
	/**
	 * Questo attributo ha valore "true" se siamo all'interno di un "subProcess", "false" altrimenti
	 */
	private boolean isSubprocess;
	
	
	/**
	 * Questo attributo ha valore "true" se siamo all'interno del processo principale, "false" altrimenti
	 */
	private boolean isMainprocess;
	
	
	/**
	 * Questo attributo ha valore "true" se siamo all'interno del di un "adHocSucProcess", "false" altrimenti
	 */
	private boolean isAdHocSubProcess;
	
	
	/**
	 * ArrayList di coppie di partecipanti (attributi "id" e "name" del tag "bpmn2:participant")
	 */
	private ArrayList<Pair <String, String>> participantNames;
	
	
	
	/**
	 * Costruttore
	 * 
	 * @param fileName File BPMN su cui eseguire il parsing
	 */
	public XMLPrefixNameElements (String fileName)
	{
		this.fileName                            = fileName;
		this.prefixNames                         = new ArrayList<String> ();
		this.lanesElements                       = new ArrayList<String> ();
		this.participantNames                    = new ArrayList<Pair <String, String>> ();
		this.isFlowNodeRef                       = false;
		this.defaultProcess = this.defaultPrefix = "defaultProcess";
		this.isSubprocess                        = false;
		this.isMainprocess                       = true;
		this.isAdHocSubProcess                   = false;
	}
	
	
	/**
	 * Metodo che esegue il parsing del file XML (this.fileName)
	 * 
	 * @return                               ArrayList dei prefissi. Se l'elemento BPMN non e' contenuto in uno degli 
	 *                                       elementi specificati sopra, viene assegnato un prefisso di default
	 * @throws SAXException                  Eccezione generata dal metodo newSAXParser () e new XMLMappedElements (...).parse ()
	 * @throws ParserConfigurationException  Eccezione generata dal metodo newSAXParser () e new XMLMappedElements (...).parse ()
	 * @throws IOException                   Eccezione generata dal metodo parse (...) e new XMLMappedElements (...).parse ()
	 */
	public ArrayList<String> parse (final ArrayList<BPMNElement> b, final ArrayList<String> mappedElements, final ArrayList<MappingRule> mappingRules) throws ParserConfigurationException, SAXException, IOException
	{
		// Setto il valore di prefixNames con l'id dell'elemento BPMN 
		for (int k = 0, size_b = b.size (); k < size_b; k++)
			prefixNames.add (this.getNameOrIdBPMNElement (b.get (k), mappingRules));
		
		SAXParserFactory factory = SAXParserFactory.newInstance ();
		SAXParser saxParser = factory.newSAXParser ();
	 
		DefaultHandler handler = new DefaultHandler () 
		{
			public void startElement (String uri, String localName, String qName, Attributes attributes) throws SAXException 
			{
				if (qName.equals ("bpmn2:lane")) {
					defaultPrefix = StringUtils.__trim (attributes.getValue ("name"));
					isMainprocess = false;
				}
				
				else if (qName.equals ("bpmn2:flowNodeRef")) {
					isFlowNodeRef = true;
					isMainprocess = false;
				}
					
				else if (qName.equals ("bpmn2:process")) {
					isMainprocess = true;
					defaultProcess = null;
					
					for (int k = 0; k < participantNames.size (); k++) {
						if (participantNames.get (k).getSecondElement () != null && participantNames.get (k).getSecondElement ().equals (attributes.getValue ("id"))) {
							
							defaultPrefix = participantNames.get (k).getFirstElement ();
							break;
						}
					}
					
					if (defaultProcess == null) {
						if (attributes.getValue ("name") == null && attributes.getValue ("id") == null)
							defaultProcess = defaultPrefix = "defaultProcess";
						else if (attributes.getValue ("name") != null)
							defaultProcess = defaultPrefix = StringUtils.__trim (attributes.getValue ("name"));
						else
							defaultProcess = defaultPrefix = StringUtils.__trim (attributes.getValue ("id"));
					}
				}
				
				
				else if (qName.equals ("bpmn2:laneSet"))
					isMainprocess = false;
				
				else if (mappedElements.contains (qName) && !lanesElements.contains (attributes.getValue ("id")) && !isSubprocess && !isAdHocSubProcess && isMainprocess) {
					int index = GenericUtils.getIndexBPMNElementById (b, attributes.getValue ("id"));
					
					if (index != -1)
						prefixNames.set (index, defaultPrefix + "_" + getNameOrIdBPMNElement (b.get (index), mappingRules));
					//else
						//;
						//return prefixNamesCopy;
				}
				
				else if (qName.equals ("bpmn2:participant")) {
					participantNames.add (Pair.pair (
							StringUtils.__trim (attributes.getValue ("name") == null ? "" : attributes.getValue ("name")), 
							attributes.getValue ("processRef") == null ? "" : attributes.getValue ("processRef")
						)
					);
				}
				
				// Il prefisso di elementi BPMN interni ad un "adHocSubProcess", sono gestiti dalla classe "XMLSubProcess"
				// Questa classe determina sia gli "adHocSubProcess" che i "subProcess" dichiarati all'interno del processo.
				else if (qName.equals ("bpmn2:adHocSubProcess")) {
					isMainprocess     = false;
					isSubprocess      = false;
					isAdHocSubProcess = true;
				}
				
				// Il prefisso di elementi BPMN interni ad un "subProcess", sono gestiti dalla classe "XMLSubProcess"
				// Questa classe determina sia i "subProcess" che gli "adHocSubProcess" dichiarati all'interno del processo.
				else if (qName.equals ("bpmn2:subProcess"))
					isSubprocess = true;
			}
			
			
			public void endElement (String uri, String localName, String qName) throws SAXException 
			{
				if (qName.equals ("bpmn2:lane") || qName.equals ("bpmn2:laneSet") || qName.equals ("bpmn2:adHocSubProcess") || qName.equals ("bpmn2:transaction") || qName.equals ("bpmn2:subProcess")) {
					defaultPrefix     = defaultProcess;
					isMainprocess     = true;
					isAdHocSubProcess = false;
				}
				
				if (qName.equals ("bpmn2:subProcess"))
					isSubprocess = false;
			}
			
			
			public void characters (char ch[], int start, int length) throws SAXException 
			{
				if (isFlowNodeRef) {
					String nameElement = new String (ch, start, length);
					int index          = GenericUtils.getIndexBPMNElementById (b, nameElement);
					
					
					prefixNames.set (index, defaultPrefix + "_" + getNameOrIdBPMNElement (b.get (index), mappingRules));
					
					lanesElements.add (nameElement);
					isFlowNodeRef = false;
				}
			}
		};
		
		saxParser.parse (FileUtils.getInputStreamFromPathFile (this.fileName, true), handler);
	    
	    return prefixNames;
	}
	
	
	/**
	 * Prelevo l'id o il name dell'elemento BPMN in base alla regola di mapping
	 * 
	 * @param b   Elemento BPMN
	 * @param mr  Regole di mapping
	 * @return    Valore dell'attributo "id" o valore dell'attributo "name", in base alla regola di mapping
	 */
	private String getNameOrIdBPMNElement (BPMNElement b, ArrayList<MappingRule> mr)
	{
	    // I due id possono essere diversi, ad esempio: idPlace = $id_element, idTransition = $name_element.
	    // Nel caso fossero diversi, diamo precedenza a $name_element
	    String idPlace, idTransition;
	    
	    for (int k = 0, size_mr = mr.size (); k < size_mr; k++) {
	        ArrayList<String> mappedElements = mr.get (k).getElementsMapped ();
	        
	        for (int j = 0, size_me = mappedElements.size (); j < size_me; j++) {
	            if (b.getType ().equals (mappedElements.get (j))) {
	                idPlace      = mr.get (k).getPrule ().getId ();
	                idTransition = mr.get (k).getTrule ().getId ();
	                
	                return (idPlace.equals (idTransition) && idPlace.equals ("$id_element")) ? b.getId () : b.getName ();
	            }
	        }
	    }
	    
	    return null;
	}
}

/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package mapping.parser.xml;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import utils.FileUtils;

/**
 * Parsing del file "managedElements.xml", da cui ricavo tutti i tag XML che vengono gestiti dal plugin.
 * In un'altra classe, se uno di questi tag non viene gestito, il programma si arresta senza eseguire
 * il mapping.
 * 
 * Esempi di tag XML gestiti dal plugin:
 * <elements>
 *	<element>bpmn2:process</element>
 *	<element>bpmn2:message</element>
 *	<element>bpmn2:incoming</element>
 *	<element>bpmn2:outgoing</element>
 *	<!-- ... -->
 * </elements>
 * 
 * @author Marco
 *
 */
public class XMLGetAllManagedElements 
{
	/**
	 * File XML su cui eseguire il parsing (in questo caso puo' essere solamente il file "managedElements.xml")
	 */
	public String fileName;
	
	
	/**
	 * ArrayList dei tag XML gestiti dal plugin
	 */
	private ArrayList<String> result;
	
	
	/**
	 * Sono all'interno del tag <element>. Quindi posso memorizzare valori
	 */
	private boolean isTagElement = false;
	
	
	
	/**
	 * Costruttore
	 * 
	 * @param fileName File XML su cui eseguire il parsing (in questo caso puo' essere solamente il file "managedElements.xml")
	 */
	public XMLGetAllManagedElements (String fileName)
	{
		this.fileName = fileName;
		this.result   = new ArrayList<String> ();
	}
	
	
	/**
	 * Metodo che esegue il parsing del file XML (this.fileName)
	 * 
	 * @return                               ArrayList dei tag XML gestiti dal plugin
	 * @throws SAXException                  Eccezione generata dal metodo newSAXParser ()
	 * @throws ParserConfigurationException  Eccezione generata dal metodo newSAXParser ()
	 * @throws IOException                   Eccezione generata dal metodo parse (...)
	 */
	public ArrayList<String> parse () throws ParserConfigurationException, SAXException, IOException
	{
		SAXParserFactory factory = SAXParserFactory.newInstance ();
		SAXParser saxParser = factory.newSAXParser ();
	 
		DefaultHandler handler = new DefaultHandler () 
		{
			public void startElement (String uri, String localName, String qName, Attributes attributes) throws SAXException 
			{
				isTagElement = (qName.equals ("element")) ? true : false;
			}
			
			public void endElement (String uri, String localName, String qName) throws SAXException 
			{
				isTagElement = (qName.equals ("element")) ? false : isTagElement;
			}
			
			
			public void characters (char ch[], int start, int length) throws SAXException 
			{
				if (isTagElement)
					result.add (new String (ch, start, length));
			}
		};

		saxParser.parse (FileUtils.getInputStreamFromPathFile (this.fileName), handler);
	    
	    return result;
	}
}

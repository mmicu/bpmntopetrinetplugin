/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package mapping.parser.xml;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import utils.FileUtils;

/**
 * Parsing del file "rules.xml", da cui ricavo tutti i tag XML che vengono gestiti dal plugin.
 * Il parser preleva tutti i valori dei tag <element> presenti nel file "rules.xml".
 * In questo modo posso conoscere tutti gli elementi che possono essere mappati con una rete di petri.
 *
 * Esempio di una regola di mapping con i relativi tag <element>:
 *	<rule name="Tasks">
 *		<elements>
 *			<element>bpmn2:task</element>
 *			<element>bpmn2:manualTask</element>
 *			<element>bpmn2:userTask</element>
 *			<element>bpmn2:scriptTask</element>
 *			<element>bpmn2:businessRuleTask</element>
 *			<element>bpmn2:serviceTask</element>
 *			<element>bpmn2:sendTask</element>
 *			<element>bpmn2:receiveTask</element>
 *		</elements>
 *      <!-- ... -->
 *	</rule>
 * 
 * @author Marco
 *
 */
public class XMLMappedElements 
{
	/**
	 * File XML su cui eseguire il parsing (in questo caso puo' essere solamente il file "rules.xml")
	 */
	public String fileName;
	
	
	/**
	 * Se il valore di questo attributo e' settato a "true", durante l'esecuzione del programma, verranno
	 * stampate diverse variabili
	 */
	private boolean debug;
	
	
	/**
	 * Sono all'interno del tag <element>. Quindi posso memorizzare valori di questo tag
	 */
	private boolean isTagElement;
	
	
	/**
	 * ArrayList dei tag XML, degli elementi del BPMN, che vengono mappati dal plugin
	 */
	private ArrayList<String> result;
	
	
	
	/**
	 * Costruttore
	 * 
	 * @param fileName File XML su cui eseguire il parsing (in questo caso puo' essere solamente il file "rules.xml") 
	 */
	public XMLMappedElements (String fileName)
	{
		this.fileName     = fileName;
		this.result       = new ArrayList<String> ();
		this.debug        = false;
		this.isTagElement = false;
	}
	
	
	/**
	 * Costruttore
	 * 
	 * @param fileName File XML su cui eseguire il parsing (in questo caso puo' essere solamente il file "rules.xml") 
	 * @param debug    "true" se si vogliono stampare messaggi di debug, "false" altrimenti
	 */
	public XMLMappedElements (String fileName, boolean debug)
	{
		this (fileName);
		
		this.debug = debug;
	}
	
	
	/**
	 * Metodo che esegue il parsing del file XML (this.fileName)
	 * 
	 * @return                               ArrayList dei tag XML, degli elementi del BPMN, che vengono mappati dal plugin
	 * @throws SAXException                  Eccezione generata dal metodo newSAXParser ()
	 * @throws ParserConfigurationException  Eccezione generata dal metodo newSAXParser ()
	 * @throws IOException                   Eccezione generata dal metodo parse (...)
	 */
	public ArrayList<String> parse () throws ParserConfigurationException, SAXException, IOException
	{
		SAXParserFactory factory = SAXParserFactory.newInstance ();
		SAXParser saxParser = factory.newSAXParser ();
		
	 
		DefaultHandler handler = new DefaultHandler () 
		{
			public void startElement (String uri, String localName, String qName, Attributes attributes) throws SAXException 
			{
				isTagElement = (qName.equals ("element")) ? true : false;
			}
			
			public void endElement (String uri, String localName, String qName) throws SAXException 
			{
				isTagElement = (qName.equals ("element")) ? false : isTagElement;
			}
			
			public void characters (char ch[], int start, int length) throws SAXException 
			{
				if (isTagElement) {
					if (debug)
						System.out.println ("Class \"" + this.getClass ().getName () + "\", element: " + new String (ch, start, length));
					
					result.add (new String (ch, start, length));
				}
			}
		};
		
		saxParser.parse (FileUtils.getInputStreamFromPathFile (this.fileName), handler);
	    
		
	    // In alcuni file BPMN generati con BPMN2Modeler trovati su internet, non viene aggiunto
		// il namespace bpmn2. Quindi controllo che gli elementi mappati abbiano il namespace bpmn2.
		// Se questo namespace e' presente, aggiungo anche il tag senza namespace.
		// Esempio: trovo "bpmn2:startEvent", aggiungo anche "startEvent"
	    ArrayList<String> resultWithoutNamespace = new ArrayList<String> ();
	    for (int k = 0; k < result.size (); k++) {
	    	if (result.get (k).matches ("^bpmn2:.*") || result.get (k).matches ("^ns6:.*")) {
	    		resultWithoutNamespace.add (result.get (k).substring (6, result.get (k).length ()));
	    		resultWithoutNamespace.add ("ns6:" + result.get (k).substring (6, result.get (k).length ()));
	    	}
	    }
	    
	    
      
	    
	    for (int k = 0; k < resultWithoutNamespace.size (); k++)
	    	if (!result.contains (resultWithoutNamespace.get (k)))
	    		result.add (resultWithoutNamespace.get (k));
	    
	    return result;
	}
}

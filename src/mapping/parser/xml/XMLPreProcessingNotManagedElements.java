/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package mapping.parser.xml;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import utils.FileUtils;

/**
 * Questa classe esegue un parsing per controllare che tutti gli elementi dichiarati all'interno del file XML
 * che contiene il BPMN, siano elementi che il plugin sia in grado di gestire.
 * Per controllare eventuali elementi non gestiti dal plugin, viene eseguito un parsing per ottenere tutti i tag
 * gestiti dal plugin (file managedElements) che verranno poi confrontati con i tag contenuti all'interno del file BPMN.
 * Tutti i tag non gestiti vengono restituiti sotto forma di ArrayList dal metodo "parse ()"
 * 
 * @author Marco
 *
 */
public class XMLPreProcessingNotManagedElements 
{
	/**
	 * File BPMN su cui eseguire il parsing
	 */
	private String fileName;
	
	
	/**
	 * Tag XML non gestiti dal plugin
	 */
	private ArrayList<String> result;
	
	
	/**
	 * Se il valore di questo attributo ha valore "true" si procede a controllare gli elementi, se invece risulta essere
	 * uguale a "false" nessun elemento rilevato viene controllato.
	 * Questo avviene perche' nel file XML del BPMN sono dichiarati anche tag inerenti alla posizione dell'elemento
	 * BPMN nell'editor BPMN2Modeler. Questi tag vengono quindi ignorati
	 */
	private boolean check = false;
	
	
	
	/**
	 * Costruttore
	 * 
	 * @param fileName File BPMN su cui eseguire il parsing
	 */
	public XMLPreProcessingNotManagedElements (String fileName)
	{
		this.fileName = fileName;
		this.result   = new ArrayList<String> ();
		this.check    = true;
	}
	
	
	/**
	 * Metodo che esegue il parsing del file XML (this.fileName)
	 * 
	 * @return                               Tag XML non gestiti dal plugin
	 * @throws ParserConfigurationException  Eccezione generata dal metodo newSAXParser () e new XMLGetAllManagedElements (...).parse ()
	 * @throws SAXException                  Eccezione generata dal metodo newSAXParser () e new XMLGetAllManagedElements (...).parse ()
	 * @throws IOException                   Eccezione generata dal metodo parse (...) e new XMLGetAllManagedElements (...).parse ()
	 */
	public ArrayList<String> parse () throws ParserConfigurationException, SAXException, IOException
	{
		final ArrayList<String> managedElements = new XMLGetAllManagedElements ("resources/managedElements.xml").parse ();
		
		SAXParserFactory factory = SAXParserFactory.newInstance ();
		SAXParser saxParser = factory.newSAXParser ();
	 
		DefaultHandler handler = new DefaultHandler () 
		{
			public void startElement (String uri, String localName, String qName, Attributes attributes) throws SAXException 
			{
				if (check && qName.matches ("^bpmn2:.+$") && !managedElements.contains (qName) && !result.contains (qName))
					result.add (qName);
				
				if (qName.equals ("bpmn2:definitions")) 
					check = true;
			}
			
			public void endElement (String uri, String localName, String qName) throws SAXException 
			{
				if (qName.equals ("bpmn2:definitions")) 
					check = true;
			}
		};

			saxParser.parse (FileUtils.getInputStreamFromPathFile (this.fileName, true), handler);
	    
	    return result;
	}
}

/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package mapping.parser.xml;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import utils.FileUtils;
import mapping.data.structure.Pair;

/**
 * Parser XML per i Message Flow, realizzato sul file BPMN
 * 
 * @author Marco
 *
 */
public class XMLMessageFlow 
{
	/**
	 * File BPMN su cui eseguire il parsing
	 */
	private String fileName;
	
	
	/**
	 * Se il valore di questo attributo e' settato a "true", durante l'esecuzione del programma, verranno
	 * stampate diverse variabili
	 */
	private boolean debug;
	
	
	/**
	 * ArrayList che contiene i message flow (id elemento BPMN sorgente, id elemento BPMN destinatario)
	 */
	private ArrayList<Pair <String, String>> result;
	
	
	
	/**
	 * Costruttore
	 * 
	 * @param fileName File BPMN su cui eseguire il parsing
	 */
	public XMLMessageFlow (String fileName)
	{
		this.fileName = fileName;
		this.result   = new ArrayList<Pair <String, String>> ();
		this.debug    = false;
	}
	
	
	/**
	 * Costruttore
	 * 
	 * @param fileName File BPMN su cui eseguire il parsing
	 * @param debug    "true" se si vogliono stampare messaggi di debug, "false" altrimenti
	 */
	public XMLMessageFlow (String fileName, boolean debug)
	{
		this (fileName);
		
		this.debug = debug;
	}
	
	
	/**
	 * Metodo che esegue il parsing del file XML (this.fileName)
	 * 
	 * @return                               ArrayList dei tag XML, degli elementi del BPMN, che vengono mappati dal plugin
	 * 
	 * @throws ParserConfigurationException  Eccezione generata dal metodo newSAXParser ()
	 * @throws SAXException                  Eccezione generata dal metodo newSAXParser ()
	 * @throws IOException                   Eccezione generata dal metodo parse (...)
	 */
	public ArrayList<Pair <String, String>> parse () throws ParserConfigurationException, SAXException, IOException
	{
		SAXParserFactory factory = SAXParserFactory.newInstance ();
		SAXParser saxParser = factory.newSAXParser ();
		
	 
		DefaultHandler handler = new DefaultHandler () 
		{
			public void startElement (String uri, String localName, String qName, Attributes attributes) throws SAXException 
			{
				if (qName.equals ("bpmn2:messageFlow") || qName.equals ("messageFlow")) {
					result.add (Pair.pair (
						attributes.getValue ("sourceRef"),
						attributes.getValue ("targetRef")
					));
					
					if (debug)
						System.out.println ("new Pair (" + attributes.getValue ("sourceRef") + "," + attributes.getValue ("targetRef") + ");");
				}
			}
		};
		
		saxParser.parse (FileUtils.getInputStreamFromPathFile (this.fileName, true), handler);
	    
	    return result;
	}
}

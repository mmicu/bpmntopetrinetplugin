/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package mapping.parser.xml;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import utils.FileUtils;
import mapping.data.structure.SubElements;

/**
 * Questa classe esegue il parsing del file BPMN per conoscere il prefisso di ogni elementi BPMN
 * che hanno come elemento "padre" i seguenti tag:
 *     - subProcess;
 *     - adHocSubProcess;
 *     - subChoreography;
 * 
 * @author Marco
 *
 */
public class XMLSubElements 
{
	/**
	 * File BPMN su cui eseguire il parsing
	 */
	private String fileName;
	
	
	/**
	 * SubElements dichiarati all'interno del BPMN
	 */
	private ArrayList<SubElements> subElements;
	
	
	/**
	 * Sono all'interno del tag <incoming>. Quindi posso memorizzare valori di questo tag
	 */
	private boolean insideIncoming;
	
	
	/**
	 * Sono all'interno del tag <outgoing>. Quindi posso memorizzare valori di questo tag
	 */
	private boolean insideOutgoing;
	
	
	/**
	 * Tag XML dei SubElements (i tag vengono aggiunti nel metodo parse ())
	 */
	private ArrayList<String> tagXMLSubElements;
	
	
	/**
	 * Sono all'interno di un SubElement
	 */
	private boolean insideSubElement;
	
	
	/**
	 * Sono all'interno di un elemento BPMN che posso mappare
	 */
	private boolean insideNode;
	
	
	/**
	 * Valore del tag XML dell'elemento BPMN che posso mappare
	 */
	private String nodeI;
	
	
	/**
	 * Esempio:
	 * <subProcess>               <!-- nRecursivesubElements = 0 -->
	 *     ...
	 *     <subProcess>           <!-- nRecursivesubElements = 1 -->
	 *         ...
	 *         <subProcess>       <!-- nRecursivesubElements = 2 -->
	 *         ...
	 *         </subProcess>
	 *     </subProcess>
	 * </subProcess>
	 */
	private int nRecursivesubElements;
	
	
	
	/**
	 * Costruttore
	 * 
	 * @param fileName File BPMN su cui eseguire il parsing
	 */
	public XMLSubElements (String fileName)
	{
		this.fileName              = fileName;
		this.subElements           = new ArrayList<SubElements> ();
		this.tagXMLSubElements     = new ArrayList<String> ();
		this.nRecursivesubElements = 0;
		this.insideIncoming        = false;
		this.insideOutgoing        = false;
		this.insideSubElement      = false;
		this.insideNode            = false;
	}
	
	
	/**
	 * Metodo che esegue il parsing del file XML (this.fileName)
	 * 
	 * @return                               SubElements dichiarati all'interno del BPMN
	 *
	 * @throws SAXException                  Eccezione generata dal metodo newSAXParser () e XMLMappedElements (...).parse ()
	 * @throws IOException                   Eccezione generata dal metodo parse (...) e XMLMappedElements (...).parse ()
	 * @throws ParserConfigurationException  Eccezione generata dal metodo newSAXParser () e XMLMappedElements (...).parse ()
	 */
	public ArrayList<SubElements> parse (final ArrayList<String> mappedElements) throws SAXException, IOException, ParserConfigurationException
	{
		// Aggiungo i tag XML per i subElements
		this.tagXMLSubElements.add ("bpmn2:subProcess");
		this.tagXMLSubElements.add ("subProcess");
		this.tagXMLSubElements.add ("bpmn2:adHocSubProcess");
		this.tagXMLSubElements.add ("adHocSubProcess");
		this.tagXMLSubElements.add ("bpmn2:subChoreography");
		this.tagXMLSubElements.add ("subChoreography");
		
		// Ottengo tutti i SubProcess del modello inserendo in un apposito ArrayList di tipo SubProcess
		SAXParserFactory factory = SAXParserFactory.newInstance ();
		SAXParser saxParser = factory.newSAXParser ();
	 
		DefaultHandler handler = new DefaultHandler () 
		{
			public void startElement (String uri, String localName, String qName, Attributes attributes) throws SAXException 
			{
				if (tagXMLSubElements.contains (qName))
					subElements.add (new SubElements (
						attributes.getValue ("id"), 
						attributes.getValue ("name"))
					);
			}
		};
		
		saxParser.parse (FileUtils.getInputStreamFromPathFile (this.fileName, true), handler);
	    
	    
	    // Per ogni SubProcess, ottengo tutti gli elementi BPMN, OutgoingFlow e IncomingFlow contenuti al suo interno 
	    for (int k = (subElements.size () - 1); k >= 0; k--) {
	    	final SubElements sI = subElements.get (k);
	    	final int index = k;
	    	nRecursivesubElements = 0;
	    	
			factory = SAXParserFactory.newInstance ();
			saxParser = factory.newSAXParser ();

			handler = new DefaultHandler () 
			{
				public void startElement (String uri, String localName, String qName, Attributes attributes) throws SAXException 
				{
					if (tagXMLSubElements.contains (qName)) {
						if (sI.getId ().equals (attributes.getValue ("id")) && sI.getName ().equals (attributes.getValue ("name")))
							insideSubElement = true;
						else if (insideSubElement)
							nRecursivesubElements++;
					}
					
					
					else if (mappedElements.contains (qName) && insideSubElement && nRecursivesubElements == 0) {
						subElements.get (index).addbpmnElementId (attributes.getValue ("id"));
						insideNode = true;
						nodeI = qName;
					}
					
					else if ((qName.equals ("bpmn2:incoming") || qName.equals ("incoming")) && insideSubElement)
						insideIncoming = true;
					
					else if ((qName.equals ("bpmn2:outgoing") || qName.equals ("outgoing")) && insideSubElement)
						insideOutgoing = true;
					
				}
				
				public void endElement (String uri, String localName, String qName) throws SAXException 
				{
					if (tagXMLSubElements.contains (qName)) {
						if (nRecursivesubElements == 0)
							insideSubElement = false;
						else
							nRecursivesubElements--;
					}
					
					if (qName.equals (nodeI)) {
						insideNode = false;
						insideIncoming = false;
						insideOutgoing = false;
					}
				}
				
				
				public void characters (char ch[], int start, int length) throws SAXException 
				{
					if (insideSubElement && insideIncoming && !insideNode) {
						subElements.get (index).addsequenceFlowIncoming (new String (ch, start, length));
						insideIncoming = false;
					}
					else if (insideSubElement && insideOutgoing && !insideNode) {
						subElements.get (index).addsequenceFlowOutgoing (new String (ch, start, length));
						insideOutgoing = false;
					}
				}
			};
			
			saxParser.parse (FileUtils.getInputStreamFromPathFile (this.fileName, true), handler);
	    }
	    
	    return subElements;
	}
}

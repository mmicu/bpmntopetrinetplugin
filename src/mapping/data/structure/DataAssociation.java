/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package mapping.data.structure;

/**
 * Questa classe viene utilizzata per semplificare il parsing XML dei Data Object.
 * Infatti i Data Object sono dichiarati all'interno del tag dell'elemento BPMN a cui sono
 * collegati. Ecco un esempio:
 * 
 * <bpmn2:task id="Task_1" name="Task 1">
 *    ...
 *    <bpmn2:dataInputAssociation id="DataInputAssociation_1">
 *      <bpmn2:sourceRef>DataObject_1</bpmn2:sourceRef>
 *      <bpmn2:targetRef>DataInput_1</bpmn2:targetRef>
 *    </bpmn2:dataInputAssociation>
 *    ...
 *    <bpmn2:dataOutputAssociation id="DataOutputAssociation_2">
 *      <bpmn2:sourceRef>DataOutput_3</bpmn2:sourceRef>
 *      <bpmn2:targetRef>DataOutput_2</bpmn2:targetRef>
 *    </bpmn2:dataOutputAssociation>
 * </bpmn2:task>
 * 
 * Se il Data Object e' un "dataInputAssociation", dobbiamo prelevare il valore del tag figlio "sourceRef" (supponiamo X)
 * e cercare nell'XML un Data Object con id X.
 * 
 * Se il Data Object e' un "dataOutputAssociation", dobbiamo prelevare il valore del tag figlio "targetRef" (supponiamo Y)
 * e cercare nell'XML un Data Object con id Y.
 * 
 * @author Marco
 *
 */
public class DataAssociation 
{
    /**
     * Tag root del dataAssociation. Questo valore sara' uguale ad un tag XML di elementi che possono essere mappati.
     * Se prendiamo l'esempio precedente, rootTag = "bpmn2:task"
     */
    private String rootTag;
    
    
    /**
     * Id del tag root
     * Se prendiamo l'esempio precedente, rootId = "Task_1"
     */
    private String rootId;
    
    
    /**
     * Nome del tag root
     * Se prendiamo l'esempio precedente, rootName = "Task 1"
     */
    private String rootName;
    
    
    /**
     * Tag del dataAssociation. Due valori possibili:
     *     - bpmn2:dataInputAssociation
     *     - bpmn2:dataOutputAssociation
     */
    private String tag;
    
    
    /**
     * Valore del tag figlio "bpmn2:sourceRef" del nodo root "bpmn2:dataInputAssociation" o "bpmn2:dataOutputAssociation"
     */
    private String idSource;
    
    
    /**
     * Valore del tag figlio "bpmn2:targetRef" del nodo root "bpmn2:dataInputAssociation" o "bpmn2:dataOutputAssociation"
     */
    private String idTarget;
    
    
    /**
     * Una volta trovato il Data Object riferito al dataAssociation, aggiungo l'istanza dell'oggetto "Data Object"
     */
    private DataObject refDataObject;
    
    
    
    /**
     * Costruttore
     */
    public DataAssociation ()
    {
    }
    
    
    /**
     * Costruttore
     * 
     * @param tag Tag del dataAssociation (valori possibili: "bpmn2:dataInputAssociation" e "bpmn2:dataOutputAssociation")
     */
    public DataAssociation (String tag)
    {
        this.tag = tag;
    }
    
    
    /**
     * Ottengo il valore dell'attributo rootTag
     * 
     * @return Valore dell'attributo rootTag
     */
    public String getRootTag () 
    {
        return this.rootTag;
    }


    /**
     * Modifica del valore dell'attributo rootTag
     * 
     * @param rootTag Nuovo valore dell'attributo rootTag
     */
    public void setRootTag (String rootTag) 
    {
        this.rootTag = rootTag;
    }


    /**
     * Ottengo il valore dell'attributo rootId
     * 
     * @return Valore dell'attributo rootId
     */
    public String getRootId () 
    {
        return this.rootId;
    }


    /**
     * Modifica del valore dell'attributo rootId
     * 
     * @param rootId Nuovo valore dell'attributo rootId
     */
    public void setRootId (String rootId) 
    {
        this.rootId = rootId;
    }


    /**
     * Ottengo il valore dell'attributo rootName
     * 
     * @return Valore dell'attributo rootName
     */
    public String getRootName () 
    {
        return this.rootName;
    }


    /**
     * Modifica del valore dell'attributo rootName
     * 
     * @param rootName Nuovo valore dell'attributo rootName
     */
    public void setRootName (String rootName) 
    {
        this.rootName = rootName;
    }


    /**
     * Ottengo il valore dell'attributo tag
     * 
     * @return Valore dell'attributo tag
     */
    public String getTag () 
    {
        return this.tag;
    }

    
    /**
     * Modifica del valore dell'attributo tag
     * 
     * @param tag Nuovo valore dell'attributo tag
     */
    public void setTag (String tag) 
    {
        this.tag     = tag;
    }

    
    /**
     * Ottengo il valore dell'attributo idSource
     * 
     * @return Valore dell'attributo idSource
     */
    public String getIdSource () 
    {
        return this.idSource;
    }

    
    /**
     * Modifica del valore dell'attributo idSource
     * 
     * @param idSource Nuovo valore dell'attributo idSource
     */
    public void setIdSource (String idSource) 
    {
        this.idSource = idSource;
    }

    
    /**
     * Ottengo il valore dell'attributo idTarget
     * 
     * @return Valore dell'attributo idTarget
     */
    public String getIdTarget () 
    {
        return this.idTarget;
    }

    
    /**
     * Modifica del valore dell'attributo idTarget
     * 
     * @param idTarget Nuovo valore dell'attributo idTarget
     */
    public void setIdTarget (String idTarget) 
    {
        this.idTarget = idTarget;
    }

    
    /**
     * Controllo se il data object sia di tipo "input"
     * 
     * @return "true" se il data object e' un "input", "false" altrimenti
     */
    public boolean isInputAssociation ()
    {
        return tag.equals ("bpmn2:dataInputAssociation") || tag.equals ("dataInputAssociation");
    }
    
    
    /**
     * Ottengo il valore dell'attributo refDataObject
     * 
     * @return Valore dell'attributo refDataObject
     */
    public DataObject getRefDataObject ()
    {
        return this.refDataObject;
    }
    
    
    /**
     * Modifica del valore dell'attributo refDataObject
     * 
     * @param refDataObject Nuovo valore dell'attributo refDataObject
     */
    public void setRefDataObject (DataObject refDataObject)
    {
        this.refDataObject = refDataObject;
    }
    
    
    /*
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString () 
    {
        return "DataAssociation [rootTag=" + this.rootTag + ", rootId=" + this.rootId
                + ", rootName=" + this.rootName + ", tag=" + this.tag + ", idSource="
                + this.idSource + ", idTarget=" + this.idTarget + "]";
    }
}

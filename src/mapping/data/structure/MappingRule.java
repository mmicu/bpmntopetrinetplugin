/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package mapping.data.structure;

import java.util.ArrayList;

import mapping.rules.*;

/**
 * Questa classe rappresenta le regole dichiarate all'interno del file "rules.xml".
 * Esempio di una regola:
 * 
 * <rule name="Start event">
 *      <elements>
 *          <element>bpmn2:startEvent</element>
 *          <element>bpmn2:intermediateThrowEvent</element>
 *          <element>bpmn2:intermediateCatchEvent</element>
 *      </elements>
 *      <mapping>
 *          <place id="$id_element" />
 *          <transition id="$id_element" n="$n_outgoing" />
 *          <pts>
 *              <pt place="1" transition="[1..$n_outgoing]" />
 *          </pts>
 *      </mapping>
 *  </rule>
 * 
 * @author Marco
 *
 */
public class MappingRule
{
    /**
     * Nome della regola
     * Nel caso del precedente esempio, ruleName = "Start event"
     */
    private String ruleName;
    
    
    /**
     * Tag XML dell'elemento BPMN che puo' essere mappato.
     * Nel caso del precedente esempio, elementsMapped = ["bpmn2:startEvent", "bpmn2:intermediateThrowEvent", "bpmn2:intermediateCatchEvent"]
     */
    private ArrayList<String> elementsMapped;
    
    
    /**
     * Regola di mapping per le piazze
     */
    private PlaceRule pRule;
    
    
    /**
     * Regola di mapping per le transizioni
     */
    private TransitionRule tRule;
    
    
    /**
     * Regola di mapping per i collegamenti PT (Place to Transition)
     */
    private PTRule ptRule;
    

    /**
     * Regola di mapping per i collegamenti TP (Transition to Place)
     */
    private TPRule tpRule;
    
    
    /**
     * Per i collegamenti con i place degli elementi BPMN collegati al BPMN in esame, attraverso questo attributo
     * possiamo specificare la transizione iniziale per questi collegamenti.
     * Questo attributo puo' essere:
     *  - un numero;
     *  - una keyword;
     *  - un'espressione (che puo' contenere keyword).
     * Esempio: se l'elemento BPMN in esame ha 5 transizione e "initialTransition = 3", significa che i collegamenti
     *          TP inizieranno dalla terza transizione
     */
    private String initialTransition;
    
    
    /**
     * Se il valore di questo attributo e' "true", nei collegamenti TP seleziono sempre la transizione in base
     * al valore di "initialTransition" (ad esempio il parallel gateway seleziona sempre la transizione numero 1)
     */
    private boolean isUnique;
    
    
    
    /**
     * Costruttore
     * 
     * @param ruleName Nome della regola
     */
    public MappingRule (String ruleName)
    {
        this.ruleName          = ruleName;
        this.elementsMapped    = new ArrayList<String> ();
        this.isUnique          = false;
    }
    
    
    /**
     * Metodo che aggiunge il tag XML dell'elemento BPMN che puo' essere mappato
     * 
     * @param element Tag XML dell'elemento BPMN che puo' essere mappato
     */
    public void addElementMapped (String element)
    {
        this.elementsMapped.add (element);
    }
    
    
    /**
     * Ottengo il valore dell'attributo ruleName
     * 
     * @return Valore dell'attributo ruleName
     */
    public String getRuleName () 
    {
        return ruleName;
    }

    
    /**
     * Modifica del valore dell'attributo ruleName
     * 
     * @param ruleName Nuovo valore dell'attributo ruleName
     */
    public void setRuleName (String ruleName) 
    {
        this.ruleName = ruleName;
    }

    
    /**
     * Ottengo il valore dell'attributo elementsMapped
     * 
     * @return Valore dell'attributo elementsMapped
     */
    public ArrayList<String> getElementsMapped () 
    {
        return this.elementsMapped;
    }
    

    /**
     * Modifica del valore dell'attributo elementsMapped
     * 
     * @param elementsMapped Nuovo valore dell'attributo elementsMapped
     */
    public void setElementsMapped (ArrayList<String> elementsMapped) 
    {
        this.elementsMapped = elementsMapped;
    }

    
    /**
     * Ottengo il valore dell'attributo pRule
     * 
     * @return Valore dell'attributo pRule
     */
    public PlaceRule getPrule () 
    {
        return this.pRule;
    }

    
    /**
     * Modifica del valore dell'attributo pRule
     * 
     * @param pRule Nuovo valore dell'attributo pRule
     */
    public void setPrule (PlaceRule pRule) 
    {
        this.pRule = pRule;
    }

    
    /**
     * Ottengo il valore dell'attributo tRule
     * 
     * @return Valore dell'attributo tRule
     */
    public TransitionRule getTrule () 
    {
        return this.tRule;
    }

    
    /**
     * Modifica del valore dell'attributo tRule
     * 
     * @param tRule Nuovo valore dell'attributo tRule
     */
    public void setTrule (TransitionRule tRule) 
    {
        this.tRule = tRule;
    }

    
    /**
     * Ottengo il valore dell'attributo ptRule
     * 
     * @return Valore dell'attributo ptRule
     */
    public PTRule getPTrule () 
    {
        return this.ptRule;
    }

    
    /**
     * Modifica del valore dell'attributo ptRule
     * 
     * @param ptRule Nuovo valore dell'attributo ptRule
     */
    public void setPTrule (PTRule ptRule) 
    {
        this.ptRule = ptRule;
    }

    
    /**
     * Ottengo il valore dell'attributo tpRule
     * 
     * @return Valore dell'attributo tpRule
     */
    public TPRule getTPrule () 
    {
        return this.tpRule;
    }

    
    /**
     * Modifica del valore dell'attributo tpRule
     * 
     * @param tpRule Nuovo valore dell'attributo tpRule
     */
    public void setTPrule (TPRule tpRule) 
    {
        this.tpRule = tpRule;
    }
    
    
    /**
     * Ottengo il valore dell'attributo initialTransition
     * 
     * @return Valore dell'attributo initialTransition
     */
    public String getInitialTransition () 
    {
        return this.initialTransition;
    }

    
    /**
     * Modifica del valore dell'attributo initialTransition
     * 
     * @param initialTransition Nuovo valore dell'attributo initialTransition
     */
    public void setInitialTransition (String initialTransition) 
    {
        this.initialTransition = initialTransition;
    }

    
    /**
     * Ottengo il valore dell'attributo isUnique
     * 
     * @return Valore dell'attributo isUnique
     */
    public boolean isUnique () 
    {
        return this.isUnique;
    }

    
    /**
     * Modifica del valore dell'attributo isUnique
     * 
     * @param isUnique Nuovo valore dell'attributo isUnique
     */
    public void setUnique (boolean isUnique) 
    {
        this.isUnique = isUnique;
    }


    /*
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString () 
    {
        return "MappingRule [ruleName=" + this.ruleName + ", elementsMapped=" + this.elementsMapped 
                + ", pRule="  + (this.pRule == null  ? "null" : this.pRule.toString ()) 
                + ", tRule="  + (this.tRule == null  ? "null" : this.tRule.toString ()) 
                + ", ptRule=" + (this.ptRule == null ? "null" : this.ptRule.toString ()) 
                + ", tpRule=" + (this.tpRule == null ? "null" : this.tpRule.toString ())
                + ", initialTransition=" + this.initialTransition + ", isUnique=" + this.isUnique + "]";
    }
}

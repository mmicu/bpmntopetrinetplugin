/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package mapping.data.structure;

import java.util.ArrayList;

/**
 * Porting dello script "pep2dot"
 * 
 * @author Marco
 *
 */
public class PEP2DOT 
{
    /**
     * Colore di default della piazza con il token
     */
    public static final String DEFAULT_COLOR_INITIAL_MARKING = "red";
    
    
    /**
     * Colore di default della piazza/transizione presente nella traccia di deadlock
     */
    public static final String DEFAULT_COLOR_TRACE_DEADLOCK = "yellow";
    
    
    /**
     * Grandezza font
     */
    public static final int FONT_SIZE = 4;
    
    
    
    /**
     * Metodo che realizza la trasformazione ll_net -> dot
     * 
     * @return Rete di Petri nel formato "dot"
     */
    /**
     * @param petriNet
     * @return
     */
    public static String getDotFormat (PEP petriNet)
    {
        return getDotFormat (petriNet, new ArrayList<String> ());
    }
    
    
    /**
     * @param petriNet
     * @param trace
     * @return
     */
    public static String getDotFormat (PEP petriNet, ArrayList<String> trace)
    {
       String dot = "digraph {\n\t/* places */\n";
        
        /**
         * Places
         */
        ArrayList<Pair <String, Boolean>> p = petriNet.getP ();
        
        // Se la prima piazza della rete di Petri e' presente nel trace di deadlock, imposto il colore della piazza a 'DEFAULT_COLOR_TRACE_DEADLOCK'
        if (p.size () > 0 && PEP2DOT.traceContainsElement (trace, p.get (0).getFirstElement ()))
            dot += "\tnode    [style=filled fillcolor=" + PEP2DOT.DEFAULT_COLOR_TRACE_DEADLOCK + " shape=circle fontsize=" + PEP2DOT.FONT_SIZE + "];\n";
        // Se la prima piazza della rete di Petri ha un token, imposto il colore della piazza a 'DEFAULT_COLOR_INITIAL_MARKING'
        else
            dot += (p.size () > 0 && p.get (0).getSecondElement ().booleanValue ()) 
                       ? "\tnode    [style=filled fillcolor=" + PEP2DOT.DEFAULT_COLOR_INITIAL_MARKING + " shape=circle fontsize=" + PEP2DOT.FONT_SIZE + "];\n" 
                       : "\tnode    [style=filled fillcolor=gray90 shape=circle fontsize=" + PEP2DOT.FONT_SIZE + "];\n";
        
        for (int k = 0; k < p.size (); k++) {
            if (k >= 1) {
                // Se la piazza della rete di Petri e' presente nel trace di deadlock, imposto il colore della piazza a 'DEFAULT_COLOR_TRACE_DEADLOCK'
                if (PEP2DOT.traceContainsElement (trace, p.get (k).getFirstElement ()))
                    dot += "\tnode    [style=filled fillcolor=fillcolor=" + PEP2DOT.DEFAULT_COLOR_TRACE_DEADLOCK + " shape=circle fontsize=" + PEP2DOT.FONT_SIZE + "];\n";
                else
                    dot += (p.size () > 0 && p.get (k).getSecondElement ().booleanValue ()) ? "\tnode    [style=filled fillcolor=red shape=circle fontsize=" + PEP2DOT.FONT_SIZE + "];\n" 
                                                                                            : "\tnode    [style=filled fillcolor=gray90 shape=circle fontsize=" + PEP2DOT.FONT_SIZE + "];\n";
            }
            
            dot += "\t" + p.get (k).getFirstElement () + " [label=\"" + p.get (k).getFirstElement () + "\" fontsize=" + PEP2DOT.FONT_SIZE + "];" + 
                   ((p.get (k).getSecondElement ().booleanValue ()) ? " /* initial */" : "") + "\n";
        }
        
        
        /**
         * Transitions
         */
        ArrayList<String> t = petriNet.getT ();
        
        dot += "\n\t/* transitions */\n";
        for (int k = 0; k < t.size (); k++) {
            // Se la transizione della rete di Petri e' presente nel trace di deadlock, imposto il colore della transizione a 'DEFAULT_COLOR_TRACE_DEADLOCK'
            if (PEP2DOT.traceContainsElement (trace, t.get (k)))
                dot += "\tnode    [style=filled fillcolor=" + PEP2DOT.DEFAULT_COLOR_TRACE_DEADLOCK + " shape=box fontsize=" + PEP2DOT.FONT_SIZE + "];\n";
            else
                dot += "\tnode    [shape=box style=filled fillcolor=grey60 fontsize=" + PEP2DOT.FONT_SIZE + "];\n";
            
            dot += "\t" + t.get (k) + " [label=\"" + t.get (k) + "\" fontsize=" + PEP2DOT.FONT_SIZE + "];\n";
        }
        
        
        /**
         * Transitions to Places
         */
        ArrayList<Pair <String, String>> tp = petriNet.getTP ();
        
        dot += "\n\t/* postset of each transition */\n";
        for (int k = 0; k < tp.size (); k++)
            dot += "\t" + tp.get (k).getFirstElement () + " -> " + tp.get (k).getSecondElement () + "\n";
        
        
        /**
         * Places to Transitions
         */
        ArrayList<Pair <String, String>> pt = petriNet.getPT ();
        
        dot += "\n\t/* preset and context of each transition */\n";
        for (int k = 0; k < pt.size (); k++)
            dot += "\t" + pt.get (k).getFirstElement () + " -> " + pt.get (k).getSecondElement () + "\n";
        

        dot += "\n";
        dot += "\tgraph   [label=\"" + t.size () + " transitions\\n" + p.size () + " places\"];\n";
        dot += "}\n";
        
        
        return dot;
    }
    
    
    /**
     * Metodo che controlla se la piazza o la transizione e' presente nel trace di deadlock
     * 
     * @param trace    Trace di deadlock
     * @param element  Id della piazza/transizione
     * @return         "true" se l'elemento e' contenuto nella traccia, "false" altrimenti
     */
    private static boolean traceContainsElement (ArrayList<String> trace, String element)
    {
        for (int k = 0, size = trace.size (); k < size; k++)
            if (trace.get (k).equals (element) || trace.get (k).matches (".*" + element + ".*") || trace.get (k).equals (element.replace ("_", "")))
                return true;
        
        return false;
    }
}

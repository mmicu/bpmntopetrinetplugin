/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package mapping.data.structure;

import java.util.ArrayList;

/**
 * Questa classe rappresenta elementi BPMN "padri" che racchiudono altri elementi BPMN "figli".
 * Nel nostro caso, i elementi BPMN "padri" sono:
 *     - subProcess;
 *     - adHocSubProcess;
 *     - subChoreography;
 * 
 * @author Marco
 *
 */
public class SubElements 
{
    /**
     * Id del SubElement
     */
    private String id;
    
    
    /**
     * Nome del SubElement
     */
    private String name;
    
    
    /**
     * Id degli elementi BPMN "figli" del SubElement
     */
    private ArrayList<String> bpmnElementsId;
    
    
    /**
     * Id delle "sequence incoming flow" SubElement
     */
    private ArrayList<String> sequenceFlowIncoming;
    
    
    /**
     * Id delle "sequence outgoing flow" SubElement
     */
    private ArrayList<String> sequenceFlowOutgoing;
    
    
    
    /**
     * Costruttore
     */
    public SubElements () 
    {
        this.bpmnElementsId       = new ArrayList<String> ();
        this.sequenceFlowIncoming = new ArrayList<String> ();
        this.sequenceFlowOutgoing = new ArrayList<String> ();
    }
    
    
    /**
     * Costruttore
     * 
     * @param id Id del SubElement
     */
    public SubElements (String id)
    {
        this ();
        
        this.id = id;
    }
    
    
    /**
     * Costruttore
     * 
     * @param id   Id del SubElement
     * @param name Nome del SubElement
     */
    public SubElements (String id, String name)
    {
        this (id);
        
        this.name = name;
    }
    
    
    /**
     * Aggiungo l'id di un elemento BPMN al SubElement
     * 
     * @param bpmnElementId Id dell'elemento BPMN da aggiungere al SubElement
     */
    public void addbpmnElementId (String bpmnElementId)
    {
        this.bpmnElementsId.add (bpmnElementId);
    }
    
    
    /**
     * Aggiungo l'id del sequence flow in ingresso all'ArrayList che contiene tutte le sequence flow in ingresso
     * 
     * @param sequenceFlowIncoming Id del sequence flow in ingresso
     */
    public void addsequenceFlowIncoming (String sequenceFlowIncoming)
    {
        this.sequenceFlowIncoming.add (sequenceFlowIncoming);
    }
    
    
    /**
     * Aggiungo l'id del sequence flow in uscita all'ArrayList che contiene tutte le sequence flow in uscita
     * 
     * @param sequenceFlowOutgoing Id del sequence flow in uscita
     */
    public void addsequenceFlowOutgoing (String sequenceFlowOutgoing)
    {
        this.sequenceFlowOutgoing.add (sequenceFlowOutgoing);
    }


    /**
     * Ottengo il valore dell'attributo id
     * 
     * @return Valore dell'attributo id
     */
    public String getId () 
    {
        return this.id;
    }

    
    /**
     * Modifica del valore dell'attributo id
     * 
     * @param id Nuovo valore dell'attributo id
     */
    public void setId (String id) 
    {
        this.id = id;
    }


    /**
     * Ottengo il valore dell'attributo name
     * 
     * @return Valore dell'attributo name
     */
    public String getName () 
    {
        return this.name;
    }

    
    /**
     * Modifica del valore dell'attributo name
     * 
     * @param name Nuovo valore dell'attributo name
     */
    public void setName (String name) 
    {
        this.name = name;
    }


    /**
     * Ottengo il valore dell'attributo bpmnElementsId
     * 
     * @return Valore dell'attributo bpmnElementsId
     */
    public ArrayList<String> getBpmnElementsId () 
    {
        return this.bpmnElementsId;
    }

    
    /**
     * Modifica del valore dell'attributo bpmnElementsId
     * 
     * @param bpmnElementsId Nuovo valore dell'attributo bpmnElementsId
     */
    public void setBpmnElementsId (ArrayList<String> bpmnElementsId) 
    {
        this.bpmnElementsId = bpmnElementsId;
    }


    /**
     * Ottengo il valore dell'attributo sequenceFlowIncoming
     * 
     * @return Valore dell'attributo sequenceFlowIncoming
     */
    public ArrayList<String> getSequenceFlowIncoming () 
    {
        return this.sequenceFlowIncoming;
    }

    
    /**
     * Modifica del valore dell'attributo sequenceFlowIncoming
     * 
     * @param sequenceFlowIncoming Nuovo valore dell'attributo sequenceFlowIncoming
     */
    public void setSequenceFlowIncoming (ArrayList<String> sequenceFlowIncoming) 
    {
        this.sequenceFlowIncoming = sequenceFlowIncoming;
    }


    /**
     * Ottengo il valore dell'attributo sequenceFlowOutgoing
     * 
     * @return Valore dell'attributo sequenceFlowOutgoing
     */
    public ArrayList<String> getSequenceFlowOutgoing () 
    {
        return this.sequenceFlowOutgoing;
    }

    
    /**
     * Modifica del valore dell'attributo sequenceFlowOutgoing
     * 
     * @param sequenceFlowOutgoing Nuovo valore dell'attributo sequenceFlowOutgoing
     */
    public void setSequenceFlowOutgoing (ArrayList<String> sequenceFlowOutgoing) 
    {
        this.sequenceFlowOutgoing = sequenceFlowOutgoing;
    }


    /*
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString () 
    {
        return "SubProcess [id=" + this.id + ", name=" + this.name + ", bpmnElementsId="
                + this.bpmnElementsId + ", sequenceFlowIncoming (" + this.sequenceFlowIncoming.size () + ")="
                + this.sequenceFlowIncoming + ", sequenceFlowOutgoing (" + this.sequenceFlowOutgoing.size() + ")="
                + this.sequenceFlowOutgoing + "]";
    }
}

/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package mapping.data.structure;

import java.util.ArrayList;

/**
 * Questa classe rappresenta l'entita' BPMN.
 * 
 * @author Marco
 *
 */
public class BPMNElement 
{
    /**
     * Tipo di elemento BPMN. Esempio: bpmn2:startEvent, startEvent, bpmn2:task ecc.
     */
    private String type;
    
    
    /**
     * Id dell'elemento BPMN (attributo "id" del tag dell'elemento dichiarato all'interno del file .bpmn)
     */
    private String id;
    
    
    /**
     * Nuovo id dell'elemento BPMN. Il nuovo id deriva dall'attributo "id" e dall'appartenenza dell'elemento BPMN
     * (subprocess, lane, pool ecc.)
     */
    private String renamedID;
    
    
    /**
     * Nome dell'elemento BPMN (attributo "name" del tag dell'elemento dichiarato all'interno del file .bpmn)
     */
    private String name;
    
    
    /**
     * Nuovo nome dell'elemento BPMN. Il nuovo id deriva dall'attributo "name" e dall'appartenenza dell'elemento BPMN
     * (subprocess, lane, pool ecc.)
     */
    private String renamedName;
    
    
    /**
     * Se l'elemento BPMN e' di tipo "event" (start, end ecc.), puo' avere un "event definition".
     * Ci sono diversi tipi di "event definition" come ad esempio: "bpmn2:timerEventDefinition", "bpmn2:messageEventDefinition" ecc.
     */
    private String eventDefinition;
    
    
    /**
     * Se l'elemento BPMN e' di tipo "bpmn2:choreographyTask", puo' avere uno o piu' partecipanti.
     */
    private ArrayList<String> participantName;
    
    
    /**
     * Id delle "sequence incoming flow" dell'elemento BPMN (frecce entranti all'elemento BPMN)
     */
    private ArrayList<String> sequenceFlowIncoming;
    
    
    /**
     * Id delle "sequence outgoing flow" dell'elemento BPMN (frecce uscenti all'elemento BPMN)
     */
    private ArrayList<String> sequenceFlowOutgoing;
    
    
    /**
     * Lista di elementi BPMN collegati, in ingresso, con il "message flow", all'elemento BPMN
     */
    private ArrayList<BPMNElement> refIncomingMessageFlow;
    
    
    /**
     * Lista di elementi BPMN collegati, in uscita, con il "message flow", all'elemento BPMN
     */
    private ArrayList<BPMNElement> refOutgoingMessageFlow;
    
    
    /**
     * Lista di "data object collegati, in ingresso, con il "data flow", all'elemento BPMN
     */
    private ArrayList<DataObject> incomingDataFlow;
    
    
    /**
     * Lista di "data object collegati, in uscita, con il "data flow", all'elemento BPMN
     */
    private ArrayList<DataObject> outgoingDataFlow;
    
    
    /**
     * Rete di Petri dell'elemento BPMN
     */
    private PEP petriNet;
    
    
    /**
     * Id del nodo a cui l'elemento BPMN fa parte:
     *  - subprocess;
     *  - adhocsubprocess;
     *  - subchoreography.
     */
    private String idSubElement;
    
    
    
    /**
     * Costruttore
     */
    public BPMNElement () 
    {
        this.participantName        = new ArrayList<String> ();
        this.sequenceFlowIncoming   = new ArrayList<String> ();
        this.sequenceFlowOutgoing   = new ArrayList<String> ();
        this.refIncomingMessageFlow = new ArrayList<BPMNElement> ();
        this.refOutgoingMessageFlow = new ArrayList<BPMNElement> ();
        this.incomingDataFlow       = new ArrayList<DataObject> ();
        this.outgoingDataFlow       = new ArrayList<DataObject> ();
        this.petriNet               = new PEP ();
    }
    
    
    /**
     * Costruttore
     * 
     * @param type Tipo di elemento BPMN. Esempio: bpmn2:startEvent, startEvent, bpmn2:task ecc.
     */
    public BPMNElement (String type)
    {
        this ();
        
        this.type = type;
    }
    
    
    /**
     * Costruttore
     * 
     * @param type Tipo di elemento BPMN. Esempio: bpmn2:startEvent, startEvent, bpmn2:task ecc.
     * @param id   Id dell'elemento BPMN
     * @param name Nome dell'elemento BPMN
     */
    public BPMNElement (String type, String id, String name)
    {
        this (type);
        
        this.id        = id;
        this.name      = name;
    }
    
    
    /**
     * Aggiungo l'id del sequence flow in ingresso all'ArrayList che contiene tutte le sequence flow in ingresso
     * 
     * @param sequenceFlowIncoming Id del sequence flow in ingresso
     */
    public void addParticipant (String participant)
    {
        this.participantName.add (participant);
    }


    /**
     * Aggiungo l'id del sequence flow in ingresso all'ArrayList che contiene tutte le sequence flow in ingresso
     * 
     * @param sequenceFlowIncoming Id del sequence flow in ingresso
     */
    public void addsequenceFlowIncoming (String sequenceFlowIncoming)
    {
        if (!this.sequenceFlowIncoming.contains (sequenceFlowIncoming))
            this.sequenceFlowIncoming.add (sequenceFlowIncoming);
    }
    
    
    /**
     * Aggiungo l'id del sequence flow in uscita all'ArrayList che contiene tutte le sequence flow in uscita
     * 
     * @param sequenceFlowOutgoing Id del sequence flow in uscita
     */
    public void addsequenceFlowOutgoing (String sequenceFlowOutgoing)
    {
        if (!this.sequenceFlowOutgoing.contains (sequenceFlowOutgoing))
            this.sequenceFlowOutgoing.add (sequenceFlowOutgoing);
    }
    
    
    /**
     * Aggiungo un elemento BPMN all'ArrayList che contiene tutti gli elementi BPMN in ingresso con il message flow
     * 
     * @param b Elemento BPMN
     */
    public void addRefIncomingMessageFlow (BPMNElement b)
    {
        this.refIncomingMessageFlow.add (b);
    }
    
    
    /**
     * Aggiungo un elemento BPMN all'ArrayList che contiene tutti gli elementi BPMN in uscita con il message flow
     * 
     * @param b Elemento BPMN
     */
    public void addRefOutgoingMessageFlow (BPMNElement b)
    {
        this.refOutgoingMessageFlow.add (b);
    }
    
    
    /**
     * Aggiungo un "data object" all'ArrayList che contiene tutti i data object in ingresso all'elemento BPMN
     * 
     * @param d Data object in ingresso
     */
    public void addIncomingDataFlow (DataObject d)
    {
        this.incomingDataFlow.add (d);
    }
    
    
    /**
     * Aggiungo un "data object" all'ArrayList che contiene tutti i data object in uscita all'elemento BPMN
     * 
     * @param d Data object in uscita
     */
    public void addOutgoingDataFlow (DataObject d)
    {
        this.outgoingDataFlow.add (d);
    }
    
    
    /**
     * Metodo che controlla se l'elemento e' uno "start event"
     * 
     * @return "true" se l'elemento BPMN e' uno "start event", "false" altrimenti
     */
    public boolean isStartEvent ()
    {
        return this.type.equals ("bpmn2:startEvent") || this.type.equals ("startEvent");
    }
    
    
    /**
     * Metodo che controlla se l'elemento e' un "end event"
     * 
     * @return "true" se l'elemento BPMN e' un "end event", "false" altrimenti
     */
    public boolean isEndEvent ()
    {
        return this.type.equals ("bpmn2:endEvent") || this.type.equals ("endEvent");
    }
    
    
    /**
     * Metodo che controlla se l'elemento e' un "task"
     * 
     * @return "true" se l'elemento BPMN e' un "task", "false" altrimenti
     */
    public boolean isTask ()
    {
        return this.type.matches ("(?i).*task.*"); // (?i) = case-insensitive matching
    }

    
    /**
     * Ottengo il valore dell'attributo type
     * 
     * @return Valore dell'attributo type
     */
    public String getType () 
    {
        return this.type;
    }

    
    /**
     * Modifica del valore dell'attributo type
     * 
     * @param type Nuovo valore dell'attributo type
     */
    public void setType (String type) 
    {
        this.type = type;
    }


    /**
     * Ottengo il valore dell'attributo id
     * 
     * @return Valore dell'attributo id
     */
    public String getId () 
    {
        return this.id;
    }

    
    /**
     * Modifica del valore dell'attributo id
     * 
     * @param id Nuovo valore dell'attributo id
     */
    public void setId (String id) 
    {
        this.id = id;
    }
    
    
    /**
     * Ottengo il valore dell'attributo renamedID
     * 
     * @return Valore dell'attributo renamedID
     */
    public String getRenamedID () 
    {
        return this.renamedID;
    }

    
    /**
     * Modifica del valore dell'attributo renamedID
     * 
     * @param renamedID Nuovo valore dell'attributo renamedID
     */
    public void setRenamedID (String renamedID) 
    {
        this.renamedID = renamedID;
    }

    
    /**
     * Ottengo il valore dell'attributo name
     * 
     * @return Valore dell'attributo name
     */
    public String getName () 
    {
        return this.name;
    }

    
    /**
     * Modifica del valore dell'attributo name
     * 
     * @param name Nuovo valore dell'attributo name
     */
    public void setName (String name) 
    {
        this.name = name;
    }
    

    /**
     * Ottengo il valore dell'attributo renamedName
     * 
     * @return Valore dell'attributo renamedName
     */
    public String getRenamedName ()
    {
        return this.renamedName;
    }

    
    /**
     * Modifica del valore dell'attributo renamedName
     * 
     * @param renamedName Nuovo valore dell'attributo renamedName
     */
    public void setRenamedName (String renamedName)
    {
        this.renamedName = renamedName;
    }


    /**
     * Ottengo il valore dell'attributo eventDefinition
     * 
     * @return Valore dell'attributo eventDefinition
     */
    public String getEventDefinition () 
    {
        return this.eventDefinition;
    }

    
    /**
     * Modifica del valore dell'attributo eventDefinition
     * 
     * @param eventDefinition Nuovo valore dell'attributo eventDefinition
     */
    public void setEventDefinition (String eventDefinition) 
    {
        this.eventDefinition = eventDefinition;
    }
    
    
    /**
     * Ottengo il valore dell'attributo participantName
     * 
     * @return Valore dell'attributo participantName
     */
    public ArrayList<String> getParticipantName () 
    {
        return this.participantName;
    }

    
    /**
     * Modifica del valore dell'attributo participantName
     * 
     * @param participantName Nuovo valore dell'attributo participantName
     */
    public void setParticipantName (ArrayList<String> participantName) 
    {
        this.participantName = participantName;
    }

    
    /**
     * Ottengo il valore dell'attributo sequenceFlowIncoming
     * 
     * @return Valore dell'attributo sequenceFlowIncoming
     */
    public ArrayList<String> getSequenceFlowIncoming () 
    {
        return this.sequenceFlowIncoming;
    }

    
    /**
     * Modifica del valore dell'attributo sequenceFlowIncoming
     * 
     * @param sequenceFlowIncoming Nuovo valore dell'attributo sequenceFlowIncoming
     */
    public void setSequenceFlowIncoming (ArrayList<String> sequenceFlowIncoming) 
    {
        this.sequenceFlowIncoming = sequenceFlowIncoming;
    }

    
    /**
     * Ottengo il valore dell'attributo sequenceFlowOutgoing
     * 
     * @return Valore dell'attributo sequenceFlowOutgoing
     */
    public ArrayList<String> getSequenceFlowOutgoing () 
    {
        return this.sequenceFlowOutgoing;
    }

    
    /**
     * Modifica del valore dell'attributo sequenceFlowOutgoing
     * 
     * @param sequenceFlowOutgoing Nuovo valore dell'attributo sequenceFlowOutgoing
     */
    public void setSequenceFlowOutgoing (ArrayList<String> sequenceFlowOutgoing) 
    {
        this.sequenceFlowOutgoing = sequenceFlowOutgoing;
    }
    
    
    /**
     * Ottengo il valore dell'attributo refIncomingMessageFlow
     * 
     * @return Valore dell'attributo refIncomingMessageFlow
     */
    public ArrayList<BPMNElement> getRefIncomingMessageFlow () 
    {
        return this.refIncomingMessageFlow;
    }

    
    /**
     * Modifica del valore dell'attributo refIncomingMessageFlow
     * 
     * @param refIncomingMessageFlow Nuovo valore dell'attributo refIncomingMessageFlow
     */
    public void setRefIncomingMessageFlow (ArrayList<BPMNElement> refIncomingMessageFlow) 
    {
        this.refIncomingMessageFlow = refIncomingMessageFlow;
    }

    
    /**
     * Ottengo il valore dell'attributo refOutgoingMessageFlow
     * 
     * @return Valore dell'attributo refOutgoingMessageFlow
     */
    public ArrayList<BPMNElement> getRefOutgoingMessageFlow () 
    {
        return this.refOutgoingMessageFlow;
    }

    
    /**
     * Modifica del valore dell'attributo refOutgoingMessageFlow
     * 
     * @param refOutgoingMessageFlow Nuovo valore dell'attributo refOutgoingMessageFlow
     */
    public void setRefOutgoingMessageFlow (ArrayList<BPMNElement> refOutgoingMessageFlow) 
    {
        this.refOutgoingMessageFlow = refOutgoingMessageFlow;
    }
    
    
    /**
     * Ottengo il valore dell'attributo incomingDataFlow
     * 
     * @return Valore dell'attributo incomingDataFlow
     */
    public ArrayList<DataObject> getIncomingDataFlow () 
    {
        return this.incomingDataFlow;
    }

    
    /**
     * Modifica del valore dell'attributo incomingDataFlow
     * 
     * @param incomingDataFlow Nuovo valore dell'attributo incomingDataFlow
     */
    public void setIncomingDataFlow (ArrayList<DataObject> incomingDataFlow) 
    {
        this.incomingDataFlow = incomingDataFlow;
    }

    
    /**
     * Ottengo il valore dell'attributo outgoingDataFlow
     * 
     * @return Valore dell'attributo outgoingDataFlow
     */
    public ArrayList<DataObject> getOutgoingDataFlow () 
    {
        return this.outgoingDataFlow;
    }

    
    /**
     * Modifica del valore dell'attributo outgoingDataFlow
     * 
     * @param outgoingDataFlow Nuovo valore dell'attributo outgoingDataFlow
     */
    public void setOutgoingDataFlow (ArrayList<DataObject> outgoingDataFlow) 
    {
        this.outgoingDataFlow = outgoingDataFlow;
    }

    
    /**
     * Ottengo il valore dell'attributo petriNet
     * 
     * @return Valore dell'attributo petriNet
     */
    public PEP getPetriNet () 
    {
        return this.petriNet;
    }

    
    /**
     * Modifica del valore dell'attributo petriNet
     * 
     * @param petriNet Nuovo valore dell'attributo petriNet
     */
    public void setPetriNet (PEP petriNet) 
    {
        this.petriNet = petriNet;
    }
    

    /**
     * Ottengo il valore dell'attributo idSubElement
     * 
     * @return Valore dell'attributo idSubElement
     */
    public String getidSubElement () 
    {
        return this.idSubElement;
    }

    
    /**
     * Modifica del valore dell'attributo idSubElement
     * 
     * @param idSubElement Nuovo valore dell'attributo idSubElement
     */
    public void setidSubElement (String idSubElement) 
    {
        this.idSubElement = idSubElement;
    }


    /*
     * Due elementi sono uguali se hanno gli attributi "id", "name", "renamedID" e "type" uguali
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals (Object obj) 
    {
        if (this == obj)
            return true;
        
        if (obj == null)
            return false;
        
        if (getClass () != obj.getClass ())
            return false;
        
        BPMNElement other = (BPMNElement) obj;
        
        if (id == null) {
            if (other.id != null)
                return false;
        } 
        else if (!id.equals(other.id))
            return false;
        
        if (name == null) {
            if (other.name != null)
                return false;
        } 
        else if (!name.equals(other.name))
            return false;
        
        if (type == null) {
            if (other.type != null)
                return false;
        } 
        else if (!type.equals(other.type))
            return false;
        
        return true;
    }


    /*
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString () 
    {
        return "BPMNElement [type=" + this.type + ", id=" + this.id + ", renamedID="
                + this.renamedID + ", name=" + this.name + ", renamedName=" + this.renamedName + " eventDefinition="
                + this.eventDefinition + ", sequenceFlowIncoming="
                + this.sequenceFlowIncoming + ", sequenceFlowOutgoing="
                + this.sequenceFlowOutgoing + ", refIncomingMessageFlow="
                + this.refIncomingMessageFlow + ", refOutgoingMessageFlow="
                + this.refOutgoingMessageFlow + ", incomingDataFlow="
                + this.incomingDataFlow + ", outgoingDataFlow=" + this.outgoingDataFlow
                + ", petriNet=" + this.petriNet + ", idSubElement=" + this.idSubElement
                + "]";
    }
}

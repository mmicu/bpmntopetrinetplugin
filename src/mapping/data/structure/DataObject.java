/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package mapping.data.structure;

/**
 * Questa classe rappresenta l'entita' "Data Object".
 * 
 * @author Marco
 *
 */
public class DataObject 
{
    /**
     * Tag XML del data object
     */
    private String tag;
    
    
    /**
     * Id del data object (attributo "id" del tag dell'elemento dichiarato all'interno del file .bpmn)
     */
    private String id;
    
    
    /**
     * Nome del data object  (attributo "name" del tag dell'elemento dichiarato all'interno del file .bpmn)
     */
    private String name;
    
    
    /**
     * Stato del data object  (attributo "name" del tag "bpmn2:dataState" dichiarato all'interno del tag del data object)
     */
    private String state;
    
    
    /**
     * Se il data object e' dichiarato con il tag "bpmn2:dataObjectReference", significa che e' referenziato ad
     * un data object creato precedentemente. All'interno di questo tag, come attributo, e' presente l'id a cui
     * il data object e' referenziato (attributo "dataObjectRef").
     * 
     * Esempio: 
     * <bpmn2:dataObjectReference id="_DataObjectReference_3" name="Reference to Data Object 2" dataObjectRef="DataObject_1"/>
     * <bpmn2:dataObject id="DataObject_1" name="Data Object 2"/>
     */
    private String dataObjectRefId;
    
    
    /**
     * Discorso analogo rispetto all'attributo precedente. Questa volta, pero', salvo l'istanza anziche' l'id del data object,
     * visto che l'istanza e' aggiunta successivamente
     */
    private DataObject dataObjectRef;
    
    
    
    /**
     * Costruttore
     * 
     * @param tag Tag XML del data object
     */
    public DataObject (String tag)
    {
        this.tag = tag;
    }
    
    
    /**
     * Metodo che controlla se il "data object" e' referenziato ad un altro "data object"
     * 
     * @return "true" se il "data object" e' referenziato ad un altro "data object", "false" altrimenti
     */
    public boolean isReferencedDataObject ()
    {
        return this.tag.equals ("bpmn2:dataObjectReference") || this.tag.equals ("dataObjectReference");
    }


    /**
     * Ottengo il valore dell'attributo tag
     * 
     * @return Valore dell'attributo tag
     */
    public String getTag () 
    {
        return this.tag;
    }

    
    /**
     * Modifica del valore dell'attributo tag
     * 
     * @param tag Nuovo valore dell'attributo tag
     */
    public void setTag (String tag) 
    {
        this.tag = tag;
    }

    
    /**
     * Ottengo il valore dell'attributo id
     * 
     * @return Valore dell'attributo id
     */
    public String getId () 
    {
        return this.id;
    }

    
    /**
     * Modifica del valore dell'attributo id
     * 
     * @param id Nuovo valore dell'attributo id
     */
    public void setId (String id) 
    {
        this.id = id;
    }

    
    /**
     * Ottengo il valore dell'attributo name
     * 
     * @return Valore dell'attributo name
     */
    public String getName () 
    {
        return this.name;
    }

    
    /**
     * Modifica del valore dell'attributo name
     * 
     * @param name Nuovo valore dell'attributo name
     */
    public void setName (String name) 
    {
        this.name = name;
    }
    
    
    /**
     * Ottengo il valore dell'attributo state
     * 
     * @return Valore dell'attributo state
     */
    public String getState () 
    {
        return this.state;
    }

    
    /**
     * Modifica del valore dell'attributo state
     * 
     * @param state Nuovo valore dell'attributo state
     */
    public void setState (String state) 
    {
        this.state = state;
    }
    

    /**
     * Ottengo il valore dell'attributo dataObjectRefId
     * 
     * @return Valore dell'attributo dataObjectRefId
     */
    public String getDataObjectRefId () 
    {
        return this.dataObjectRefId;
    }

    
    /**
     * Modifica del valore dell'attributo dataObjectRefId
     * 
     * @param dataObjectRefId Nuovo valore dell'attributo dataObjectRefId
     */
    public void setDataObjectRefId (String dataObjectRefId) 
    {
        this.dataObjectRefId = dataObjectRefId;
    }

    
    /**
     * Ottengo il valore dell'attributo dataObjectRef
     * 
     * @return Valore dell'attributo dataObjectRef
     */
    public DataObject getDataObjectRef () 
    {
        return this.dataObjectRef;
    }

    
    /**
     * Modifica del valore dell'attributo dataObjectRef
     * 
     * @param dataObjectRef Nuovo valore dell'attributo dataObjectRef
     */
    public void setDataObjectRef (DataObject dataObjectRef) 
    {
        this.dataObjectRef = dataObjectRef;
    }


    /*
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        
        if (obj == null)
            return false;
        
        if (getClass() != obj.getClass())
            return false;
        
        DataObject other = (DataObject) obj;
        
        if (dataObjectRef == null) {
            if (other.dataObjectRef != null)
                return false;
        } 
        else if (!dataObjectRef.equals(other.dataObjectRef))
            return false;
        
        if (dataObjectRefId == null) {
            if (other.dataObjectRefId != null)
                return false;
        } 
        else if (!dataObjectRefId.equals(other.dataObjectRefId))
            return false;
        
        if (id == null) {
            if (other.id != null)
                return false;
        } 
        else if (!id.equals(other.id))
            return false;
        
        if (name == null) {
            if (other.name != null)
                return false;
        } 
        else if (!name.equals(other.name))
            return false;
        
        if (state == null) {
            if (other.state != null)
                return false;
        } 
        else if (!state.equals(other.state))
            return false;
        
        if (tag == null) {
            if (other.tag != null)
                return false;
        } 
        else if (!tag.equals(other.tag))
            return false;
        
        return true;
    }


    /*
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString () 
    {
        return "DataObject [tag=" + this.tag + ", id=" + this.id + ", name=" + this.name + ", state=" + this.state
                + ", dataObjectRefId=" + this.dataObjectRefId + ", dataObjectRef=" + this.dataObjectRef + "]";
    }
}

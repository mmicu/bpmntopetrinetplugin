/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package mapping.data.structure;

/**
 * Questa classe permette di creare oggetti "pair" di qualsiasi tipo.
 * 
 * @author Marco
 *
 * @param <k> Primo elemento della coppia 
 * @param <j> Secondo elemento della coppia 
 */
public class Pair <k, j> 
{
    /**
     * Primo elemento della coppia 
     */
    private k e_1;
    
    
    /**
     * Secondo elemento della coppia 
     */
    private j e_2;

    
    
    /**
     * Costruttore
     */
    public Pair ()
    {
    }
    
    
    /**
     * Costruttore
     * 
     * @param e_1 Primo elemento della coppia 
     * @param e_2 Secondo elemento della coppia 
     */
    public Pair (k e_1, j e_2) 
    {
        this.e_1 = e_1;
        this.e_2 = e_2;
    }
    
    
    /**
     * Metodo statico che ritorna un oggetto di tipo "Pair"
     * 
     * @param e_1 Primo elemento della coppia 
     * @param e_2 Secondo elemento della coppia 
     * @return
     */
    public static <k, j> Pair<k, j> pair (k e_1, j e_2) 
    {
        return new Pair <k, j>(e_1, e_2);
    }

    
    /**
     * Ottengo il valore dell'attributo e_1
     * 
     * @return Valore dell'attributo e_1
     */
    public k getFirstElement () 
    {
        return this.e_1;
    }
    
    
    /**
     * Modifica del valore dell'attributo e_1
     * 
     * @param e_1 Nuovo valore dell'attributo e_1
     */
    public void setFirstElement (k e_1) 
    {
        this.e_1 = e_1;
    }

    
    /**
     * Ottengo il valore dell'attributo e_2
     * 
     * @return Valore dell'attributo e_2
     */
    public j getSecondElement () 
    {
        return this.e_2;
    }
    
    
    /**
     * Modifica del valore dell'attributo e_2
     * 
     * @param e_2 Nuovo valore dell'attributo e_2
     */
    public void setSecondElement (j e_2) 
    {
        this.e_2 = e_2;
    }


    /*
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Pair [e_1=" + this.e_1.toString () + ", e_2=" + this.e_2.toString () + "];";
    }


    /*
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals (Object obj) 
    {
        Pair<?, ?> o = (Pair<?, ?>) obj;
        
        if (this == obj)
            return true;
        
        if (obj == null)
            return false;
        
        if (getClass () != obj.getClass ())
            return false;
        
        if (e_1 == null) {
            if (o.e_1 != null)
                return false;
        } 
        else if (!e_1.equals (o.e_1))
            return false;
        
        if (e_2 == null) {
            if (o.e_2 != null)
                return false;
        } 
        else if (!e_2.equals (o.e_2))
            return false;
        
        return true;
    }
}

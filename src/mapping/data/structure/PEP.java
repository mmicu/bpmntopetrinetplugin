/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package mapping.data.structure;

import java.util.ArrayList;

import mapping.data.structure.Pair;

/**
 * Questa classe rappresenta l'entita' rete di Petri secondo il formato ll_net
 * Esempio di rete di Petri in questo formato (M1 rappresenta il token assegnato ad una piazza):
 * 
 * PEP
 * PetriBox
 * FORMAT_N
 * PL
 * "P0"M1
 * "P1"
 * TR
 * "T0"
 * TP
 * 1<2
 * PT
 * 1>1
 * 
 * @author Marco
 *
 */
public class PEP
{
    /**
     * Informazioni standard della rete di Petri nel formato ll_net:
     * PEP
     * PetriBox
     * FORMAT_N
     */
    private String stInformation;
    

    /**
     * Piazze della rete di Petri (id, "true" se c'e' il token, "false" altrimenti)
     */
    private ArrayList<Pair<String, Boolean>> p;
    

    /**
     * Transizioni della rete di Petri (id)
     */
    private ArrayList<String> t;
    
    
    /**
     * Collegamenti PT (Place to Transition) della rete di Petri (id piazza, id transizione)
     */
    private ArrayList<Pair<String, String>> pt;
    
    
    /**
     * Collegamenti TP (Transition to Place) della rete di Petri (id transizione, id piazza)
     */
    private ArrayList<Pair<String, String>> tp;
    
    
    
    /**
     * Costruttore
     */
    public PEP ()
    {
        this.p             = new ArrayList<Pair<String, Boolean>> ();
        this.t             = new ArrayList<String> ();
        this.pt            = new ArrayList<Pair<String, String>> ();
        this.tp            = new ArrayList<Pair<String, String>> ();
        this.stInformation = "PEP\nPetriBox\nFORMAT_N";
    }
    
    
    /**
     * Metodo che aggiunge una piazza alla rete di Petri
     * 
     * @param place Piazza da aggiungere
     */
    public void addP (String place)
    {
        this.addP (place, false);
    }
    
    
    /**
     * Metodo che aggiunge una piazza alla rete di Petri
     * 
     * @param place             Piazza da aggiungere
     * @param isInitialMarking  "true" se la piazza ha un token, "false" altrimenti
     */
    public void addP (String place, boolean isInitialMarking)
    {
        if (!this.p.contains (Pair.pair (place, false)) && !this.p.contains (Pair.pair (place, true)))
            this.p.add (Pair.pair (place, isInitialMarking));
    }
    
    
    /**
     * Metodo che aggiunge una transizione alla rete di Petri
     * 
     * @param transition Transizione da aggiungere
     */
    public void addT (String transition)
    {
        if (!this.t.contains (transition))
            this.t.add (transition);
    }
    
    
    /**
     * Metodo che aggiunge un collegamento PT alla rete di Petri
     * 
     * @param place       Piazza da collegare alla transizione
     * @param transition  Transizione che ha in ingresso la piazza
     */
    public void addPT (String place, String transition)
    {
        if (!this.pt.contains (Pair.pair (place, transition)))
            this.pt.add (new Pair<String, String> (place, transition));
    }
    
    
    /**
     * Metodo che aggiunge un collegamento TP alla rete di Petri
     * 
     * @param transition Transizione da collegare alla piazza
     * @param place      Piazza che ha in ingresso la transizione
     */
    public void addTP (String transition, String place)
    {
        if (!this.tp.contains (Pair.pair (transition, place)))
            this.tp.add (new Pair<String, String> (transition, place));
    }
    
    
    /**
     * Metodo che aggiunge il token ad una certa piazza
     * 
     * @param place Piazza a cui va aggiunto il token
     */
    public void setInitialMarking (String place)
    {
        this.setInitialMarking (place, true);
    }
    
    
    /**
     * Metodo che aggiunge o toglie il token ad una certa piazza
     * 
     * @param place Piazza a cui va aggiunto o tolto il token
     * @param mark  "true" se aggiungo il token, "false" se lo tolgo
     */
    public void setInitialMarking (String place, boolean mark)
    {
        ArrayList<Pair <String, Boolean>> allPlaces = this.getP ();
        
        for (int k = 0; k < allPlaces.size (); k++)
            if (allPlaces.get (k).getFirstElement ().equals (place))
                this.p.get (k).setSecondElement (mark);
    }
    
    
    /**
     * Metodo che controlla l'esistenza di una piazza in base al nome
     * 
     * @param place Piazza di cui controllo l'esistenza nella rete di Petri
     * @return      "true" se la piazza esiste, "false" altrimenti
     */
    public boolean existsPlace (String place)
    {
        return this.p.contains (place);
    }
    
    
    /**
     * Metodo che controlla l'esistenza di una transizione in base al nome
     * 
     * @param transition Transizione di cui controllo l'esistenza nella rete di Petri
     * @return           "true" se la transizione esiste, "false" altrimenti
     */
    public boolean existsTransition (String transition)
    {
        return this.t.contains (transition);
    }
    
    
    /**
     * Metodo che controlla l'esistenza di un collegamento PT in base al nome
     * della piazza e della transizione
     *
     * @param place      Piazza del collegamento PT
     * @param transition Transizione del collegamento PT
     * @return           "true" se il collegamento PT esiste, "false" altrimenti
     */
    public boolean existsPlaceToTransition (String place, String transition)
    {
        return this.pt.contains (new Pair<String, String> (place, transition));
    }
    
    
    /**
     * Metodo che controlla l'esistenza di un collegamento TP in base al nome
     * della transizione e della piazza
     * 
     * @param transition Transizione del collegamento TP
     * @param place      Piazza del collegamento TP
     * @return           "true" se il collegamento TP esiste, "false" altrimenti
     */
    public boolean existsTransitionToPlace (String transition, String place)
    {
        return this.tp.contains (new Pair<String, String> (transition, place));
    }
    
    
    /**
     * "Merge" tra due reti di Petri
     * 
     * @param otherPEP Rete di Petri che va unita alla rete di Petri "this"
     * @return         Le due rete di Petri unite
     */
    public PEP mergeWithAnotherPetriNet (PEP otherPEP)
    {
        ArrayList<Pair<String, Boolean>> otherPEPplaces = otherPEP.getP ();
        for (int k = 0; k < otherPEPplaces.size (); k++)
            this.addP (otherPEPplaces.get (k).getFirstElement (), otherPEPplaces.get (k).getSecondElement ());
        
        ArrayList<String> otherPEPtransitions = otherPEP.getT ();
        for (int k = 0; k < otherPEPtransitions.size (); k++)
            this.addT (otherPEPtransitions.get (k));
        
        ArrayList<Pair<String, String>> otherPEPtp = otherPEP.getTP ();
        for (int k = 0; k < otherPEPtp.size (); k++)
            this.addTP (otherPEPtp.get (k).getFirstElement (), otherPEPtp.get (k).getSecondElement ());
        
        ArrayList<Pair<String, String>> otherPEPpt = otherPEP.getPT ();
        for (int k = 0; k < otherPEPpt.size (); k++)
            this.addPT (otherPEPpt.get (k).getFirstElement (), otherPEPpt.get (k).getSecondElement ());
        
        return this;
    }
    
    
    /**
     * Ottengo il valore dell'attributo p
     * 
     * @return Valore dell'attributo p
     */
    public ArrayList<Pair<String, Boolean>> getP ()
    {
        return this.p;
    }
    
    
    /**
     * Ottengo solo gli id delle piazze, senza il booleano che indica la presenza del token
     * 
     * @return ArrayList degli id delle piazze
     */
    public ArrayList<String> getOnlyPlace ()
    {
        ArrayList<String> res = new ArrayList<String> ();
        
        for (int k = 0; k < this.p.size (); k++)
            res.add (this.p.get (k).getFirstElement ());
        
        return res;
    }
    
    
    /**
     * Ottengo il valore dell'attributo t
     * 
     * @return Valore dell'attributo t
     */
    public ArrayList<String> getT ()
    {
        return this.t;
    }
    
    
    /**
     * Ottengo il valore dell'attributo tp
     * 
     * @return Valore dell'attributo tp
     */
    public ArrayList<Pair<String, String>> getTP ()
    {
        return this.tp;
    }
    
    
    /**
     * Ottengo il valore dell'attributo pt
     * 
     * @return Valore dell'attributo pt
     */
    public ArrayList<Pair<String, String>> getPT ()
    {
        return this.pt;
    }
    
    
    /**
     * Metodo che restituisce l'oggetto "PEP" nella notazione standard del formato ll_net, ignorando
     * l'id reale degli elementi
     * 
     * @return Notazione standard del formato ll_net
     */
    public String getStandardNotation ()
    {
        String content = this.stInformation;
        
        content += "\nPL\n";
        for (int k = 0; k < this.p.size (); k++)
            content += "\"P" + k + "\"" + ((this.p.get (k).getSecondElement ()) ? "M1\n" : "\n");
        
        content += "TR\n";
        for (int k = 0; k < this.t.size (); k++)
            content += "\"T" + k + "\"\n";

        content += "TP\n";
        for (int k = 0; k < this.tp.size (); k++)
            content += (this.t.lastIndexOf (this.tp.get (k).getFirstElement ()) + 1) + "<" + 
                       (this.getOnlyPlace ().lastIndexOf (this.tp.get (k).getSecondElement ()) + 1) + "\n";

        content += "PT\n";
        for (int k = 0; k < this.pt.size (); k++)
            content += (this.getOnlyPlace ().lastIndexOf (this.pt.get (k).getFirstElement ()) + 1) + ">" + 
                       (this.t.lastIndexOf (this.pt.get (k).getSecondElement ()) + 1) + "\n";
        
        return content;
    }

    
    /**
     * Metodo che restituisce l'oggetto "PEP" nella notazione standard del formato ll_net,
     * con l'id reale degli elementi
     * 
     * @return Notazione standard del formato ll_net
     */
    public String getStandardNotationWithRealNames ()
    {
        String content = this.stInformation;
        
        content += "\nPL\n";
        for (int k = 0; k < this.p.size (); k++)
            content += "\"" + this.p.get (k).getFirstElement () + "\"" + ((this.p.get (k).getSecondElement ()) ? "M1\n" : "\n");
        
        content += "TR\n";
        for (int k = 0; k < this.t.size (); k++)
            content += "\"" + this.t.get (k) + "\"\n";

        content += "TP\n";
        for (int k = 0; k < this.tp.size (); k++)
            content += (this.t.lastIndexOf (this.tp.get (k).getFirstElement ()) + 1) + "<" + 
                       (this.getOnlyPlace ().lastIndexOf (this.tp.get (k).getSecondElement ()) + 1) + "\n";

        content += "PT\n";
        for (int k = 0; k < this.pt.size (); k++)
            content += (this.getOnlyPlace ().lastIndexOf (this.pt.get (k).getFirstElement ()) + 1) + ">" + 
                       (this.t.lastIndexOf (this.pt.get (k).getSecondElement ()) + 1) + "\n";
        
        return content;
    }

    
    /*
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString () 
    {
        String content = (this.p.size () > 0 || this.t.size () > 0 || 
                          this.tp.size () > 0 || this.pt.size () > 0) ? this.stInformation : "Empty Petri net";
        int k;
        
        if (content.equals ("Empty Petri net"))
            return content;
        
        content += "\nPL\n";
        for (k = 0; k < this.p.size (); k++)
            content += this.p.get (k) + "\n";
        
        content += "TR\n";
        for (k = 0; k < this.t.size (); k++)
            content += this.t.get (k) + "\n";

        content += "TP\n";
        for (k = 0; k < this.tp.size (); k++)
            content += this.tp.get (k).getFirstElement () + "<" + this.tp.get (k).getSecondElement () + "\n";

        content += "PT\n";
        for (k = 0; k < this.pt.size (); k++)
            content += this.pt.get (k).getFirstElement () + ">" + this.pt.get (k).getSecondElement () + "\n";
        
        return content;
    }
}

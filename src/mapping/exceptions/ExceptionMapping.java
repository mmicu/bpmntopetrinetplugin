/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package mapping.exceptions;

/**
 * Classe di eccezione che puo' essere generata durante il mapping.
 * 
 * @author Marco
 */
public class ExceptionMapping extends Exception
{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 8160823210896262968L;


	
	/**
	 * Costruttore senza parametri
	 */
	public ExceptionMapping () 
	{ 
		super (); 
	}
	  
	
	/**
	 * Costruttore con il parametro "message"
	 * 
	 * @param message Messaggio dell'eccezione
	 */
	public ExceptionMapping (String message) 
	{ 
		super (message); 
	}
}

/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package mapping.rules;

import java.util.Locale;

import config.GlobalConfiguration;
import utils.GenericUtils;
import utils.NumberUtils;
import utils.StringUtils;
import mapping.data.structure.BPMNElement;
import mapping.data.structure.PEP;
import mapping.exceptions.ExceptionMapping;

/**
 * Questa classe viene utilizzata per applicare le regole che gestiscono le piazze.
 * Queste regole sono dichiarate all'interno del file "rules.xml" in questo modo:
 * 
 * <place id="$id_element" n="$n_incoming + $n_outgoing" />
 * <place id="piazza_id" n="1" />
 * <place id="piazza_id" />
 * 
 * I valori che puo' assumere l'attributo "id" sono i seguenti:
 *     - keyword:  in questo caso e' ammessa solo la keyword "$id_element" che sta ad indicare la selezione dell'id dell'elemento
 *                 BPMN corrispondente alla piazza stessa. Se ci sono piu' piazze, viene aggiunta la seguente
 *                 stringa: "_k" dove "k" e' il numero della piazza;
 *     - stringa:  il nome della piazza e' il valore dell'attributo "id". Anche in questo caso, se ci sono piu' piazze
 *                 viene aggiunta la seguente stringa: "_k" dove "k" e' il numero della piazza.
 *     
 * I valori che puo' assumere l'attributo "n" sono i seguenti:
 *     - numero:      sta ad indicare il numero di piazze da creare;
 *     - espressione: il numero di piazze da creare e' il risultato dell'espressione. L'espressione puo' contenere keyword come
 *                    "$n_incoming" e "$n_outgoing" che indicano, rispettivamente, il numero di frecce entranti e uscenti dell'elemento
 *                    BPMN in esame.
 *     
 * @author Marco
 *
 */
public class PlaceRule 
{
	/**
	 * Se il valore di questo attributo e' settato a "true", durante l'esecuzione del programma, verranno
	 * stampate diverse variabili
	 */
	private boolean debug = false;
	
	
	/**
	 * Id della piazza
	 */
	private String id;
	
	
	/**
	 * Numero delle piazze da creare
	 */
	private String n;
	
	
	/**
	 * Possibili keyword per l'attributo "id"
	 */
	private static final String[] KEYWORD_ID = { "$id_element", "$name_element" };
	
	
	/**
	 * Possibili keyword per l'attributo "n"
	 */
	private static final String[] KEYWORD_N = { "$n_incoming", "$n_outgoing" };
	
	
	
	/**
	 * Costruttore della classe PlaceRule
	 * 
	 * @param id Id della piazza
	 * @param n  Numero di piazze da creare
	 */
	public PlaceRule (String id, String n)
	{
		this.id = id;
		this.n  = n;
	}
	
	
	/**
	 * Metodo che applica le regole di mapping per la creazione delle piazze
	 * 
	 * @param b                 Elemento BPMN
	 * @return                  Rete di Petri corrispondente all'elemento BPMN
	 * @throws ExceptionMapping Eccezione generata nel caso di espressione errata
	 */
	public PEP applyRule (BPMNElement b, Locale lang) throws ExceptionMapping
	{
		// Rete di Petri che conterra' le piazze dell'elemento BPMN "b"
		PEP petriNet = new PEP ();
		
		// Numero di piazze da creare
		int nPlaces = 0;
		
		// Se l'attributo "n" non viene specificato nel tag "place", viene assegnato 1 come default
		// al numero di piazze da creare
		if (this.n == null)
			nPlaces = 1;
		
		// Se l'attributo "n" e' un numero, si determina il corrispondente intero della stringa
		else if (NumberUtils.isNumber (this.n))
			nPlaces = StringUtils.getIntByString (this.n);
		
		// L'attributo "n" e' una keyword. In questa prima versione, le keyword possibili sono: "$n_incoming" e "$n_outgoing"
		else if (StringUtils.isKeyword (this.n)) {
			for (int k = 0; k < this.KEYWORD_N.length; k++) {
				if (this.n.equals (this.KEYWORD_N[k])) {
					switch (k) {
					case 0:
						nPlaces = b.getSequenceFlowIncoming ().size ();
						break;
					
					case 1:
						nPlaces = b.getSequenceFlowOutgoing ().size ();
						break;
					}
				}
			}
		}
		
		// L'attributo "n" e' un'espressione
		else if (StringUtils.isExpression (this.n)) {
			int resultExpression = GenericUtils.evaluateExpression (
					                   // L'espressione puo' contenere keyword, quindi modifico il valore dell'espressione
	                                   // inserendo il valore della keyword al posto della keyword.
	                                   // Esempio: ($n_outgoing + 2) --> (1 + 3), assumendo che $n_outgoing = 1
                                       GenericUtils.replaceKeywordWithValue (this.n, b, this.KEYWORD_N)
					               );
			
			if (resultExpression == -1) {
				String messageException = GenericUtils._l (lang, "exception") + " ExceptionMapping. " + GenericUtils._l (lang, "message") + ":\n";
				
				messageException += "PlaceRule.applyRule (...):\n";
				messageException += GenericUtils._l (lang, "wrong_expression") + "! (" + this.n + ")";
				
				throw new ExceptionMapping (messageException);
			}
			else
				nPlaces = resultExpression;
		}
		
		
		// Suffisso della/e piazza/e
		String suffixPlace = null;
		
		// Se il valore dell'attributo "id" e' una keyword, il prefisso sara' uguale al valore della keyword.
		// In questa prima versione l'unica keyword ammessa per l'attributo "id" e' "$id_element"
		if (StringUtils.isKeyword (this.id)) {
			for (int j = 0; j < this.KEYWORD_ID.length; j++) {
				if (this.id.equals (this.KEYWORD_ID[j])) {
					// RESO PIU' MODULARE CON LO SWITCH PER EVENTUALI KEYWORD DA AGGIUGNERE IN FUTUROOOOOO
					switch (j) {
					case 0:
					    suffixPlace = b.getRenamedID ();
						break;
					case 1:
					    suffixPlace = b.getRenamedName ();
					    break;
					}
				}
			}
			
			// Elimino i caratteri in eccesso in base al valore di GlobalConfiguration.MAX_CHARACTERS_NAME_PLACE
			if (suffixPlace != null)
			    if (suffixPlace.length () > GlobalConfiguration.MAX_CHARACTERS_NAME_PLACE)
			        suffixPlace = suffixPlace.substring (0, GlobalConfiguration.MAX_CHARACTERS_NAME_PLACE);
		}
		// Se il valore dell'attributo "id" non e' una keyword, l'id della transizione sara' uguale all'id dell'elemento BPMN
		else
		    suffixPlace = this.id;
		
		
		// Creazione delle piazze
		for (int k = 1; k <= nPlaces; k++) {
			String namePlace = "p" + (nPlaces > 1 ? k : "") + suffixPlace;
			
			petriNet.addP (namePlace);
			
			if (this.debug)
				System.out.println ("Place:  " + namePlace);
		}
		
		return petriNet;
	}


	/**
	 * Ottengo il valore dell'attributo id
	 * 
	 * @return Valore dell'attributo id
	 */
	public String getId () 
	{
		return this.id;
	}


	/**
	 * Modifica del valore dell'attributo id
	 * 
	 * @param id Nuovo valore dell'attributo id
	 */
	public void setId (String id) 
	{
		this.id = id;
	}


	/**
	 * Ottengo il valore dell'attributo n
	 * 
	 * @return Valore dell'attributo n
	 */
	public String getN () 
	{
		return this.n;
	}


	/**
	 * Modifica del valore dell'attributo n
	 * 
	 * @param n Nuovo valore dell'attributo n
	 */
	public void setN (String n) 
	{
		this.n = n;
	}
	
	
	/**
	 * Modifica del valore dell'attributo debug
	 * 
	 * @param debug Nuovo valore dell'attributo debug
	 */
	public void setDebug (boolean debug) 
	{
		this.debug = debug;
	}


	/*
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () 
	{
		return "PlaceRule [id=" + this.id + ", n=" + this.n + "]";
	}
}

/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package mapping.rules;

import java.util.ArrayList;

import utils.GenericUtils;
import utils.StringUtils;
import mapping.data.structure.BPMNElement;
import mapping.data.structure.PEP;
import mapping.data.structure.Pair;

/**
 * Questa classe viene utilizzata per applicare le regole che gestiscono i collegamenti TP (Transition to Place).
 * Queste regole sono dichiarate all'interno del file "rules.xml" in questo modo:
 * 
 * <tps>
 *     <tp transition="Y" place="X" type="Z" />    <!-- Prima regola -->
 *     <tp transition="Y1" place="X1" type="Z1" /> <!-- Seconda regola -->
 * </tps>
 * 
 * I valori che possono assumere X, X1, Y e Y1 sono i seguenti:
 *     - numero intero: sta ad indicare la selezione della transizione/piazza numero "n";
 *     - espressione:   sta ad indicare la selezione della transizione/piazza risultate dalla valutazione dell'espressione (sempre un numero);
 *     - range [a, b]:  sta ad indicare la selezione delle transizione/piazza che sono comprese nell'intervallo chiuso [a, b].
 *     
 * I valori che puo' assumere Z e Z1:
 *     - $one_to_one:   sta ad indicare il collegamento 1-1 tra la transizione e la piazza;
 *     - $all:          sta ad indicare il collegamento 1-N tra la transizione e la piazza.
 *     
 * @author Marco
 *
 */
public class TPRule 
{
	/**
	 * ArrayList che memorizza il valore dell'attributo "transition" presente nel tag "tp"
	 */
	private ArrayList<String> keywordTransition;
	

	/**
	 * ArrayList che memorizza il valore dell'attributo "place" presente nel tag "tp"
	 */
	private ArrayList<String> keywordPlace;
	

	/**
	 * ArrayList che memorizza il valore dell'attributo "type" presente nel tag "tp". 
	 * Se l'attributo non e' presente, "$one_to_one" e' il valore di default
	 */
	private ArrayList<String> type;
	
	
	
	/**
	 * Costruttore della classe TPRule.
	 */
	public TPRule ()
	{
		this.keywordTransition = new ArrayList<String> ();
		this.keywordPlace      = new ArrayList<String> ();
		this.type              = new ArrayList<String> ();
	}
	
	
	/**
	 * Metodo che aggiunge il valore dell'attributo del tag "transition" all'interno dell'ArrayList "keywordTransition"
	 * 
	 * @param keywordTransition
	 */
	public void addKeywordTransition (String keywordTransition)
	{
		this.keywordTransition.add (keywordTransition);
	}
	
	
	/**
	 * Metodo che aggiunge il valore dell'attributo del tag "place" all'interno dell'ArrayList "keywordPlace"
	 * 
	 * @param keywordPlace
	 */
	public void addKeywordPlace (String keywordPlace)
	{
		this.keywordPlace.add (keywordPlace);
	}
	
	
	/**
	 * Metodo che aggiunge il valore dell'attributo del tag "type" all'interno dell'ArrayList "type"
	 * 
	 * @param type
	 */
	public void addType (String type)
	{
		this.type.add (type);
	}

	
	/**
	 * Metodo che applica le regole di mapping per il collegamento TP (Transition to Place)
	 * 
	 * @param b         Elementi BPMN
	 * @param petriNet  Rete di Petri
	 * @return          Rete di Petri con l'aggiunta dei collegamenti TP
	 */
	public PEP applyRule (BPMNElement b, PEP petriNet)
	{
		PEP nPetriNet                      = petriNet;         // Evito di modificare la rete di Petri del parametro del metodo
		ArrayList<Pair<String, Boolean>> p = petriNet.getP (); // Ottengo tutti le piazze della rete di Petri
		ArrayList<String> t                = petriNet.getT (); // Ottengo tutte le transizione della rete di Petri
		
		// Le dimensioni dei due ArrayList sono le stesse:
		// this.keywordPlace.size () = this.keywordTransition.size ()
		for (int k = 0; k < this.keywordTransition.size (); k++) {
			// Ottengo i due estremi dell'intervallo => cardinalita' ArrayList = 2, se l'attributo "transition" e' un range.
			//                                       => cardinalita' ArrayList = 1, se l'attributo "transition" non e' un range.
			ArrayList<Integer> rangeTransition = GenericUtils.getRangeFromValueAttribute (StringUtils.__trim (this.keywordTransition.get (k)), b);
			
			// Ottengo i due estremi dell'intervallo => cardinalita' ArrayList = 2, se l'attributo "place" e' un range.
			//                                       => cardinalita' ArrayList = 1, se l'attributo "place" non e' un range.
			ArrayList<Integer> rangePlace      = GenericUtils.getRangeFromValueAttribute (StringUtils.__trim (this.keywordPlace.get (k)), b);

			// Da qui in poi vengono aggiunti i collegamenti TP attraverso l'utilizzo del metodo "addTP"
			if (rangePlace.size () == 1) {
				if (rangeTransition.size () == 1) {
					// Gli attributi "place" e "transition" non contengono range. Cio' significa che posso collegare la transizione n_t
					// e il place numero n_p
					//if (rangeTransition.get (0) <= (t.size () - 1) && rangePlace.get (0) <= (p.size () - 1))
						petriNet.addTP (t.get (rangeTransition.get (0)), p.get (rangePlace.get (0)).getFirstElement ());
				}
				// L'attributo "place" non contiene un range, mentre l'attributo "transition" si'.
				// Cio' significa che le transizioni del range [a, b] devono essere collegate alla piazza n_p.
				// a <= n_t <= b
				else { //rangeTransition.size () = 2
					for (int j = rangeTransition.get (0); j <= rangeTransition.get (1); j++)
						//if (j <= (t.size () - 1) && rangePlace.get (0) <= (p.size () - 1))
							petriNet.addTP (t.get (j), p.get (rangePlace.get (0)).getFirstElement ());
				}
			}
			else { // rangePlace.size () = 2
				// L'attributo "transition" non contiene un range, mentre l'attributo "place" si'.
				// Cio' significa che la transizione numero n_t deve essere collegata a tutte le piazze del range [a, b].
				// a <= n_p <= b
				if (rangeTransition.size () == 1) {
					for (int j = rangePlace.get (0); j <= rangePlace.get (1); j++)
						//if (rangeTransition.get (0) <= (t.size () - 1) && j <= (p.size () - 1))
							petriNet.addTP (t.get (rangeTransition.get (0)), p.get (j).getFirstElement ());
							//petriNet.addTP (t.get (rangeTransition.get (rangeTransition.get (0))), p.get (j).getFirstElement ());
				}
				// Entrambi gli attributi "place" e "transition" sono range.
				// Se l'attributo "type" e' uguale a "$one_to_one" il range deve essere lo stesso: [a, b]. Quindi per a <= i <= b
				// c'e' il collegamento n_t_i --> n_p_i
				//
				// Se l'attributo "type" e' uguale a "$all", per i due range [a, b], [c, d] e per a <= i <= b, c <= k <= d
				// c'e' il collegamento n_t_i --> n_p_k
				else { //rangeTransition.size () = 2
					for (int j = rangeTransition.get (0); j <= rangeTransition.get (1); j++) {
						for (int i = rangePlace.get (0); i <= rangePlace.get (1); i++) {
							//if (j == i && j <= (t.size () - 1) && i <= (p.size () - 1))
								//petriNet.addTP (t.get (j), p.get (i).getFirstElement ());
							// type = "$one_to_one"
							if (this.type.get(k).equals ("$one_to_one")) {
								if (j == i)// && j <= (p.size () - 1) && i <= (t.size () - 1))
									nPetriNet.addPT (p.get (j).getFirstElement (), t.get (i));
							}
							
							// type = "$all"
							else if (this.type.get(k).equals ("$all")) {
								//if (j <= (p.size () - 1) && i <= (t.size () - 1))
									nPetriNet.addPT (p.get (j).getFirstElement (), t.get (i));
							}
						} // Fine for rangePlace
					} // Fine for rangeTransition
				}
			}
		} // Fine for keywordTransition
		
		
		return nPetriNet;
	}


	/*
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () 
	{
		return "TPRule [keywordTransition=" + this.keywordTransition + ", keywordPlace=" + this.keywordPlace + ", type=" + this.type + "]";
	}
}

/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package mapping.transformation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import mapping.parser.xml.XMLMappedElements;
import mapping.parser.xml.XMLMappingRules;
import mapping.parser.xml.XMLMessageFlow;
import mapping.parser.xml.XMLPreProcessingNotManagedElements;
import mapping.parser.xml.XMLPrefixNameElements;
import mapping.parser.xml.XMLReadBPMNElements;
import mapping.parser.xml.XMLSubElements;
import mapping.rules.PTRule;
import mapping.rules.TPRule;
import utils.ArrayListUtils;
import utils.GenericUtils;
import utils.NumberUtils;
import utils.StringUtils;
import mapping.data.structure.BPMNElement;
import mapping.data.structure.MappingRule;
import mapping.data.structure.PEP;
import mapping.data.structure.Pair;
import mapping.data.structure.SubElements;
import mapping.exceptions.ExceptionMapping;

/**
 * Questa classe applica la trasformazione BPMN -> Rete di Petri
 * 
 * @author Marco
 *
 */
public class Transformation 
{
	/**
	 * Path del file dove sono contenute le regole di mapping
	 */
	private static final String RULES_XML_FILE = "resources/rules.xml";
	
	
	/**
	 * Se il valore di questo attributo e' settato a "true", durante l'esecuzione del programma, verranno
	 * stampate diverse variabili
	 */
	private static final boolean debug = false;
	
	
	/**
	 * File BPMN su cui eseguire il parsing
	 */
	private String fileName;
	
	
	/**
	 * Elementi BPMN da mappare
	 */
	private ArrayList<BPMNElement> b;
	
	
	/**
	 * Rete di Petri corrispondente al BPMN
	 */
	private PEP petriNet;
	
	
	/**
	 * Eventuali "subElements" contenuti nel file BPMN
	 */
	private ArrayList<SubElements> subElements;
	
	
	/**
	 * "File" di lingua
	 */
	private Locale lang;
	
	
	/**
	 * Lingua selezionata dall'utente
	 */
	private String language;
	
	
	/**
	 * ArrayList dei tag XML, degli elementi del BPMN, che vengono mappati dal plugin.
	 * Passo questo parametro ai vari parser per evitare il calcolo di questo valore per
	 * ogni classe di parsing 
	 */
	private ArrayList<String> mappedElements;
	
	
	/**
	 * Regole di mapping 
	 */
	private ArrayList<MappingRule> mappingRules;
	
	
	
	/**
	 * Costruttore 
	 */
	public Transformation () 
	{
		this.petriNet = new PEP ();
	}
	
	
	/**
	 * Costruttore
	 * 
	 * @param fileName File BPMN su cui eseguire il parsing
	 */
	public Transformation (String fileName)
	{
		this ();
		
		this.fileName = fileName;
	}
	
	
	/**
	 * Metodo che applica il mapping
	 * 
	 * @return              Rete di Petri corrispondente al file passato in input al costruttore della classe
	 * @throws IOException  Dovuta al metodo GenericUtils.getLanguage ()
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 * @throws ExceptionMapping 
	 */
	public PEP applyTransformation () throws IOException, ExceptionMapping, ParserConfigurationException, SAXException
	{
		// Determino la lingua da utilizzare
		this.language = GenericUtils.getLanguage (); // IOException
		if (this.language.equals ("Italiano"))
			this.lang = new Locale ("it");
		else// if (language.equals ("English"))
			this.lang = new Locale ("en");
		
		
		// Fase di pre-processing in cui controllo che tutti i tag dell'XML del BPMN
		// siano gestiti dal programma
		this.preProcessing ();
		
		
		// Determino i tag degli elementi BPMN che possono essere mappati (startEvent, task ecc.)
		this.mappedElements = new XMLMappedElements (Transformation.RULES_XML_FILE).parse ();
		
		
		// Determino gli elementi BPMN dichiarati all'interno dell'XML del BPMN
		this.b = new XMLReadBPMNElements (this.fileName).parse (this.mappedElements);
		
		
		// Determino le regole di mapping dichiarate all'interno del file "rules.xml"
		this.mappingRules = new XMLMappingRules (Transformation.RULES_XML_FILE).parse ();
		
		
		// Rimuovo elementi BPMN che non hanno "incoming" e "outgoing" flow
		this.removeBPMNelementsWithoutFlow ();
		
		
		// Determino i "SubElements" dichiarati all'interno del BPMN per sistemare i collegamenti con gli elementi dichiarati
		// all'interno del SubProcess stesso
		this.subElements = new XMLSubElements (this.fileName).parse (this.mappedElements);
		
		
		// Aggiungo i "SequenceFlow" agli elementi di un "SubElement". L'incoming del SubElement va nel primo elemento 
		// del SubElement stesso (StartEvent per convenzione), mentre l'outgoing va nell'ultimo elemento del SubElement 
		// (EndEvent per convenzione)
		// Questo metodo potrebbe aggiungere un task nel caso in cui un SubElement siano privo di elementi
		this.setSequenceFlowForSubElements ();
		
		
		// Se gli elementi sono all'interno di un SubElement aggiungo l'id del SubElement agli elementi della rete di Petri
		this.addIdSubElementOnBPMNElements ();
		
		
		// Rinomino gli elementi che hanno l'attributo eventDefinition != null. Aggiungo il valore dell'event definition, alla fine
		// del nome. 
		// Esempio: id_bpmn_element = "start_event_1", 
		// eventDefinition = bpmn2:timerEventDefinition -> start_event_1_timerEventDefinition
		this.renameElements ();
		
		int size_b = this.b.size ();
		
		if (Transformation.debug) {
			System.out.println ("\n|b| = " + this.b.size ());
			
			for (int k = 0; k < size_b; k++)
				System.out.println ("b[" + k + "]=" + this.b.get (k) + "\n\n");
			
			System.out.println ("\n\n|subElements| = " + this.subElements.size ());
			
			for (int k = 0, size_se = this.subElements.size (); k < size_se; k++)
				System.out.println ("SubElement[" + k + "]=" + this.subElements.get (k) + "\n\n");
		}
		
		
		// Dopo la chiamata del metodo "getPetriNetElementsByBPMNelement", ogni element BPMN conterra' la sua rete di Petri
		// Inoltre, la rete di Petri "intera" (this.petriNet) verra' aggiornata con gli elementi della rete di Petri di ogni
		// elemento BPMN
		for (int k = 0; k < size_b; k++) {
			if (Transformation.debug) {
				System.out.println ("Try to map this element:  " + this.b.get (k));
				System.out.println (this.petriNet.getStandardNotationWithRealNames ());
			}
			
			this.getPetriNetElementsByBPMNelement (this.b.get (k));
		}
		
		
		// Aggiungo gli altri collegamenti TP (Transition to Place)
		// Infatti non tutti sono dichiarati all'interno del fileName "rules.xml"
		this.addTPlinks ();
		
		// Aggiungo una piazza a tutti gli end event che non hanno un "control flow" in uscita.
		// Tuttavia, nel caso in cui l'end event si trovasse all'interno di un subProcess, all'end
		// event viene aggiunto un collegamento con l'elemento BPMN che ha in ingresso il subProcess
		// e, in questo caso, la piazza non va aggiunta
		this.addPlaceToEndEvents ();
		
		// Si crea un ciclo infinito nella rete di Petri per evitare deadlock
		this.addTransitionsFromStartEventToEndEvent ();
		
		
		// Aggiungo il token sui place degli start-event
		this.addInitialMarkingToPlaces ();
		
		
		// Aggiungo il messageFlow
		this.addMessageFlowElements ();
		
		
		// Gestione dei dataobject
		//this.addDataObjects ();
		

		// Mapping terminato, restituisco la rete di Petri
		return this.petriNet;
	}

	
	/**
	 * Fase di pre-processing dove controllo che ogni elemento BPMN sia gestito dal plugin
	 * 
	 * @throws ExceptionMapping             Eccezione generata se almeno uno dei tag XML non e' gestito dal plugin    
	 * @throws IOException                  Eccezione generata dal metodo new XMLPreProcessingNotManagedElements (...).parse ()
	 * @throws SAXException                 Eccezione generata dal metodo new XMLPreProcessingNotManagedElements (...).parse ()
	 * @throws ParserConfigurationException Eccezione generata dal metodo new XMLPreProcessingNotManagedElements (...).parse ()
	 */
	private void preProcessing () throws ExceptionMapping, ParserConfigurationException, SAXException, IOException
	{
		ArrayList<String> notManagedElements = new XMLPreProcessingNotManagedElements (this.fileName).parse ();
		String error = GenericUtils._l (lang, "exception") + " ExceptionMapping. " + GenericUtils._l (lang, "message") + ":\n";
		
		if (notManagedElements.size () == 0)
			return;
		
		for (int k = 0, size_nme = notManagedElements.size (); k < size_nme; k++)
			error += GenericUtils._l (lang, "element_not_managed") + " " + notManagedElements.get (k) + "\n";
		
		throw new ExceptionMapping (error);
	}
	
	
	/**
	 * Rimuovo gli elementi BPMN che non hanno altri elementi BPMN in ingresso o in uscita
	 */
	private void removeBPMNelementsWithoutFlow ()
	{
	    ArrayList<Integer> delIndex = new ArrayList<Integer> ();
	    
		for (int k = 0, size_b = this.b.size (); k < size_b; k++)
			if (this.b.get (k).getSequenceFlowIncoming ().size () == 0 && this.b.get (k).getSequenceFlowOutgoing ().size () == 0) 
			    delIndex.add (k);
	     
		for (int k = delIndex.size () - 1; k >= 0; k--)
            this.b.remove (delIndex.get (k).intValue ()); 
	}
	
	
	/**
	 * Risolvo i collegamenti tra elementi BPMN collegati a subElement e viceversa.
	 * 
	 * Esempio:
	 * 
	 * (start_1) --> [ (start_2) --> (task_1) --> (end_1) ] --> (end_2)
	 * 
	 * Supponendo che gli elementi tra parentesi [ ] siano interni ad un qualunque subElement,
	 * per creare un normale flusso tra elementi BPMN che possono essere mappati, i collegamenti
	 * vengono trasformati in questo modo:
	 * 
	 * (start_1) --> (start_2) --> (task_1) --> (end_1) --> (end_2)
	 * 
	 * Inoltre, per convezione, ogni subElement deve essere in una di queste due forme:
	 *  - (start_event_1) --> ... --> (end_event_1), quindi iniziare con uno "start event" e terminare con un "end event";
	 *  - subElement vuoto, in questo caso viene trasformato in un normale "task".
	 */
	private void setSequenceFlowForSubElements ()
	{
		// Memorizzo gli indici dei subElement che non contengono elementi, per eliminarli successivamente.
		// Questi subElement vengono trasformati in task
		ArrayList<Integer> indexsubElementsToRemove = new ArrayList<Integer> ();
		
		for (int k = 0, size_se = this.subElements.size (); k < size_se; k++) {
			SubElements s = this.subElements.get (k);
			
			ArrayList<String> elBPMNSubProcess = this.subElements.get (k).getBpmnElementsId ();
			
			if (elBPMNSubProcess.size () == 0) {
				ArrayList<String> incomingFlow = s.getSequenceFlowIncoming ();
				ArrayList<String> outgoingFlow = s.getSequenceFlowOutgoing ();
				
				if (incomingFlow.size () > 0 && outgoingFlow.size () > 0) {
					BPMNElement b = new BPMNElement (
						"bpmn2:task",
						"empty" + StringUtils.__trim (s.getName ()),
						"empty" + StringUtils.__trim (s.getName ())
					);
					
					b.addsequenceFlowIncoming (incomingFlow.get (0));
					b.addsequenceFlowOutgoing (outgoingFlow.get (0));
					
					b.setidSubElement (s.getId ());
					
					this.b.add (b);
					
					indexsubElementsToRemove.add (k);
				}
			}
			
			for (int j = 0, size_e = elBPMNSubProcess.size (); j < size_e; j++) {
				for (int i = 0, size_b  = this.b.size (); i < size_b; i++) {
					if (elBPMNSubProcess.get (j).equals (this.b.get (i).getId ())) {
						if (this.b.get (i).isStartEvent ()) {
							// Il primo sequence-flow incoming del sub-elements, e' il flow che viene attaccato fuori dal sottoprocesso
							if (s.getSequenceFlowIncoming ().size () > 0) {
								this.b.get (i).addsequenceFlowIncoming (s.getSequenceFlowIncoming ().get (0));
								
								if (Transformation.debug)
									System.out.println ("setSequenceFlowForSubElements Incoming, from=" + this.b.get (i).getId () + ", to=" + s.getSequenceFlowIncoming ().size ()
											+ " " + s.getSequenceFlowIncoming ().get (0));
							}	
						}
						
						else if (this.b.get (i).isEndEvent ()) {
							if (s.getSequenceFlowOutgoing ().size () > 0) {
								this.b.get (i).addsequenceFlowOutgoing (s.getSequenceFlowOutgoing ().get (0));
								
								if (Transformation.debug)
									System.out.println ("setSequenceFlowForSubElements Outgoing, from=" + this.b.get (i).getId () + ", to=" + s.getSequenceFlowIncoming ().size ()
											+ " " + s.getSequenceFlowOutgoing ().get (0));
							}
						}
					}	
				}
			}
		}
		
		
		for (int k = indexsubElementsToRemove.size () - 1; k >= 0; k--)
		    this.subElements.remove (indexsubElementsToRemove.get (k).intValue ());
	}
	
	
	/**
	 * Se gli elementi BPMN fanno parte di un SubElement, aggiungo, nell'istanta dell'oggetto BPMN,
	 * l'id del SubElement
	 */
	private void addIdSubElementOnBPMNElements ()
	{
		for (int k = 0, size_s = this.subElements.size (); k < size_s; k++) {
			ArrayList<String> bpmnElementsSubProcess = this.subElements.get (k).getBpmnElementsId ();
			
			for (int j = 0, size_se = bpmnElementsSubProcess.size (); j < size_se; j++)
				for (int i = 0, size_b = this.b.size (); i < size_b; i++)
					if (bpmnElementsSubProcess.get (j).equals (this.b.get (i).getId ()))
						this.b.get (i).setidSubElement (this.subElements.get (k).getId ());
		}
	}
	
	
	/**
	 * Rinomino gli elementi in base al nodo "padre" (pool, lane, ecc.) e subElements (subProcess, adHocSubprocess, subChoreography)
	 * ---
	 * Se l'elemento BPMN e' un "choreographyTask", posso avere dei "participantRef" al suo interno.
	 * Esempio:
	 * 
	 * <bpmn2:choreographyTask id="ChoreographyTask_2" name="Choreography Task 2" initiatingParticipantRef="_Participant_2">
     *   <bpmn2:participantRef>_Participant_2</bpmn2:participantRef>
     * </bpmn2:choreographyTask>
     * 
     * "_Participant_2" e' l'id del "participant", che e' dichiarato fuori dal nodo "bpmn2:choreographyTask":
     * 
     * <bpmn2:participant id="_Participant_2" name="Participant 2"/>
     * 
     * All'id dell'elemento BPMN andranno aggiunti tutti i nomi dei partecipanti (senza gli spazi).
     * In questo caso, visto che name="Participant 2", aggiungeremo "Participant2".
     * ---
     * Rinomino tutti gli eventi che hanno un "event definition"
     * ---
	 * 
	 * @throws IOException                  Eccezioni generate dal metodo new XMLPrefixNameElements (...).parse (...)
	 * @throws SAXException                 Eccezioni generate dal metodo new XMLPrefixNameElements (...).parse (...)
	 * @throws ParserConfigurationException Eccezioni generate dal metodo new XMLPrefixNameElements (...).parse (...)
	 * 
	 */
	private void renameElements () throws ParserConfigurationException, SAXException, IOException
	{
		// Determino i nuovi nomi da assegnare ad ogni elemento BPMN, in base all'elemento di cui fanno parte
		ArrayList<String> renamedElements = new XMLPrefixNameElements (this.fileName).parse (this.b, this.mappedElements, this.mappingRules);
		int size_b  = this.b.size ();
			
		// In caso di errori nel parser per determinare i nuovi nomi, non rinomino nessun elemento BPMN
		if (this.b.size () != renamedElements.size ())
			return;
		
		// this.b.size () = renamedElements.size () 
		// Il carattere "-" non viene gestito su dot. Quindi, sostituisco "-" con ""
		
		// Rinomino gli elementi in base al nodo "padre" (pool, lane, ecc.)
		for (int k = 0; k < size_b; k++) {
		    // --- Prima rinominazione
		    // renamedElement ha come default l'id dell'elemento BPMN, quindi, nel caso in cui l'elemento,
		    // non appartiene a nessun "sub element", renamedElement[i] = id[i]
		    String renamedElement = renamedElements.get (k).replace ("-", "").replace (".", "");
		    
			this.b.get (k).setRenamedID (renamedElement);
			this.b.get (k).setRenamedName (renamedElement);
			
			
			// --- Seconda rinominazione, in base all'appartenenza di un elemento BPMN a un "sub element"
			// Rinomino gli elementi in base al subElements (subProcess, adHocSubprocess, subChoreography)
			for (int j = 0, size_se = this.subElements.size (); j < size_se; j++) {
			    ArrayList<String> bpmnElementsId = this.subElements.get (j).getBpmnElementsId ();
			    
			    for (int i = 0, size_bi = bpmnElementsId.size (); i < size_bi; i++) {
			        for (int x = 0; x < size_b; x++) {
			            if (bpmnElementsId.get (i).equals (this.b.get (x).getId ())) {
			                String renamedElementPrefix = StringUtils.__trim (this.subElements.get (j).getName ()) + "_";
			                
			                this.b.get (x).setRenamedID (renamedElementPrefix + this.b.get (x).getId ());
	                        this.b.get (x).setRenamedName (renamedElementPrefix + this.b.get (x).getName ());
			            }
			        }
			    }
			}
			
			
			// --- Terza rinominazione, rinomino gli elementi in base all'event definition
			BPMNElement b_k = this.b.get (k);
            String eventDefinition = b_k.getEventDefinition ();
            
            // Gli eventDefinition sono dichiarati all'interno del fileName parser.xml.XMLReadBPMNElements.java e per ora sono della forma
            // bpmn2:X. Quindi, nel caso in cui venga aggiunto un evento senza il prefisso "bpmn2:" aggiungo direttamente il valore,
            // altrimenti, tolgo il prefisso "bpmn2:" e aggiungo il resto.
            if (eventDefinition != null) {
                this.b.get (k).setRenamedID (
                    b_k.getRenamedID () + (eventDefinition.contains (":") ? eventDefinition.split (":")[1] : eventDefinition)
                );
                
                this.b.get (k).setRenamedName (
                    b_k.getRenamedName () + (eventDefinition.contains (":") ? eventDefinition.split (":")[1] : eventDefinition)
                );
            }
            
            
            // --- Quarta rinominazione, rinomino gli elementi in base ai "participant"
            ArrayList<String> participants = b_k.getParticipantName ();
            int size_p                     = participants.size ();

            if (participants.size () > 0) {
                // Il toString () dell'ArrayList aggiunge i delimitatori [], che non sono gestiti da "dot"
                String arrayListToS = "";
                
                for (int j = 0; j < size_p; j++) {
                    arrayListToS += (j == 0) ? "<" : "";
                    arrayListToS += participants.get (j);
                    arrayListToS += (j >= 0 && j < (size_p - 1)) ? "," : "";
                }
                arrayListToS += ">";
                
                
                this.b.get (k).setRenamedID (
                    b_k.getRenamedID () + "p" + arrayListToS
                );
                
                this.b.get (k).setRenamedName (
                    b_k.getRenamedName () + "p" + arrayListToS
                );
            }
            
            
            /*
             * Caratteri ammessi da dot:
             * 
             * An ID is one of the following (http://www.graphviz.org/doc/info/lang.html):
             *    - any string of alphabetic ([a-zA-Z\200-\377]) characters, underscores ('_') or digits ([0-9]), not beginning with a digit;
             *    - a numeral [-]?(.[0-9]+ | [0-9]+(.[0-9]*)? );
             *    - any double-quoted string ("...") possibly containing escaped quotes (\")1;
             *    - an HTML string (<...>).
             */
            String renamedId = b_k.getRenamedID (), 
                   renamedName = b_k.getRenamedName (),
                   renamedIdDot = StringUtils.getDOTString (renamedId),
                   renamedNameDot = StringUtils.getDOTString (renamedName);
            
            // renamedId contiene caratteri non ammessi da dot
            if (!renamedId.equals (renamedIdDot))
                this.b.get (k).setRenamedID (renamedIdDot);
            
            // renamedName contiene caratteri non ammessi da dot
            if (!renamedName.equals (renamedNameDot))
                this.b.get (k).setRenamedName (renamedNameDot);   
		}
	}
	
	
	/**
	 * Questo metodo trasforma l'elemento BPMN nella corrispondente rete di Petri
	 * 
	 * @param b                 Elemento BPMN in esame
	 * @throws ExceptionMapping Eccezione generata dai metodi applyRule (...)
	 */
	private void getPetriNetElementsByBPMNelement (BPMNElement b) throws ExceptionMapping
	{
		for (int k = 0, size_mr = mappingRules.size (); k < size_mr; k++) {
			ArrayList<String> e = mappingRules.get (k).getElementsMapped ();
			
			int size_e = e.size ();
			
			for (int j = 0; j < size_e; j++) {
				if (b.getType ().equals (e.get (j))) {
					// Rete di Petri dell'elemento BPMN in esame
					PEP localPetriNet = new PEP ();
					
					// Applico le regole per generare le "piazze" (Places)
					PEP petriNetOnlyPlaces = mappingRules.get (k).getPrule ().applyRule (b, lang);
					
					// Aggiungo la rete di Petri contenente solo "piazze" nella rete di Petri denominata "localPetriNet"
					localPetriNet.mergeWithAnotherPetriNet (petriNetOnlyPlaces);
					
					
					// Applico le regole per generare le "transizioni" (Transitions)
					PEP petriNetOnlyTransitions = mappingRules.get (k).getTrule ().applyRule (b, lang);
					
					// Aggiungo la rete di Petri contenente solo "transizioni" nella rete di Petri denominata "localPetriNet"
					localPetriNet.mergeWithAnotherPetriNet (petriNetOnlyTransitions);
					
					
					// Applico le regole per generare i collegamenti P->T (Place to Transitions)
					PTRule ptRule = mappingRules.get (k).getPTrule ();
					
					if (ptRule != null) {
						PEP petriNetWithP_T_PT = ptRule.applyRule (
							b, localPetriNet
						);
						
						// Aggiungo la rete di Petri contenente solo collegamenti "PT" nella rete di Petri denominata "localPetriNet"
						localPetriNet.mergeWithAnotherPetriNet (petriNetWithP_T_PT);
					}
					
					
					// Applico le regole per generare i collegamenti T->P (Transitions to Place)
					TPRule tpRule = mappingRules.get (k).getTPrule ();
					
					if (tpRule != null) {
						PEP petriNetWithP_T_PT_TP = tpRule.applyRule (
							b, localPetriNet
						);
						
						// Aggiungo la rete di Petri contenente solo collegamenti "PT" nella rete di Petri denominata "localPetriNet"
						localPetriNet.mergeWithAnotherPetriNet (petriNetWithP_T_PT_TP);
					}
					
					// Alla rete di Petri passata come parametro, viene aggiunta la rete di Petri generata
					// per l'elemento BPMN "b" in esame
					this.petriNet.mergeWithAnotherPetriNet (localPetriNet);
					
					// Imposto la rete di Petri per l'elemento BPMN "b" appena esaminato
					b.setPetriNet (localPetriNet);
				}
			}
		}
	}
	
	
	/**
	 * Aggiungo una piazza a tutti gli end event che non hanno un "sequence flow" in uscita
	 */
	private void addPlaceToEndEvents ()
	{
		for (int k = 0, size_b = this.b.size (); k < size_b; k++) {
			BPMNElement b_k = this.b.get (k);
			
			if (b_k.isEndEvent () && b_k.getSequenceFlowOutgoing ().size () == 0) {
				ArrayList<String> placesEndEvent      = this.petriNet.getOnlyPlace ();
				ArrayList<String> transitionsEndEvent = b_k.getPetriNet ().getT ();
				String idNewPlace = "pEndEvent" + b_k.getRenamedID ();
				
				String newPlace = (!placesEndEvent.contains (idNewPlace)) 
						? idNewPlace
						: GenericUtils.generateNamePlaceForPetriNet (idNewPlace, this.petriNet);

				int transitionsEndEventSize = transitionsEndEvent.size ();
				
				if (transitionsEndEventSize > 0) {
					this.petriNet.addP (newPlace);
					this.petriNet.addTP (transitionsEndEvent.get (transitionsEndEventSize - 1), newPlace);
					
					this.b.get (k).getPetriNet ().addP (newPlace);
					this.b.get (k).getPetriNet ().addTP (transitionsEndEvent.get (transitionsEndEventSize - 1), newPlace);
				}
			}
		}
	}

	
	/**
	 * Aggiungo il collegamento TP (Transition to Place) alla rete di Petri.
	 * Per aggiungere questo collegamento, per ogni elemento BPMN ottengo i sequence flow in uscita
	 * e cerco gli elementi BPMN che hanno come sequence flow di ingresso, l'i-esimo sequence flow in uscita
	 * 
	 * @throws ExceptionMapping Eccezione generata nel caso in cui un sequence flow in uscita non e' collegato
	 *                          a nessun elemento BPMN
	 */
	private void addTPlinks () throws ExceptionMapping 
	{
		ArrayList<String> bpmnElementsIDfrom = new ArrayList<String> ();
		ArrayList<String> bpmnElementsIDto   = new ArrayList<String> ();
		
		for (int k = 0, size_b = this.b.size (); k < size_b; k++) {
			BPMNElement b_k = this.b.get (k);
			ArrayList<String> t = b_k.getPetriNet ().getT ();
			ArrayList<String> outgoingSequenceFlow = b_k.getSequenceFlowOutgoing ();
			int size_osf = outgoingSequenceFlow.size ();
			
			for (int j = 0; j < size_osf; j++) {
				BPMNElement elementTo = this.getBPMNElementsConnectedBySourceElement (outgoingSequenceFlow.get (j));
				
				if (elementTo == null) {
					String messageException = GenericUtils._l (lang, "exception") + " ExceptionMapping. " + GenericUtils._l (lang, "message") + ":\n";
					
					messageException += "addTPlinks ():\n";
					messageException += GenericUtils._l (lang, "sequence_flow_outgoing_no_elements") + "(" + outgoingSequenceFlow.get (j) + ", " + b_k.getName () + ")";
					
					throw new ExceptionMapping (messageException);
				}
				
				ArrayList<String> allPlaces = elementTo.getPetriNet ().getOnlyPlace ();
				
				int n_transition = ArrayListUtils.countOccurrencesString (bpmnElementsIDfrom, b_k.getId ());
				int n_place      = ArrayListUtils.countOccurrencesString (bpmnElementsIDto, elementTo.getId ());
				
				if (Transformation.debug) {
					System.out.println ("BPMN from: " + b_k.getId ());
					System.out.println ("BPMN to:   " + elementTo.getId ());
					System.out.println ("Collego n_transition=" + n_transition + ", n_place=" + n_place + "\n");
				}
				
				MappingRule mr = this.getMappingRuleByTagBPMNElement (b_k.getType ());
				
				if (mr == null) {
					String messageException = GenericUtils._l (lang, "exception") + " ExceptionMapping. " + GenericUtils._l (lang, "message") + ":\n";
					
					messageException += "addTPlinks ():\n";
					messageException += GenericUtils._l (lang, "no_mapping_rule") + ": " + b_k.getType () + "!";
					
					throw new ExceptionMapping (messageException);
				}
				
				if (Transformation.debug) {
					System.out.println ("BPMN from:      " + b_k.getId ());
					System.out.println ("Mapping rule:   " + ((mr != null) ? mr.toString () : "NULL"));
				}
				

				/*
				 * Ottengo il numero di transizione in base al valore di "initialTransition" - Inizio
				 */
				String initialTransition   = mr.getInitialTransition ();
				int valueInitialTransition = 0;
				
				String[] KEYWORD_INIT_TRANSITION = { "$n_incoming", "$n_outgoing" };

				
				// Se l'attributo "n" e' un numero, si determina il corrispondente intero della stringa
				if (NumberUtils.isNumber (initialTransition))
					valueInitialTransition = StringUtils.getIntByString (initialTransition);
				
				// L'attributo "n" e' una keyword. In questa prima versione, le keyword possibili sono: "$n_incoming" e "$n_outgoing"
				else if (StringUtils.isKeyword (initialTransition)) {
					for (int i = 0; i < KEYWORD_INIT_TRANSITION.length; i++) {
						if (initialTransition.equals (KEYWORD_INIT_TRANSITION[i])) {
							switch (i) {
							case 0:
								valueInitialTransition = b_k.getSequenceFlowIncoming ().size ();
								break;
							
							case 1:
								valueInitialTransition = b_k.getSequenceFlowOutgoing ().size ();
								break;
							}
						}
					}
				}
				
				// L'attributo "n" e' un'espressione
				else if (StringUtils.isExpression (initialTransition)) {
					int resultExpression = GenericUtils.evaluateExpression (
							                   // L'espressione puo' contenere keyword, quindi modifico il valore dell'espressione
			                                   // inserendo il valore della keyword al posto della keyword.
			                                   // Esempio: ($n_outgoing + 2) --> (1 + 3), assumendo che $n_outgoing = 1
		                                       GenericUtils.replaceKeywordWithValue (initialTransition, b_k, KEYWORD_INIT_TRANSITION)
							               );

					if (resultExpression == -1) {
						String messageException = GenericUtils._l (lang, "exception") + " ExceptionMapping. " + GenericUtils._l (lang, "message") + ":\n";
						
						messageException += "addTPlinks ():\n";
						messageException += GenericUtils._l (lang, "wrong_expression") + "! (" + initialTransition + ")";
						
						throw new ExceptionMapping (messageException);
					}
					else
						valueInitialTransition = resultExpression;
				}
				/*
				 * Ottengo il numero di transizione in base al valore di "initialTransition" - Fine
				 */
				

				if (mr.isUnique ())
					n_transition = (valueInitialTransition > 0) ? --valueInitialTransition : valueInitialTransition;
				else
					n_transition += (valueInitialTransition > 0) ? --valueInitialTransition : valueInitialTransition;
				/*
				if (b_k.getType ().equals ("bpmn2:exclusiveGateway") || b_k.getType ().equals ("exclusiveGateway"))
					n_transition += b_k.getSequenceFlowIncoming ().size ();
				else if (b_k.getType ().equals ("bpmn2:parallelGateway") || b_k.getType ().equals ("parallelGateway"))
					n_transition = 0;
				else if (b_k.getType ().equals ("bpmn2:task") || b_k.getType ().equals ("task"))
					n_transition = 1;
				else if (b_k.getType ().equals ("bpmn2:choreographyTask") || b_k.getType ().equals ("choreographyTask"))
					n_transition = 1;
					*/
				
				if (Transformation.debug) {
				    System.out.println ("Selezione t numero: " + n_transition + ", per " + b_k.getId() + ", valueInitialTransition = " + valueInitialTransition);
				    System.out.println (n_place);
				}
				
				if (n_transition <= (t.size () - 1) && n_place <= (allPlaces.size () - 1)) {
					this.petriNet.addTP (
						t.get (n_transition), 		// Transition
						allPlaces.get (n_place)		// Place
					);
					
					if (Transformation.debug)
					    System.out.println ("[true] Selezione t numero: " + n_transition + ", per " + b_k.getId() + ", place n = " + allPlaces.get (n_place) + "\n\n");
				}
				if (Transformation.debug)
				    System.out.println ("[false] Selezione t numero: " + n_transition + ", per " + b_k.getId() + ", place n = " + n_place + "\n\n");
				

				bpmnElementsIDfrom.add (b_k.getId ());
				bpmnElementsIDto.add (elementTo.getId ());
			}
		}
	}
	
	
	/**
	 * Aggiungo una transizione tra la piazza dell'end event e la piazza dello start event.
	 * Ovviamente lo start event deve essere "collegato" all'end event (per "collegato" intendiamo
	 * che esista un percorso formato da un numero limitato di elementi BPMN che permette di arrivare
	 * dallo start event all'end event). Infatti, in un BPMN, possiamo avere piu' start event e altrettanti
	 * end event che non per forza sono collegati tra loro.
	 * Supponiamo di avere questo processo:
	 * 
	 * (start_1) --> (task) --> (task) --> ... ---> ... ---> (end_1)
	 * (start_2) --> (task) --> ... ---> ... ---> (end_2)
	 * 
	 * in questo caso la transizione va aggiunta tra le piazze di (start_1, end_1) e tra le piazze di (start_2, end_2).
	 * Infatti, non esiste alcun percorso che ci permette di arrivare da "start_1" a "end_2" e un altro che ci permette
	 * di arrivare da "start_2" a "end_1"
	 * 
	 * @throws ExceptionMapping  Eccezione che puo' essere generata dal metodo getDestinationElements (...)
	 */
	private void addTransitionsFromStartEventToEndEvent () throws ExceptionMapping
	{
	    ArrayList<Pair<BPMNElement, BPMNElement>> end_start = new ArrayList<Pair<BPMNElement, BPMNElement>> ();
	    BPMNElement endEvent = null, startEvent = null;
	    
	    for (int k = 0, size_b = this.b.size (); k < size_b; k++) {
            BPMNElement b_k = this.b.get (k);
            
            if (b_k.isEndEvent () && b_k.getSequenceFlowOutgoing ().size () == 0) {
                ArrayList<BPMNElement> res = new ArrayList<BPMNElement> ();
                endEvent   = this.b.get (k);
                this.getStartEventFromBPMNElement (endEvent, res, new ArrayList<String> ());

                if (res.size () == 0) {
                    String messageException = GenericUtils._l (lang, "exception") + " ExceptionMapping. " + GenericUtils._l (lang, "message") + ":\n";
                    
                    messageException += "addTransitionsFromStartEventToEndEvent (...):\n";
                    messageException += GenericUtils._l (lang, "no_start_event_to_end_event") + " (" + endEvent.getId () + ")";
                    
                    throw new ExceptionMapping (messageException);
                }
                
                end_start.add (new Pair (endEvent, res.get (0)));
            }
        }
	    
	    for (int k = 0, size_es = end_start.size (); k < size_es; k++) {
	        if (Transformation.debug)
	            System.out.println (end_start.get(k).getFirstElement().getId() + ", " + end_start.get(k).getSecondElement().getId());
	        
	        endEvent   = end_start.get (k).getFirstElement ();
	        startEvent = end_start.get (k).getSecondElement ();
	        
	        String idNewTransition = "t" + startEvent.getRenamedID () + "_" + endEvent.getRenamedID ();

            String newTransition = (!this.petriNet.getT ().contains (idNewTransition)) 
                    ? idNewTransition
                    : GenericUtils.generateNamePlaceForPetriNet (idNewTransition, this.petriNet);
            
            ArrayList<String> placesEndEvent   = endEvent.getPetriNet ().getOnlyPlace ();
            ArrayList<String> placesStartEvent = startEvent.getPetriNet ().getOnlyPlace ();
            
            if (startEvent.getPetriNet ().getOnlyPlace ().size () > 0 && endEvent.getPetriNet ().getOnlyPlace ().size () > 1) {
                this.petriNet.addT (newTransition);
                this.petriNet.addPT (placesEndEvent.get (placesEndEvent.size () - 1), newTransition);
                this.petriNet.addTP (newTransition, placesStartEvent.get (0));
            }
	    }
	}
	
	
	/**
	 * Metedo ricorsivo che, partendo dall'end event, ricava lo start event di partenza
	 * 
	 * @param bElement           Elemento BPMN su cui avviene la ricorsione (la prima chiamata ha come elemento BPMN un end event)
	 * @return                   Elemento BPMN collegato all'end event, "null" se l'end event non e' collegato ad uno start event
	 * @throws ExceptionMapping  Eccezione generata nel caso in cui un elemento BPMN non ha nessun sequence flow in entrata
	 */
	private void getStartEventFromBPMNElement (BPMNElement bElement, ArrayList<BPMNElement> res, ArrayList<String> path) throws ExceptionMapping
	{
	    ArrayList<String> incomingSequenceFlow = bElement.getSequenceFlowIncoming ();
	    
	    if (Transformation.debug)
	        System.out.println ("CALL getStartEventFromBPMNElement (" + bElement.getName () + ")");
	    
	    for (int k = 0, size_isf = incomingSequenceFlow.size (); k < size_isf; k++) {
	        String sequenceFlowName    = incomingSequenceFlow.get (k);
	        BPMNElement bElementSource = null;
	        boolean found              = false;
	        boolean repeatedElement    = false;
	        
	        for (int j = 0, size_b = this.b.size (); j < size_b; j++) {
	            ArrayList<String> outgoingSequenceFlow = this.b.get (j).getSequenceFlowOutgoing ();
	            
	            for (int i = 0, size_osf = outgoingSequenceFlow.size (); i < size_osf; i++) {
	                if (sequenceFlowName.equals (outgoingSequenceFlow.get (i))) {
	                    bElementSource = this.b.get (j);
	                    
	                    if (path.contains (bElementSource.getId ())) {
	                        repeatedElement = true;
	                        break;
	                    }
	                    
	                    found = true;
	                    break;
	                }
	            }
	            
	            if (found)
	                break;
	            
	            if (repeatedElement)
                    break;
	        }

	        if (repeatedElement == false) {
    	        if (bElementSource == null) {
    	            String messageException = GenericUtils._l (lang, "exception") + " ExceptionMapping. " + GenericUtils._l (lang, "message") + ":\n";
                    
                    messageException += "getDestinationElements (...):\n";
                    messageException += GenericUtils._l (lang, "sequence_flow_outgoing_no_elements") + " (" + incomingSequenceFlow.get (k) + ")";
                    
                    throw new ExceptionMapping (messageException);
    	        }
    	        
    	        if (bElementSource.isStartEvent () /*&& bElementSource.getSequenceFlowIncoming ().size () == 0 && bElementSource.getRefIncomingMessageFlow ().size () == 0*/)
    	            res.add (bElementSource);
    	        else {
    	            if (!path.contains (bElementSource.getId ()))
    	                path.add (bElementSource.getId ());
    	            
    	            this.getStartEventFromBPMNElement (bElementSource, res, path);
    	        }
	        }
	    }
	}
	
	
	/**
	 * Metodo che aggiunge l'initial marking agli "start event"
	 */
	private void addInitialMarkingToPlaces ()
	{
		// Se tutti gli start event non hanno "incoming flow" e "message flow" in entrata
		// l'initial marking viene dato a caso ad uno degli start events
		ArrayList<Integer> startEventsIndex = new ArrayList<Integer> ();
		
		for (int k = 0, size_b = this.b.size (); k < size_b; k++) {
			BPMNElement b_k = this.b.get (k);
			
			if (b_k.isStartEvent () && b_k.getSequenceFlowIncoming ().size () == 0) {
				if (b_k.getRefIncomingMessageFlow ().size () > 0)
					startEventsIndex.add (k);
				else {
					startEventsIndex = null;
					
					break;
				}
			}
		}
		// Tutti gli start event hanno un "message flow" in entrata, quindi scelgo a caso uno degli 
		// start event per l'"initial marking"
		if (startEventsIndex != null && startEventsIndex.size () > 0) {
			int nStartEvent = NumberUtils.generateInteger (0, startEventsIndex.size () - 1);
			
			ArrayList<String> pStartEvent = this.b.get (startEventsIndex.get (nStartEvent)).getPetriNet ().getOnlyPlace ();
			
			// Per sicurezza controllo che il numero delle piazze dello "start event" sia > 0
			if (pStartEvent.size () > 0)
				this.petriNet.setInitialMarking (pStartEvent.get (0));
			
			return;
		}
		
		
		for (int k = 0, size_b = this.b.size (); k < size_b; k++) {
			BPMNElement b_k = this.b.get (k);
			
			if (b_k.isStartEvent () && b_k.getSequenceFlowIncoming ().size () == 0 && b_k.getRefIncomingMessageFlow ().size () == 0) {
				// Ottengo tutte le piazze dello "start event"
				ArrayList<String> pStartEvent = b_k.getPetriNet ().getOnlyPlace ();
				
				// Per sicurezza controllo che il numero delle piazze dello "start event" sia > 0
				if (pStartEvent.size () > 0)
					this.petriNet.setInitialMarking (pStartEvent.get (0));
			}
		}
	}
	
	
	/**
	 * Aggiungo il "message flow"
	 * 
	 * @throws IOException                  Eccezione generata dal metodo new XMLMessageFlow (...).parse (...)
	 * @throws SAXException                 Eccezione generata dal metodo new XMLMessageFlow (...).parse (...)
	 * @throws ParserConfigurationException Eccezione generata dal metodo new XMLMessageFlow (...).parse (...)
	 * @throws ExceptionMapping             Eccezione generata nel caso in cui il metodo getInstanceBPMNElementById (...)
	 *                                      restituisca il valore "null"
	 */
	private void addMessageFlowElements () throws ParserConfigurationException, SAXException, IOException, ExceptionMapping
	{
		// Ottengo il message flow. Il primo elemento della coppia indica l'id del BPMN sorgente
		// mentre il secondo elemento della coppia indica l'id del BPMN destinatario
		ArrayList<Pair<String, String>> messageFlow = new XMLMessageFlow (this.fileName).parse ();

		if (Transformation.debug) {
			System.out.println ("\n\n|messageFlow| = " + messageFlow.size ());
			
			for (int k = 0, size_mf = messageFlow.size (); k < size_mf; k++)
				System.out.println ("messageFlow[" + k + "]=" + messageFlow.get (k).getFirstElement () + ", " + messageFlow.get (k).getSecondElement ());
		}
		
		for (int k = 0, size_m = messageFlow.size (); k < size_m; k++) {
			// Primo elemento della coppia
			BPMNElement b_1 = getInstanceBPMNElementById (messageFlow.get (k).getFirstElement ());
			
			// Secondo elemento della coppia
			BPMNElement b_2 = getInstanceBPMNElementById (messageFlow.get (k).getSecondElement ());
			
			if (b_1 != null && b_2 != null) {
			    // Task ---> Start Event
				if (b_1.isTask () && b_2.isStartEvent ()) {
					ArrayList<String> tB_1 = b_1.getPetriNet ().getT ();
					ArrayList<String> pB_2 = b_2.getPetriNet ().getOnlyPlace ();
					
					if (tB_1.size () > 0 && pB_2.size () > 0)
						this.petriNet.addTP (tB_1.get (0), pB_2.get (0));
				}
				// Task ---> Task
				else if (b_1.isTask () && b_2.isTask ()) {
					ArrayList<String> tB_1 = b_1.getPetriNet ().getT ();
					ArrayList<String> tB_2 = b_2.getPetriNet ().getT ();
					ArrayList<String> allPlacesPetriNet = this.petriNet.getOnlyPlace ();
					String idNewPlace = b_1.getRenamedID () + "_" + b_2.getRenamedID () + "_MessageFlow";
					
					
					
					if (tB_1.size () > 0 && tB_2.size () > 0) {
						String newPlace = (!allPlacesPetriNet.contains (idNewPlace)) 
								? idNewPlace
								: GenericUtils.generateNamePlaceForPetriNet (idNewPlace, this.petriNet);
						
						this.petriNet.addP (newPlace);
						this.petriNet.addTP (tB_1.get (0), newPlace);
						this.petriNet.addPT (newPlace, tB_2.get (0));
					}
				}
				// End Event ---> Task
				else if (b_1.isEndEvent () && b_2.isTask ()) {
					ArrayList<String> tB_1 = b_1.getPetriNet ().getT ();
					ArrayList<String> tB_2 = b_2.getPetriNet ().getT ();
					ArrayList<String> allPlacesPetriNet = this.petriNet.getOnlyPlace ();
					String idNewPlace = b_1.getRenamedID () + "_" + b_2.getRenamedID () + "_MessageFlow";
					
					
					
					if (tB_1.size () > 0 && tB_2.size () > 0) {
						String newPlace = (!allPlacesPetriNet.contains (idNewPlace)) 
								? idNewPlace
								: GenericUtils.generateNamePlaceForPetriNet (idNewPlace, this.petriNet);
						
						this.petriNet.addP (newPlace);
						this.petriNet.addTP (tB_1.get (0), newPlace);
						this.petriNet.addPT (newPlace, tB_2.get (0));
					}
				}
				// End Event ---> Start Event
				else if (b_1.isEndEvent () && b_2.isStartEvent ()) {
					ArrayList<String> tB_1 = b_1.getPetriNet ().getT ();
					ArrayList<String> pB_2 = b_2.getPetriNet ().getOnlyPlace ();
					
					if (tB_1.size () > 0 && pB_2.size () > 0)
						this.petriNet.addTP (tB_1.get (0), pB_2.get (0));
				}
			}
			// Uno dei due elementi BPMN, o entrambi, non viene/vengono trovato/i: lancio l'eccezione
			else {
				String messageException = GenericUtils._l (lang, "exception") + " ExceptionMapping. " + GenericUtils._l (lang, "message") + ":\n";
				
				messageException += "addMessageFlowElements ():\n";
				messageException += (b_1 == null) ? GenericUtils._l (lang, "element") + " b_1 = null, " + messageFlow.get (k).getFirstElement () : "";
				messageException += (b_2 == null) ? GenericUtils._l (lang, "element") + " b_2 = null, " + messageFlow.get (k).getSecondElement () : "";
				
				throw new ExceptionMapping (messageException);
			}
		}
	}
	
	
	/**
	 * Aggiungo il "flow" per i data object
	 * 
	 * @throws IOException                  Eccezione generata dal metodo new XMLDataObjects (...).parse (...)
	 * @throws SAXException                 Eccezione generata dal metodo new XMLDataObjects (...).parse (...)
	 * @throws ParserConfigurationException Eccezione generata dal metodo new XMLDataObjects (...).parse (...)
	 */
	/*
	private void addDataObjects () throws ParserConfigurationException, SAXException, IOException
	{
		// Viene eseguito il parsing per i data object.
		// Durante il parsing, viene aggiunta l'istanza del data object in ingresso e in uscita su ogni
		// istanza dell'ArrayList degli elementi BPMN (this.b). Infatti, la classe BPMNElement contiene due attributi:
		//     - incomingDataFlow (ArrayList<DataObject>): array di DataObject in ingresso all'elemento BPMN;
        //	   - outgoingDataFlow (ArrayList<DataObject>): array di DataObject in uscita all'elemento BPMN.
		new XMLDataObjects (this.fileName).parse (this.b, this.subElements, this.mappedElements);
		
		// ArrayList che tiene traccia dei data object mappati utilizzando l'id del data object
		ArrayList<String> dMappedId = new ArrayList<String> ();
		
        int size_b = this.b.size ();
        
		
		for (int k = 0; k < size_b; k++) {
			BPMNElement b_k = this.b.get (k);
			
			ArrayList<DataObject> dInput  = b_k.getIncomingDataFlow ();
			ArrayList<DataObject> dOutput = b_k.getOutgoingDataFlow ();
			
			int size_di = dInput.size ();
			int size_do = dOutput.size ();
			
			for (int j = 0; j < size_di; j++) {
				ArrayList<DataObject> dInputChangesState = new ArrayList<DataObject> ();
				
				for (int i = 0; i < size_do; i++) {
					// Controllo se ci sono data object che da uno stato in input, cambiano stato in output
					if (dInput.get (j).equals (dOutput.get (i))) {
						System.out.println (dInput.get (j).getId() + " cambia stato in " + dOutput.get (i).getId());
						dInputChangesState.add (dOutput.get (i));
						
					}
				}
			}
		}
	}
	*/
	
	
	/**
	 * Ottengo l'elemento collegato ad un sequence flow in uscita.
	 * Per fare questo basta controllare tutte le sequence flow in ingresso di tutti
	 * gli elementi BPMN
	 * 
	 * @param outgoing Id del sequence flow in uscita
	 * @return         Elemento BPMN collegato al sequence flow in uscita, "null" se nessun elemento e' collegato al
	 *                 sequence flow in uscita
	 */
	private BPMNElement getBPMNElementsConnectedBySourceElement (String outgoing)
	{
		for (int k = 0, size_b = this.b.size (); k < size_b; k++) {
			// Ottengo tutti i sequence flow in ingresso per l'elemento BPMN selezionato
			ArrayList<String> incomingFlow = this.b.get (k).getSequenceFlowIncoming ();
			
			for (int j = 0, size_if = incomingFlow.size (); j < size_if; j++)
				if (incomingFlow.get (j).equals (outgoing))
					return this.b.get (k);
		}
		
		return null;
	}
	
	
	/**
	 * Metodo che ottiene l'istanza dell'oggetto BPMN dal valore del suo attributo "id"
	 * 
	 * @param id Id del BPMN
	 * @return   Istanza dell'oggetto se l'id esiste, "null" altrimenti
	 */
	private BPMNElement getInstanceBPMNElementById (String id)
	{
		for (int k = 0, size_b = this.b.size (); k < size_b; k++)
			if (this.b.get (k).getId ().equals (id))
				return this.b.get (k);
		
		return null;
	}
	
	
	/**
	 * Metodo che ottiene l'istanza della regola di mapping secondo il valore del tag XML dell'elemento BPMN
	 * 
	 * @param tag Tag XML dell'elemento BPMN
	 * @return    Instanza della regola di mapping se il tag XML dell'elemento BPMN ha una regola associata, "null" altrimenti
	 */
	private MappingRule getMappingRuleByTagBPMNElement (String tag)
	{
		for (int k = 0, size_m = this.mappingRules.size (); k < size_m; k++) {
			ArrayList<String> tagElements = this.mappingRules.get (k).getElementsMapped ();
			
			for (int j = 0, size_te = tagElements.size (); j < size_te; j++)
				if (tagElements.get (j).equals (tag))
					return this.mappingRules.get (k);
		}
		
		return null;
	}
}

/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package command;

/**
 * Questa classe rappresenta il risultato prodotto da un comando.
 * Nel nostro caso, il comando rappresenta la chiamata di un programma esterno:
 * 
 *     - cunf:    utilizzato per produrre l'unfolding della rete di Petri
 *                (esempio: "cunf -o unfolding.cuf rete_di_Petri.ll_net");
 *             
 *     - cna:     utilizzato per controllare l'eventuale presenza di deadlock
 *                nell'unfolding della rete di Petri
 *                (esempio: "cna -d unfolding.cuf");
 *             
 *     - dot:     utilizzato per produrre il corrispondete file "svg" della rete di Petri
 *                (esempio: "dot l.ll_net.dot -Tsvg -o l.svg", il file ".dot" viene prodotto
 *                dalla classe mapping.data.structure.PEP2DOT);
 *             
 *     - cuf2dot: utilizzato per conversione di un file "cuf" in un file "ll_net".
 * 
 * @author Marco
 */
public class FormatCommand 
{
    /**
     * Comando da eseguire
     */
    private String command;
    
    
    /**
     * Output del comando
     */
    private String output;
    
    
    /**
     * Eventuale errore del comando
     */
    private String error;
    
    
    /**
     * Valore di exit del programma
     */
    private int exitValue;
    
    
    
    /**
     * Costruttore
     * 
     * @param command Stringa del comando da eseguire
     */
    public FormatCommand (String command)
    {   
        this.command = command;
    }
    

    /**
     * Ottengo il valore dell'attributo command
     * 
     * @return Valore dell'attributo command
     */
    public String getCommand () 
    {
        return this.command;
    }

    
    /**
     * Modifica del valore dell'attributo command
     * 
     * @param command Nuovo valore dell'attributo command
     */
    public void setCommand (String command) 
    {
        this.command = command;
    }
    
    
    /**
     * Ottengo il valore dell'attributo output
     * 
     * @return Valore dell'attributo output
     */
    public String getOutput () 
    {
        return this.output;
    }

    
    /**
     * Modifica del valore dell'attributo output
     * 
     * @param output Nuovo valore dell'attributo output
     */
    public void setOutput (String output) 
    {
        this.output = output;
    }

    
    /**
     * Ottengo il valore dell'attributo error
     * 
     * @return Valore dell'attributo error
     */
    public String getError () 
    {
        return this.error;
    }

    
    /**
     * Modifica del valore dell'attributo error
     * 
     * @param error Nuovo valore dell'attributo error
     */
    public void setError (String error) 
    {
        this.error = error;
    }

    
    /**
     * Ottengo il valore dell'attributo exitValue
     * 
     * @return Valore dell'attributo exitValue
     */
    public int getExitValue () 
    {
        return this.exitValue;
    }

    
    /**
     * Modifica del valore dell'attributo exitValue
     * 
     * @param exitValue Nuovo valore dell'attributo exitValue
     */
    public void setExitValue (int exitValue) 
    {
        this.exitValue = exitValue;
    }


    /*
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "FormatCommand [command=" + this.command + ", output=" + this.output
                + ", error=" + this.error + ", this.exitValue=" + this.exitValue + "]";
    }
}

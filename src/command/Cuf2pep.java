/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package command;

import java.io.IOException;

import utils.GenericUtils;

/**
 * Classe che gestisce l'esecuzione dello script "cuf2pep".
 * 
 * @author Marco
 *
 */
public class Cuf2pep
{
    /**
     * Metodo che avvia lo script "cuf2pep"
     * 
     * @param pathCunfFile              Percorso del file ".cuf"
     * @param pathPEPfile               Percorso del file ".ll_net" (formato PEP)
     * @return                          Output del comando
     * @throws IOException
     * @throws InterruptedException
     */
    public static FormatCommand execute (String pathCunfFile, String pathPEPfile) throws IOException, InterruptedException
    {
        return GeneralCommand.execute (new String[] {
                "python",
                GenericUtils.getValueProperty ("resources/properties/settings/settings.properties", "path_cuf2pep"),
                pathCunfFile,
                pathPEPfile
        });
    }
}

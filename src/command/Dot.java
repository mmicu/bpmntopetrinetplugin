/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package command;

import java.io.IOException;

import utils.GenericUtils;

/**
 * Classe che gestisce l'esecuzione di "dot".
 * 
 * @author Marco
 *
 */
public class Dot
{
    /**
     * Metodo che avvia il software "dot"
     * 
     * @param petriNetDot           Percorso del file ".ll_net" (formato PEP)
     * @param pathSVG               Percorso del file ".svg"
     * @return                      Output del comando
     * @throws IOException
     * @throws InterruptedException
     */
    public static FormatCommand execute (String petriNetDot, String pathSVG) throws IOException, InterruptedException
    {
        return GeneralCommand.execute (new String[] {
                GenericUtils.getValueProperty ("resources/properties/settings/settings.properties", "path_dot"),
                petriNetDot,
                "-Tsvg",
                "-o",
                pathSVG
        });
    }
}

/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Classe utilizzata per l'esecuzione di un programma esterno.
 * Nel nostro caso, il comando rappresenta la chiamata di un programma esterno:
 * 
 *     - cunf:    utilizzato per produrre l'unfolding della rete di Petri
 *                (esempio: "cunf -o unfolding.cuf rete_di_Petri.ll_net");
 *             
 *     - cna:     utilizzato per controllare l'eventuale presenza di deadlock
 *                nell'unfolding della rete di Petri
 *                (esempio: "cna -d unfolding.cuf");
 *             
 *     - dot:     utilizzato per produrre il corrispondete file "svg" della rete di Petri
 *                (esempio: "dot l.ll_net.dot -Tsvg -o l.svg", il file ".dot" viene prodotto
 *                dalla classe mapping.data.structure.PEP2DOT);
 *             
 *     - cuf2dot: utilizzato per conversione di un file "cuf" in un file "ll_net".
 *     
 * @author Marco
 *
 */
public class GeneralCommand
{
    public StreamWrapper getStreamWrapper (InputStream is, String type)
    {
        return new StreamWrapper (is, type);
    }
    
    
    private class StreamWrapper extends Thread 
    {
        public InputStream is = null;
        public String type    = null;
        public String message = null;

        
        StreamWrapper (InputStream is, String type) 
        {
            this.is   = is;
            this.type = type;
        }

        
        public void run () 
        {
            try {
                BufferedReader br   = new BufferedReader (new InputStreamReader (is));
                StringBuffer buffer = new StringBuffer ();
                String line         = null;
                
                while ((line = br.readLine ()) != null)
                    buffer.append (line).append ("\n");
                
                message = buffer.toString ();
            } 
            catch (IOException ioe) {
                ioe.printStackTrace ();  
            }
        }
    }
    
    
    /**
     * Metodo utilizzato per richiamare un programma esterno
     * 
     * @param command               Comando da eseguire
     * @return                      Output del comando
     * @throws IOException
     * @throws InterruptedException
     */
    public static FormatCommand execute (String[] command) throws IOException, InterruptedException
    {
        StringBuilder commandToS = new StringBuilder ();
        for (String s : command) {
            commandToS.append (s);
        }
        
        // Format CommandLine command
        FormatCommand clf = new FormatCommand (commandToS.toString ());
        
        Runtime rt = Runtime.getRuntime ();
        GeneralCommand rte = new GeneralCommand ();
        StreamWrapper error, output;

        Process proc = rt.exec (command); // IOException
        error        = rte.getStreamWrapper (proc.getErrorStream (), "ERROR");
        output       = rte.getStreamWrapper (proc.getInputStream (), "OUTPUT");
        int exitVal  = 0;

        error.start ();
        output.start ();
        error.join (3000);          // InterruptedException
        output.join (3000);         // InterruptedException
        exitVal = proc.waitFor ();  // InterruptedException
        
        clf.setOutput (output.message);
        clf.setError (error.message);
        clf.setExitValue (exitVal);
        
        return clf;
    }
}

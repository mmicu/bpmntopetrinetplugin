/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;

import mapping.data.structure.PEP;
import mapping.data.structure.Pair;
import mapping.exceptions.ExceptionMapping;
import mapping.transformation.Transformation;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.xml.sax.SAXException;

import command.Cna;
import command.FormatCommand;
import command.Cunf;
import config.GlobalConfiguration;
import utils.FileUtils;
import utils.GenericUtils;

public class BPMNtoPetriNet 
{
	private static final String PATH_SETTINGS_FILE = "resources/properties/settings/settings.properties";
	
	
	@SuppressWarnings("static-access")
	private static Options setOptions (Locale lang)
	{
		Options options = new Options ();
		
		options.addOption (
			OptionBuilder.withArgName ("file")
				.hasArg ()
				.withDescription (GenericUtils._l (lang, "option_b"))
	            .create ("b")
		);
		
		options.addOption (
			OptionBuilder.withArgName ("file")
				.hasArg ()
				.withDescription (GenericUtils._l (lang, "option_p"))
		        .create ("p")
		);
		
		options.addOption (
			OptionBuilder.withArgName ("dir")
				.hasArg ()
				.withDescription (GenericUtils._l (lang, "option_d"))
			    .create ("d")
		);
		
		options.addOption (
			OptionBuilder.withArgName ("language")
				.hasArg ()
				.withDescription (GenericUtils._l (lang, "option_lang"))
			    .create ("lang")
		);
		
		options.addOption ("c", false, GenericUtils._l (lang, "option_c"));
		
		options.addOption ("svg", false, GenericUtils._l (lang, "option_svg"));
		
		options.addOption ("h", false, GenericUtils._l (lang, "option_h"));

		return options;
	}
	
	
	private static void analyzeProcess ()
	{
	    
	}
	
	
	public static void main (String[] args) 
	{
	    GlobalConfiguration.PLUGIN_ACTIVE = false;
		String language;
		Locale lang = null;
		
		try {
			language = GenericUtils.getLanguage ();
			
			if (language.equals ("Italiano"))
				lang = new Locale ("it");
			else// if (language.equals ("English"))
				lang = new Locale ("en");
		} 
		catch (IOException e) {
			System.out.println ("Unable to load the language file!");
			
			System.exit (-1);
		}
		
		try {
			if (!GenericUtils.correctSettings ()) {
				System.out.println (GenericUtils._l (lang, "incorrect_settings_analyze_bpmn") + "!");
				
				System.exit (-1);
			}
		} 
		catch (IOException e) {
			System.out.println (e.getClass ().getSimpleName () + ". " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
			
			System.exit (-1);
		}
		
		// Creazione del parser
		CommandLineParser parser = new BasicParser ();
		
		// Specifico le opzioni CLI
		Options options = BPMNtoPetriNet.setOptions (lang);
		
		/*
		  -b <file>
		 -h
		 -lang <language>
		 -p <file>
		 -svg
		 -t <dir>
		 */
		CommandLine line;
		try {
			line = parser.parse (options, args);
			
			if (line.hasOption ("lang")) {
				// Se l = null, viene generata l'eccezione ParseException. 
				// Quindi non c'e' bisogno di controllare il valore di questa variabile
		    	String l = line.getOptionValue ("lang");
		    	
		    	if (!lang.getCountry ().equals (l) && l.equals ("en") || l.equals ("it"))
		    		options = BPMNtoPetriNet.setOptions (new Locale (l));
		    }
			
			if ((!line.hasOption ("b") && !line.hasOption ("d")) || line.hasOption ("h")) {
		    	HelpFormatter formatter = new HelpFormatter ();
		    	formatter.printHelp ("BPMNtoPetriNet", options);
		    	
		        System.exit (0);
		    }
			
			
			
			
			/*
			 * 
			 * Option "d"
			 * 
			 */
			else if (line.hasOption ("d")) {
			    long start = System.currentTimeMillis ();
				ArrayList<File> files = FileUtils.getAllFileByDirName (line.getOptionValue ("d"), "bpmn");
				
				ArrayList<String> deadlockMessages = new ArrayList<String> ();
				
				if (files == null) {
			    	System.out.println ("La cartella \"" + line.getOptionValue ("d") + "\" non esiste!");
			    	
			    	System.exit (-1);
			    }
			    
				else if (files.size () == 0) {
			    	System.out.println ("La cartella \"" + line.getOptionValue ("d") + "\" non contiene nessun file con estensione \"bpmn\"!");
			    	
			    	System.exit (0);
			    }
				
				int size_f = files.size ();
				
				for (int k = 0; k < size_f; k++) {
					long local_start_mapping = System.currentTimeMillis ();
					
					String pathBPMN, pathPetriNet = null, pathPetriNetDOT = null, pathPetriNetSVG = null;
					FormatCommand commandCunf = null, commandCna = null;
					
					// Imposto path del BPMN
					pathBPMN = files.get (k).getPath ();
					
					// Imposto path del Rete di Petri
					String pathTempFile;
					
					try {
						pathTempFile = GenericUtils.getValueProperty (BPMNtoPetriNet.PATH_SETTINGS_FILE, "path_temp_file");
						pathPetriNet = pathTempFile + File.separator + FileUtils.getFileNameByPath (pathBPMN) + ".ll_net";
					} 
					catch (IOException e) {
						System.out.println (e.getClass ().getSimpleName () + ". " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
						
						System.exit (-1);
					}
					
					// Imposto path file dot e path della rete Petri nel formato SVG
					if (line.hasOption ("svg")) {
						pathPetriNetDOT = pathPetriNet + ".dot";
						pathPetriNetSVG = pathPetriNet + ".svg";
					}
					
					// Applico il mapping
					long local_start_verification = 0, time_verification = 0;

					try {
						GenericUtils.applyMapping (pathBPMN, pathPetriNet, pathPetriNetDOT, pathPetriNetSVG);
						
						long time_mapping = System.currentTimeMillis () - local_start_mapping;
						
						// Analisi della rete di Petri con Cunf e Cna
						if (line.hasOption ("c")) {
							try {
							    local_start_verification = System.currentTimeMillis ();
								String pathCunfFile = GenericUtils.getValueProperty (BPMNtoPetriNet.PATH_SETTINGS_FILE, "path_temp_file") + 
										              File.separator + FileUtils.getFileNameByPath (pathPetriNet) + ".cuf";
								
								commandCunf = Cunf.execute (pathPetriNet, pathCunfFile);
								
								commandCna = Cna.execute (pathCunfFile);
								
								time_verification = System.currentTimeMillis () - local_start_verification;
							} 
							catch (InterruptedException e) {
								System.out.println (e.getClass ().getSimpleName () + ". " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
								
								System.exit (-1);
							}
						}
						
						
						
						System.out.println ("======================================================================================");
						System.out.println ("Process:                           " + pathBPMN);
						System.out.println ("Petri net (ll_net):                " + pathPetriNet);
						
						if (line.hasOption ("svg"))
							System.out.println ("Petri net (svg):      " + pathPetriNetSVG);
						
						if (line.hasOption ("c")) {
						    System.out.println ("Cunf exit value:                   " + commandCunf.getExitValue ());
							System.out.println ("Cna exit value:                    " + commandCna.getExitValue ());
							
							String deadlockMessage = GenericUtils.getAnswerCNA (commandCna.getOutput ());
							String trace = null;
							
							if (deadlockMessage.length () > 0 && deadlockMessage.charAt (0) == 'N')
								deadlockMessage = GenericUtils._l (lang, "deadlock_free");
							else if (deadlockMessage.length () > 0 && deadlockMessage.charAt (0) == 'Y') {
								deadlockMessage = GenericUtils._l (lang, "no_deadlock_free");
								
								trace = GenericUtils.getTraceCNA (commandCna.getOutput ());
							}
							else
								deadlockMessage = "";
							
							deadlockMessages.add (deadlockMessage);
							
							if (deadlockMessage != null)
								System.out.println ("Deadlock?                          " + deadlockMessage + "!");
							
							if (trace != null)
								System.out.println ("Trace:                             " + trace);
						}
						
						System.out.println ("Time execution mapping (ms):       " + time_mapping);
						
						if (line.hasOption ("c")) {
						    System.out.println ("Time execution verification (ms):  " + time_verification);
						    System.out.println ("Total time execution (ms):         " + (time_mapping + time_verification));
						}
						
						System.out.println ("======================================================================================\n");
					} 
					catch (IOException | ExceptionMapping | ParserConfigurationException | SAXException | InterruptedException e) {
						System.out.println (e.getClass ().getSimpleName () + ". " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
						
						System.exit (-1);
					}
				}
				
				long time = System.currentTimeMillis () - start;
				if (line.hasOption ("c")) {
					System.out.println ("\n======================================================================================");
						System.out.println ("Resume deadlock:\n");
						
						for (int k = 0; k < size_f; k++) {
							System.out.println ("File:      " + files.get (k).getPath ());
							System.out.println ("Deadlock?  " + deadlockMessages.get (k) + (k == size_f - 1 ? "!" : "!\n"));
						}
						System.out.println ("======================================================================================");
					}
				
				System.out.println ("\n======================================================================================");
				System.out.println ("Total execution (ms):  " + time);
				System.out.println ("Total execution (s):   " + (double) time / 1000);
				System.out.println ("======================================================================================");
				
				
				
				System.exit (0);
			}
			
			
			/*
			 * 
			 * Option "b"
			 * 
			 */
			else if (line.hasOption ("b")) {
				if (!FileUtils.fileExists (line.getOptionValue ("b"))) {
					System.out.println (GenericUtils._l (lang, "bpmn_not_exist") + " (" + line.getOptionValue ("b") + ")!");
					
					System.exit (-1);
				}
				
				String pathBPMN, pathPetriNet = null, pathPetriNetDOT = null, pathPetriNetSVG = null;
				FormatCommand commandCunf = null, commandCna = null;
				
				// Imposto path del BPMN
				pathBPMN = line.getOptionValue ("b");
				
				// Imposto path del Rete di Petri
				if (line.hasOption ("p"))
					pathPetriNet = line.getOptionValue ("p");
				else {
					String pathTempFile;
					
					try {
						pathTempFile = GenericUtils.getValueProperty (BPMNtoPetriNet.PATH_SETTINGS_FILE, "path_temp_file");
						pathPetriNet = pathTempFile + File.separator + FileUtils.getFileNameByPath (line.getOptionValue ("b")) + ".ll_net";
					} 
					catch (IOException e) {
						System.out.println (e.getClass ().getSimpleName () + ". " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
						
						System.exit (-1);
					}
				}
				
				// Imposto path file dot e path della rete Petri nel formato SVG
				if (line.hasOption ("svg")) {
					pathPetriNetDOT = pathPetriNet + ".dot";
					pathPetriNetSVG = pathPetriNet + ".svg";
				}
				
				long start_verification, time_verification = 0;
				
				// Applico il mapping
				try {
				    long start = System.currentTimeMillis ();
				    
					ArrayList<Integer> res = GenericUtils.applyMapping (pathBPMN, pathPetriNet, pathPetriNetDOT, pathPetriNetSVG);
					
					long time = System.currentTimeMillis () - start;
					
					// Analisi della rete di Petri con Cunf e Cna
					if (line.hasOption ("c")) {
						try {
						    start_verification = System.currentTimeMillis ();
		                    
							String pathCunfFile = GenericUtils.getValueProperty (BPMNtoPetriNet.PATH_SETTINGS_FILE, "path_temp_file") + 
									              File.separator + FileUtils.getFileNameByPath (pathPetriNet) + ".cuf";
							
							commandCunf = Cunf.execute (pathPetriNet, pathCunfFile);
							
							commandCna = Cna.execute (pathCunfFile);
							
							time_verification = System.currentTimeMillis () - start_verification;
						} 
						catch (InterruptedException e) {
							System.out.println (e.getClass ().getSimpleName () + ". " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
							
							System.exit (-1);
						}
					}
					
					
					
					System.out.println ("-======================================================================================");
					System.out.println ("Process:                 " + pathBPMN);
					
					if (line.hasOption ("p"))
						System.out.println ("Petri net (ll_net):      " + pathPetriNet);
					
					if (line.hasOption ("svg"))
						System.out.println ("Petri net (svg):         " + pathPetriNetSVG);
					
					if (line.hasOption ("c")) {
					    System.out.println ("Cunf exit value:             " + commandCunf.getExitValue ());
						System.out.println ("Cna2 exit value:              " + commandCna.getExitValue ());
						System.out.println ("Cunf exit value:             " + commandCunf.getError ());
                        System.out.println ("Cna2 exit value:              " + commandCna.getError ());
						
						String deadlockMessage = GenericUtils.getAnswerCNA (commandCna.getOutput ());
						String trace = null;
						
						if (deadlockMessage.length () > 0 && deadlockMessage.charAt (0) == 'N')
							deadlockMessage = GenericUtils._l (lang, "deadlock_free");
						else if (deadlockMessage.length () > 0 && deadlockMessage.charAt (0) == 'Y') {
							deadlockMessage = GenericUtils._l (lang, "no_deadlock_free");
							
							trace = GenericUtils.getTraceCNA (commandCna.getOutput ());
						}
						else
							deadlockMessage = "";
						
						if (deadlockMessage != null)
							System.out.println ("Deadlock?                    " + deadlockMessage + "!");
						
						if (trace != null)
							System.out.println ("Trace:                       " + trace);
					}
					
					if (res != null && res.size () == 3) {
    					System.out.println ("Places mapping:                  " + res.get (0));
    					System.out.println ("Transitions mapping:             " + res.get (1));
					}
					
					
					
					System.out.println ("Time execution (ms):     " + time);
					
					if (line.hasOption ("c"))
					    System.out.println ("Time verification (ms):  " + time_verification);
					
					System.out.println ("-======================================================================================");
				} 
				catch (IOException | ExceptionMapping | ParserConfigurationException | SAXException | InterruptedException e) {
					System.out.println (e.getClass ().getSimpleName () + ". " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
					
					System.exit (-1);
				}
			}
		} 
		catch (ParseException e) {
			System.out.println ("ParseException. Message: " + e.getLocalizedMessage ());
			
			System.exit (-1);
		}
	    
		
		System.exit (0);
	}
}

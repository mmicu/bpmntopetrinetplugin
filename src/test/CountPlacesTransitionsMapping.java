package test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;

import mapping.data.structure.PEP;
import mapping.exceptions.ExceptionMapping;
import mapping.transformation.Transformation;

import org.xml.sax.SAXException;

import config.GlobalConfiguration;
import utils.FileUtils;
import utils.GenericUtils;


public class CountPlacesTransitionsMapping
{
    private static void usage ()
    {
        System.out.println ("Usage: java -cp \"bin:lib/*\" test/CountPlacesTransitionsMapping [ -d <bpmn_dir>] [-f <bpmn_file> ]");
    }
    
    
    private static void countElementsInDir (String dirPATH, Locale lang)
    {
        ArrayList<File> files = FileUtils.getAllFileByDirName (dirPATH, "bpmn");
        
        if (files == null) {
            System.out.println ("La cartella \"" + dirPATH + "\" non esiste!");
            
            System.exit (-1);
        }
        
        else if (files.size () == 0) {
            System.out.println ("La cartella \"" + dirPATH + "\" non contiene nessun file con estensione \"bpmn\"!");
            
            System.exit (0);
        }

        for (int k = 0, size_f = files.size (); k < size_f; k++)
            CountPlacesTransitionsMapping.count (files.get (k).getPath (), lang);
    }
    
    
    private static void countElementsInFile (String filePATH, Locale lang)
    {
        if (!FileUtils.fileExists (filePATH)) {
            System.out.println ("Il file \"" + filePATH + "\" non esiste!");
            
            System.exit (-1);
        }
        
        CountPlacesTransitionsMapping.count (filePATH, lang);
    }
    
    
    private static void count (String pathBPMN, Locale lang)
    {
        Transformation t = new Transformation (pathBPMN);
        PEP petriNet;
        
        System.out.println ("=========================================================");
        System.out.println ("Process                        = " + pathBPMN);
        
        try {
            petriNet = t.applyTransformation ();
            
            
            System.out.println ("Unfolding places               = " + petriNet.getOnlyPlace ().size ());
            System.out.println ("Unfolding transitions          = " + petriNet.getT ().size ());
        } 
        catch (IOException | ExceptionMapping | ParserConfigurationException | SAXException e) {
            System.out.println (e.getClass ().getSimpleName () + ". " + GenericUtils._l (lang, "message") + ": " + e.getMessage ()); 
        }
        
        System.out.println ("=========================================================\n");
    }
    
    public static void main (String[] args)
    {
        GlobalConfiguration.PLUGIN_ACTIVE = false;
        String language;
        Locale lang = null;
        
        try {
            language = GenericUtils.getLanguage ();
            
            if (language.equals ("Italiano"))
                lang = new Locale ("it");
            else// if (language.equals ("English"))
                lang = new Locale ("en");
        } 
        catch (IOException e) {
            System.out.println ("Unable to load the language file!");
            
            System.exit (-1);
        }
        
        if (args.length == 2 && args[0].equals ("-d"))
                CountPlacesTransitionsMapping.countElementsInDir (args[1], lang);
        
        else if (args.length == 2 && args[0].equals ("-f"))
                CountPlacesTransitionsMapping.countElementsInFile (args[1], lang);
        
        else
            CountPlacesTransitionsMapping.usage ();
        
        System.exit (0);
    }
}

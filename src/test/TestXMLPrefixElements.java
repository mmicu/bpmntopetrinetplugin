/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package test;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import config.GlobalConfiguration;
import utils.StringUtils;
import mapping.data.structure.BPMNElement;
import mapping.data.structure.MappingRule;
import mapping.data.structure.SubElements;
import mapping.parser.xml.XMLMappedElements;
import mapping.parser.xml.XMLMappingRules;
import mapping.parser.xml.XMLPrefixNameElements;
import mapping.parser.xml.XMLReadBPMNElements;
import mapping.parser.xml.XMLSubElements;
import mapping.transformation.Transformation;

public class TestXMLPrefixElements 
{
	//
	public static final String PROCESS_PATH = "BPMNModels/test/processes/process_3.bpmn";
	
	//
	public static final String FILE_RULES = "resources/rules.xml";
	
	public static void main (String[] args) 
	{
	    GlobalConfiguration.PLUGIN_ACTIVE = false;
		
		ArrayList<String> mappedElements;
		
		ArrayList<MappingRule> mappingRules;
		
		try {
			mappedElements = new XMLMappedElements (TestXMLPrefixElements.FILE_RULES, false).parse ();
			
			mappingRules = new XMLMappingRules (TestXMLPrefixElements.FILE_RULES).parse ();
			
			// Elementi BPMN
			ArrayList<BPMNElement> b = new XMLReadBPMNElements (TestXMLPrefixElements.PROCESS_PATH, false).parse (mappedElements);
			
			// SubElements
			ArrayList<SubElements> subElements = new XMLSubElements (TestXMLPrefixElements.PROCESS_PATH).parse (mappedElements);
			
			// Prefix
			ArrayList<String> p = new XMLPrefixNameElements (TestXMLPrefixElements.PROCESS_PATH).parse (b, mappedElements, mappingRules);
		
			// Rename
			TestXMLPrefixElements.renameForSubProcess (b, p, subElements);
		} 
		catch (ParserConfigurationException | SAXException | IOException e) {
			System.out.println (e.getClass ().getName () + ", message: " + e.getMessage ());
		}

		System.exit (0);
	}
	
	
	public static void renameForSubProcess (ArrayList<BPMNElement> b, ArrayList<String> p, ArrayList<SubElements> subElements)
	{
		// In caso di errori nel parser per determinare i nuovi nomi, non rinomino nessun elemento BPMN
		if (b.size () != p.size ()) {
			System.out.println ("b.size () != p.size ()");
			
			System.exit (-1);
		}
		
		// this.b.size () = renamedElements.size ()
		for (int k = 0; k < b.size (); k++)
			System.out.println ("Not Subprocess: " + b.get (k).getId () + " --> " + p.get (k) + (b.get (k).getId ().equals (p.get (k)) ? " (Changed)" : " (Not Changed)"));
		
		System.out.println ();
		
		for (int k = 0; k < subElements.size (); k++) {
			ArrayList<String> bpmnElementsId = subElements.get (k).getBpmnElementsId ();
			for (int j = 0; j < bpmnElementsId.size (); j++) {
				for (int i = 0; i < b.size (); i++) {
					if (bpmnElementsId.get (j).equals (b.get (i).getId ()))
						System.out.println ("SubElement (" + subElements.get (k).getName () + ") : " + b.get (i).getId () + " --> " + StringUtils.__trim (subElements.get (k).getName ()) + "_" + b.get (i).getId ());
				}
			}
		}
	}
}

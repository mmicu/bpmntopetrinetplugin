/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package test;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import config.GlobalConfiguration;
import utils.FileUtils;
import utils.GenericUtils;
import mapping.exceptions.ExceptionMapping;

public class TestTransformation 
{
	//
	public static final String PROCESS_PATH = "BPMNModels/xml/MD/process_4.bpmn";
	
	//
	public static final String RULES_XML_FILE = "resources/rules.xml";
	
	//
	public static final String PATH_TEST_FILES = "/Users/marco/Desktop/test";
	
	//
	private static final String PATH_SETTINGS_FILE = "resources/properties/settings/settings.properties";
	
	
	public static void main (String[] args) 
	{
	    GlobalConfiguration.PLUGIN_ACTIVE = false;
		
		long start = System.currentTimeMillis ();
		
		String language;
		Locale lang = null;
		
		try {
			language = GenericUtils.getLanguage ();
			
			if (language.equals ("Italiano"))
				lang = new Locale ("it");
			else// if (language.equals ("English"))
				lang = new Locale ("en");
		} 
		catch (IOException e) {
			System.out.println ("Unable to load the language file!");
			
			System.exit (-1);
		}
		
		String pathPetriNet = null, pathPetriNetDOT = null, pathPetriNetSVG = null;
		
		String pathTempFile;
		
		try {
			pathTempFile = GenericUtils.getValueProperty (TestTransformation.PATH_SETTINGS_FILE, "path_temp_file");
			pathPetriNet = pathTempFile + File.separator + FileUtils.getFileNameByPath (TestTransformation.PROCESS_PATH) + ".ll_net";
			pathPetriNetDOT = pathPetriNet + ".dot";
			pathPetriNetSVG = pathPetriNet + ".svg";
		} 
		catch (IOException e) {
			System.out.println (e.getClass ().getSimpleName () + ". " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
			
			System.exit (-1);
		}
		
		
		
		try {
			GenericUtils.applyMapping (
				TestTransformation.PROCESS_PATH, 
				pathPetriNet, 
				pathPetriNetDOT, 
				pathPetriNetSVG
			);
			
			long time = System.currentTimeMillis () - start;
			System.out.println ("======================================================================================");
			System.out.println ("Process:              " + TestTransformation.PROCESS_PATH);
			System.out.println ("Petri net (ll_net):   " + pathPetriNet);
			System.out.println ("Petri net (svg):      " + pathPetriNetSVG);
			System.out.println ("Time execution (ms):  " + time);
			System.out.println ("Time execution (s):   " + (double) time / 1000);
			System.out.println ("======================================================================================");
		} 
		catch (IOException | ExceptionMapping | ParserConfigurationException | SAXException | InterruptedException e) {
			System.out.println (e.getClass ().getSimpleName () + ". " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
			
			System.exit (-1);
		}
		
		System.exit (0);
	}
}

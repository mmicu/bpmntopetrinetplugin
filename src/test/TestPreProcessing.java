/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package test;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import config.GlobalConfiguration;
import mapping.parser.xml.XMLPreProcessingNotManagedElements;

public class TestPreProcessing 
{
	//
	public static final String PROCESS_PATH = "BPMNModels/1/collaboration_1.bpmn";
	
	public static void main(String[] args) 
	{
	    GlobalConfiguration.PLUGIN_ACTIVE = false;
		
		ArrayList<String> notManagedElements;
		
		try {
			notManagedElements = new XMLPreProcessingNotManagedElements (TestPreProcessing.PROCESS_PATH).parse ();
			
			if (notManagedElements.size () == 0) 
				System.out.println ("In the process:  \"" + TestPreProcessing.PROCESS_PATH + "\", all elements are managed!");
			
			for (int k = 0; k < notManagedElements.size (); k++) {
				if (k == 0)
					System.out.println ("Process:  \"" + TestPreProcessing.PROCESS_PATH + "\"");
				System.out.println ("This element is not managed:  " + notManagedElements.get (k));
			}
		} 
		catch (ParserConfigurationException | SAXException | IOException e) {
			System.out.println (e.getClass ().getName () + ", message: " + e.getMessage ());
		}
		
		System.exit (0);
	}
}

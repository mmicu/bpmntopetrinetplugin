package test.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author marco
 *
 */
@RunWith (Suite.class)
@Suite.SuiteClasses (
{
    MainTest.class
})


public class TestSuite
{
    // Class remains empty, used only as a holder for the above annotations
}

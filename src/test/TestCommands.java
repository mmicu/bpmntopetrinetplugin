/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package test;

import java.io.IOException;

import command.Cuf2pep;
import command.FormatCommand;
import config.GlobalConfiguration;

public class TestCommands 
{
	public static void main (String[] args)
	{
	    GlobalConfiguration.PLUGIN_ACTIVE = false;
		/*
		// Test dot
		try {
			Command c = Dot.execute (
					"/Users/marco/Desktop/cia a lol/Untitled.ll_net.dot", 
					"/Users/marco/Desktop/cia a lol/Untitled.ll_net.svg"
			);
			
			System.out.println ("Command (dot):             " + c.getCommand ());
			System.out.println ("Command exit value (dot):  " + c.getExitValue ());
			System.out.println ("Command error (dot):       " + c.getError ());
			System.out.println ("Command output (dot):      " + c.getOutput ());
		} 
		catch (IOException | InterruptedException e) {
			e.printStackTrace ();
			
			System.exit (-1);
		}
		
		
		// Test Cunf
		try {
			Command c = Cunf.execute (
					"/Users/marco/Desktop/cia a lol/Untitled.ll_net", 
					"/Users/marco/Desktop/cia a lol/Untitled.cuf"
			);
			
			System.out.println ("\nCommand (cunf):           " + c.getCommand ());
			System.out.println ("Command exit value (cunf):  " + c.getExitValue ());
			System.out.println ("Command error (cunf):       " + c.getError ());
			System.out.println ("Command output (cunf):      " + c.getOutput ());
		} 
		catch (IOException | InterruptedException e) {
			e.printStackTrace ();
			
			System.exit (-1);
		}
		
		
		// Test Cna
		try {
			Command c = Cna.execute ("/Users/marco/Desktop/cia a lol/process_2.bpmn.ll_net.cuf");
			
			System.out.println ("\nCommand (cna):           " + c.getCommand ());
			System.out.println ("Command exit value (cna):  " + c.getExitValue ());
			System.out.println ("Command error (cna):       " + c.getError ());
			System.out.println ("Command output (cna):      " + c.getOutput ());
			
			System.out.print ("\n\n");
			System.out.println ("Answer Cna (length = " + GenericUtils.getAnswerCNA (c.getOutput ()).length () + "): " + GenericUtils.getAnswerCNA (c.getOutput ()));
			System.out.println ("Trace Cna: (length = " + GenericUtils.getTraceCNA (c.getOutput ()).length () + "): " +  GenericUtils.getTraceCNA (c.getOutput ()));
		} 
		catch (IOException | InterruptedException e) {
			e.printStackTrace ();
			
			System.exit (-1);
		}*/
		
		
		// Test cuf2dot.py
		System.out.println ("A");
		try {
		    System.out.println ("B");
		    System.out.println ("B");
            FormatCommand c = Cuf2pep.execute ("/Users/marco/Desktop/process_10.bpmn.ll_net.cuf", "/Users/marco/Desktop/sasa.ll_net");
            System.out.println ("C");
            
            System.out.println ("\nCommand (cuf2dot):           " + c.getCommand ());
            System.out.println ("Command exit value (cuf2dot):  " + c.getExitValue ());
            System.out.println ("Command error (cuf2dot):       " + c.getError ());
            System.out.println ("Command output (cuf2dot):      " + c.getOutput ());
        } 
        catch (IOException | InterruptedException e) {
            e.printStackTrace ();
            
            System.exit (-1);
        }
		
		
		System.exit (0);
	}
}

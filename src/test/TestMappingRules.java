/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package test;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import config.GlobalConfiguration;
import mapping.data.structure.MappingRule;
import mapping.parser.xml.XMLMappingRules;

public class TestMappingRules
{
	//
	public static final String FILE_RULES = "resources/rules.xml";
	
	public static void main (String[] args) 
	{
	    GlobalConfiguration.PLUGIN_ACTIVE = false;
		
		ArrayList<MappingRule> mappingRules;
		
		try {
			mappingRules = new XMLMappingRules (TestMappingRules.FILE_RULES).parse ();
			
			System.out.println ("|mappingRules| = " + mappingRules.size () + "\n");
			for (int k = 0; k < mappingRules.size (); k++)
				System.out.println ("Rule (" + (k + 1) + "): " + mappingRules.get (k).toString ());
		} 
		catch (ParserConfigurationException | SAXException | IOException e) {
			System.out.println (e.getClass ().getName () + ", message: " + e.getMessage ());
		}
		
		System.exit (0);
	}
}

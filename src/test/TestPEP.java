/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package test;

import mapping.data.structure.PEP;

public class TestPEP 
{
	public static void main (String[] args) 
	{
		// Prima rete di Petri
		PEP p1 = new PEP ();
		
		p1.addP ("p_1", true);
		p1.addP ("p_2");
		p1.addP ("p_3");
		
		p1.addT ("t_1");
		p1.addT ("t_2");
		
		p1.addPT ("p_1", "t_1");
		p1.addPT ("p_2", "t_2");
		
		p1.addTP ("t_1", "p_2");
		p1.addTP ("t_2", "p_3");
		
		System.out.println ("PetriNet p1:\n");
		System.out.println (p1.getStandardNotationWithRealNames ());
		
		
		// Seconda rete di Petri
		PEP p2 = new PEP ();
		
		p2.addP ("p_4", true);
		p2.addP ("p_5");
		p2.addP ("p_6");
		
		p2.addT ("t_3");
		p2.addT ("t_4");
		
		p2.addPT ("p_4", "t_3");
		p2.addPT ("p_5", "t_4");
		
		p2.addTP ("t_3", "p_5");
		p2.addTP ("t_4", "p_6");
		
		System.out.println ("PetriNet p2:\n");
		System.out.println (p2.getStandardNotationWithRealNames ());
		
		
		// Merge p1 & p2
		System.out.println ("Merge PetriNets p1 & p2\n");
		p1.mergeWithAnotherPetriNet (p2);
		System.out.println (p1.getStandardNotationWithRealNames ());
		
		System.exit (0);
	}
}

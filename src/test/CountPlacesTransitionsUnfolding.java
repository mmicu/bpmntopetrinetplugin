package test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;

import mapping.data.structure.PEP;
import mapping.exceptions.ExceptionMapping;

import org.xml.sax.SAXException;

import command.Cuf2pep;
import command.Cunf;
import command.FormatCommand;
import config.GlobalConfiguration;
import utils.FileUtils;
import utils.GenericUtils;


public class CountPlacesTransitionsUnfolding
{
    private static final String PATH_SETTINGS_FILE = "resources/properties/settings/settings.properties";
    
    
    
    private static void usage ()
    {
        System.out.println ("Usage: java -cp \"bin:lib/*\" test/CountPlacesTransitionsUnfolding [ -d <bpmn_dir>] [-f <bpmn_file> ]");
    }
    
    
    private static void countElementsInDir (String dirPATH, Locale lang)
    {
        ArrayList<File> files = FileUtils.getAllFileByDirName (dirPATH, "bpmn");
        
        if (files == null) {
            System.out.println ("La cartella \"" + dirPATH + "\" non esiste!");
            
            System.exit (-1);
        }
        
        else if (files.size () == 0) {
            System.out.println ("La cartella \"" + dirPATH + "\" non contiene nessun file con estensione \"bpmn\"!");
            
            System.exit (0);
        }

        for (int k = 0, size_f = files.size (); k < size_f; k++)
            CountPlacesTransitionsUnfolding.count (files.get (k).getPath (), lang);
    }
    
    
    private static void countElementsInFile (String filePATH, Locale lang)
    {
        if (!FileUtils.fileExists (filePATH)) {
            System.out.println ("Il file \"" + filePATH + "\" non esiste!");
            
            System.exit (-1);
        }
        
        CountPlacesTransitionsUnfolding.count (filePATH, lang);
    }
    
    
    private static void count (String pathBPMN, Locale lang)
    {
        String pathPetriNet = null, pathPetriNetDOT = null, pathPetriNetSVG = null;
        FormatCommand commandCunf = null;
        
        // Imposto path del Rete di Petri
        String pathTempFile;
        
        try {
            pathTempFile = GenericUtils.getValueProperty (CountPlacesTransitionsUnfolding.PATH_SETTINGS_FILE, "path_temp_file");
            pathPetriNet = pathTempFile + File.separator + FileUtils.getFileNameByPath (pathBPMN) + ".ll_net";
        } 
        catch (IOException e) {
            System.out.println (e.getClass ().getSimpleName () + ". " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
            
            System.exit (-1);
        }
        
        // Applico il mapping
        try {
            GenericUtils.applyMapping (pathBPMN, pathPetriNet, pathPetriNetDOT, pathPetriNetSVG);
            
                try {
                    String pathCunfFile = GenericUtils.getValueProperty (CountPlacesTransitionsUnfolding.PATH_SETTINGS_FILE, "path_temp_file") + 
                                          File.separator + FileUtils.getFileNameByPath (pathPetriNet) + ".cuf";
                    
                    commandCunf = Cunf.execute (pathPetriNet, pathCunfFile);
                    
                    String pathUnfolding = pathPetriNet + "_unfolding.ll_net";
                    
                    FormatCommand commandCuf2Pep = Cuf2pep.execute (pathCunfFile, pathUnfolding);

                    PEP petriNetUnfolding  = GenericUtils.getPEPformatFromPEPfile (pathUnfolding);
                    
                    System.out.println ("=========================================================");
                    System.out.println ("Process                        = " + pathBPMN);
                    //System.out.println ("commandCuf2Pep.getExitValue () = " + commandCuf2Pep.getExitValue ());
                    System.out.println ("Unfolding places               = " + petriNetUnfolding.getOnlyPlace ().size ());
                    System.out.println ("Unfolding transitions          = " + petriNetUnfolding.getT ().size ());
                    System.out.println ("=========================================================\n");
                } 
                catch (InterruptedException e) {
                    System.out.println (e.getClass ().getSimpleName () + ". " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
                    
                    System.exit (-1);
                }
        }
        catch (IOException | ExceptionMapping | ParserConfigurationException | SAXException | InterruptedException e) {
            System.out.println (e.getClass ().getSimpleName () + ". " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
            
            System.exit (-1);
        }
    }
    
    public static void main (String[] args)
    {
        GlobalConfiguration.PLUGIN_ACTIVE = false;
        String language;
        Locale lang = null;
        
        try {
            language = GenericUtils.getLanguage ();
            
            if (language.equals ("Italiano"))
                lang = new Locale ("it");
            else// if (language.equals ("English"))
                lang = new Locale ("en");
        } 
        catch (IOException e) {
            System.out.println ("Unable to load the language file!");
            
            System.exit (-1);
        }
        
        if (args.length == 2 && args[0].equals ("-d"))
                CountPlacesTransitionsUnfolding.countElementsInDir (args[1], lang);
        
        else if (args.length == 2 && args[0].equals ("-f"))
                CountPlacesTransitionsUnfolding.countElementsInFile (args[1], lang);
        
        else
            CountPlacesTransitionsUnfolding.usage ();
        
        System.exit (0);
    }
}

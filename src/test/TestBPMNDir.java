/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import command.FormatCommand;
import command.Cna;
import command.Cunf;
import config.GlobalConfiguration;
import mapping.exceptions.ExceptionMapping;
import utils.FileUtils;
import utils.GenericUtils;

public class TestBPMNDir 
{
	//
	private static final String PATH_SETTINGS_FILE = "resources/properties/settings/settings.properties";
	
	//
	private static String DIR_BPMN_PROCESSES = "/Users/marco/Documents/workspace/BPMNtoPetriNetPlugin/BPMNModels/processes";
	
	//
	private static String EXTENSION_FILE_BPMN = "bpmn";
	
	
	public static void main (String[] args) 
	{
	    GlobalConfiguration.PLUGIN_ACTIVE = false;
		
		String language;
		Locale lang = null;
		
		try {
			language = GenericUtils.getLanguage ();
			
			if (language.equals ("Italiano"))
				lang = new Locale ("it");
			else// if (language.equals ("English"))
				lang = new Locale ("en");
		} 
		catch (IOException e) {
			System.out.println ("Unable to load the language file!");
			
			System.exit (-1);
		}
		
		try {
			if (!GenericUtils.correctSettings ()) {
				System.out.println (GenericUtils._l (lang, "incorrect_settings_analyze_bpmn") + "!");
				
				System.exit (-1);
			}
		} 
		catch (IOException e) {
			System.out.println (e.getClass ().getSimpleName () + ". " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
			
			System.exit (-1);
		}
		
		ArrayList<File> files = FileUtils.getAllFileByDirName (TestBPMNDir.DIR_BPMN_PROCESSES, "bpmn");
		
		if (files == null) {
	    	System.out.println ("La cartella \"" + TestBPMNDir.DIR_BPMN_PROCESSES + "\" non esiste!");
	    	
	    	System.exit (-1);
	    }
	    
		else if (files.size () == 0) {
	    	System.out.println ("La cartella \"" + TestBPMNDir.DIR_BPMN_PROCESSES + "\" non contiene nessun file con estensione \"" + EXTENSION_FILE_BPMN + "\"!");
	    	
	    	System.exit (0);
	    }
		
		System.out.println ("DIR_BPMN_PROCESSES:  \"" + TestBPMNDir.DIR_BPMN_PROCESSES + "\"");
		System.out.println ("Numero di file:       " + files.size ());
		
		long start_total = System.currentTimeMillis ();
		int size_f = files.size ();
		
		for (int k = 0; k < size_f; k++) {
			long local_start = System.currentTimeMillis ();
			String pathBPMN, pathPetriNet = null, pathPetriNetDOT = null, pathPetriNetSVG = null;
			FormatCommand commandCunf = null, commandCna = null;
			
			// Imposto path del BPMN
			pathBPMN = files.get (k).getPath ();
			
			System.out.println ("======================================================================================");
			System.out.println ("Process:              " + pathBPMN);
			
			// Imposto path del Rete di Petri
			String pathTempFile;
			
			try {
				pathTempFile = GenericUtils.getValueProperty (TestBPMNDir.PATH_SETTINGS_FILE, "path_temp_file");
				pathPetriNet = pathTempFile + File.separator + FileUtils.getFileNameByPath (pathBPMN) + ".ll_net";
			} 
			catch (IOException e) {
				System.out.println (e.getClass ().getSimpleName () + ". " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
				
				System.exit (-1);
			}
			
			// Imposto path file dot e path della rete Petri nel formato SVG
			pathPetriNetDOT = pathPetriNet + ".dot";
			pathPetriNetSVG = pathPetriNet + ".svg";
			
			// Applico il mapping
			try {
				GenericUtils.applyMapping (pathBPMN, pathPetriNet, pathPetriNetDOT, pathPetriNetSVG);
				
				// Analisi della rete di Petri con Cunf e Cna
				try {
					String pathCunfFile = GenericUtils.getValueProperty (TestBPMNDir.PATH_SETTINGS_FILE, "path_temp_file") + 
							              File.separator + FileUtils.getFileNameByPath (pathPetriNet) + ".cuf";
					
					commandCunf = Cunf.execute (pathPetriNet, pathCunfFile);
					
					commandCna = Cna.execute (pathCunfFile);
				} 
				catch (InterruptedException e) {
					System.out.println (e.getClass ().getSimpleName () + ". " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
					
					System.exit (-1);
				}
				
				
				long time = System.currentTimeMillis () - local_start;
				System.out.println ("Petri net (ll_net):   " + pathPetriNet);
				System.out.println ("Petri net (svg):      " + pathPetriNetSVG);
				System.out.println ("Cunf exit value:          " + commandCunf.getExitValue ());
				System.out.println ("Cna exit value:           " + commandCna.getExitValue ());
				System.out.println ("Time execution (ms):  " + time);
				System.out.println ("Time execution (s):   " + (double) time / 1000);
				System.out.println ("======================================================================================\n");
			} 
			catch (IOException | ExceptionMapping | ParserConfigurationException | SAXException | InterruptedException e) {
				System.out.println (e.getClass ().getSimpleName () + ". " + GenericUtils._l (lang, "message") + ": " + e.getMessage ());
				
				System.exit (-1);
			}
		}
		
		long time_total = System.currentTimeMillis () - start_total;
		
		System.out.println ("\n\n==============================================================================");
		System.out.println ("Total time execution (ms):  " + time_total);
		System.out.println ("Total time execution (s):   " + (double) time_total / 1000 + "\n==============================================================================");
		
		System.exit (0);
	}
}

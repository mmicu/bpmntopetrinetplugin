/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package test;

import java.util.ArrayList;

public class TestDeleteArrayList 
{
	public static void main (String[] args) 
	{
		ArrayList<String> a  = new ArrayList<String> ();
		ArrayList<Integer> b = new ArrayList<Integer> ();
		
		
		a.add ("a");a.add ("b");a.add ("c");a.add ("d");a.add ("e");a.add ("f");
		b.add (0);b.add (1);b.add (5);
		
		System.out.println ("Before deleting:");
		for (int k = 0; k < a.size (); k++)
			System.out.println (a.get (k));
		
		// Delete elements
		for (int k = b.size () - 1; k >= 0; k--)
		    a.remove (b.get (k).intValue ());
		
		System.out.println ("\n\nAfter deleting:");
		for (int k = 0; k < a.size (); k++)
			System.out.println (a.get (k));
		
		System.exit (0);
	}
}

/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package test;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import config.GlobalConfiguration;
import mapping.data.structure.SubElements;
import mapping.parser.xml.XMLMappedElements;
import mapping.parser.xml.XMLSubElements;

public class TestXMLSubElements
{
	//
	public static final String PROCESS_PATH = "BPMNModels/a) Process/process_11.bpmn";
	
	//
	public static final String FILE_RULES = "resources/rules.xml";
	
	public static void main (String[] args) 
	{
	    GlobalConfiguration.PLUGIN_ACTIVE = false;
		
		ArrayList<String> mappedElements;
		
		try {
			mappedElements = new XMLMappedElements (TestXMLSubElements.FILE_RULES, false).parse ();
			
			ArrayList<SubElements> s = new XMLSubElements (TestXMLSubElements.PROCESS_PATH).parse (mappedElements);
			
			System.out.println ("|SubElements| = " + s.size ());
			for (int k = 0; k < s.size (); k++)
				System.out.println (s.get (k).toString ());
		} 
		catch (ParserConfigurationException | SAXException | IOException e) {
			System.out.println (e.getClass ().getName () + ", message: " + e.getMessage ());
		}
		
		System.exit (0);
	}
}

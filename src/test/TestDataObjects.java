/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package test;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import config.GlobalConfiguration;
import mapping.data.structure.SubElements;
import mapping.parser.xml.XMLDataObjects;
import mapping.parser.xml.XMLMappedElements;
import mapping.parser.xml.XMLReadBPMNElements;
import mapping.parser.xml.XMLSubElements;

public class TestDataObjects 
{
	//
	public static final String PROCESS_PATH = "BPMNModels/1/data_object_1.bpmn";
	
	//
	public static final String FILE_RULES = "resources/rules.xml";
	
	public static void main (String[] args) 
	{
	    GlobalConfiguration.PLUGIN_ACTIVE = false;
		
		ArrayList<String> mappedElements;
		
		try {
			mappedElements = new XMLMappedElements (TestDataObjects.FILE_RULES).parse ();
			
			ArrayList<SubElements> s = new XMLSubElements (TestDataObjects.PROCESS_PATH).parse (mappedElements);
			
			new XMLDataObjects (TestDataObjects.PROCESS_PATH).parse (
				new XMLReadBPMNElements (TestDataObjects.PROCESS_PATH).parse (mappedElements),
				s,
				mappedElements
			);
		} 
		catch (ParserConfigurationException | SAXException | IOException e) {
			System.out.println (e.getClass ().getName () + ", message: " + e.getMessage ());
		}

		System.exit (0);
	}
}

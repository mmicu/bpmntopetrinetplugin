package test;

import java.io.IOException;
import java.util.ArrayList;

import config.GlobalConfiguration;
import utils.GenericUtils;

public class TestUtils
{
    public static void main (String[] args) 
    {
        GlobalConfiguration.PLUGIN_ACTIVE = false;
        
        // Test GenericUtils.getPEPformatFromPEPfile (...)
        String fileLLNET = "/Users/marco/Desktop/asd.ll_net";
        
        try {
            GenericUtils.getPEPformatFromPEPfile (fileLLNET);
        } 
        catch (IOException e) {
            System.out.println (e.getClass ().getName () + ", message: " + e.getMessage ());
        }
        
        
        // Test GenericUtils.getElementsTrace (...)
        String trace = "'t1DefaultProcess_StartEvent_1:e0''t2DefaultProcess_StartEvent_1:e1''t1DefaultProcess_ExclusiveGateway_3:e2'" +
                       "'t3DefaultProcess_ExclusiveGateway_3:e4''t1DefaultProcess_Task_2:e7''t2DefaultProcess_Task_2:e11'";
        ArrayList<String> res = GenericUtils.getElementsTrace (trace);
        
        for (int k = 0, size = res.size (); k < size; k++)
            System.out.println ("Element " + (k + 1) + ": " + res.get (k));
        
        ArrayList<String> elements = new ArrayList<String> ();
        elements.add ("p1DefaultProcess_StartEvent_11");elements.add ("t1DefaultProcessStartEvent1");elements.add ("p2DefaultProcess_StartEvent_11");
        elements.add ("t2DefaultProcessStartEvent1");elements.add ("p1DefaultProcess_Task_11");elements.add ("t1DefaultProcessTask1");
        elements.add ("p2DefaultProcess_Task_11");elements.add ("t2DefaultProcessTask1");elements.add ("t1DefaultProcessTask2");
        
        ArrayList<String> trace_2 = new ArrayList<String> ();
        trace_2.add ("t1DefaultProcess_StartEvent_1");trace_2.add ("t2DefaultProcess_StartEvent_1");trace_2.add ("t1DefaultProcess_ExclusiveGateway_3");
        trace_2.add ("t3DefaultProcess_ExclusiveGateway_3");trace_2.add ("t1DefaultProcess_Task_2");trace_2.add ("t2DefaultProcess_Task_2");
        
        for (int k = 0, size = elements.size (); k < size; k++) {
            System.out.println (TestUtils.traceContainsElement (trace_2, elements.get (k)));
            
        }
        
        System.out.println ("\n\n"+"t1DefaultProcessTask2".equals("t1DefaultProcess_Task_2".replace("_", "")));
        
        
        System.exit (0);
    }
    
    private static boolean traceContainsElement (ArrayList<String> trace, String element)
    {
        for (int k = 0, size = trace.size (); k < size; k++)
            if (trace.get (k).matches (".*" + element + ".*") || trace.get (k).equals (element.replace ("_", "")))
                return true;
        
        return false;
    }
}

/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package utils;

import java.util.Random;

/**
 * In questa classe sono dichiarati metodi di utility inerenti all'utilizzo dei numeri.
 * 
 * @author Marco
 */
public class NumberUtils 
{
	/**
	 * Questo metodo controlla che una stringa contenga solo numeri.
	 * 
	 * @param s Stringa da controllare
	 * @return  "true" se la stringa contiene solo numeri, "false" altrimenti
	 */
	public static boolean isNumber (String s)
	{
		return s.matches ("[0-9]+");
	}
	
	
	/**
	 * Questo metodo genera un numero casuale in un certo range.
	 * 
	 * @param min Valore dell'estremo sinistro dell'intervallo
	 * @param max Valore dell'estremo destro dell'intervallo
	 * @return    Numero compreso tra [min, max]
	 */
	public static int generateInteger (int min, int max)
	{
		return min + new Random ().nextInt (max - min + 1);  
	}
}

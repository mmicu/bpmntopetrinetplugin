/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Locale;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.osgi.framework.Bundle;

import plugin.bpmntopetrinetplugin.view.BPMNtoPetriNetView;

/**
 * In questa classe sono dichiarati metodi di utility inerenti all'utilizzo del plugin.
 * 
 * @author Marco
 */
public class PluginUtils 
{
	/**
	 * Id del plugin
	 */
	public static final String PLUGIN_ID = "BPMNtoPetriNetPlugin"; //$NON-NLS-1$
	
	
	/**
	 * View del plugin
	 */
	public static final String PLUGIN_MAIN_VIEW = "BPMNtoPetriNetPlugin.BPMNtoPetriNetPluginView";
	
	
	/**
	 * Percorso del file "settings.properties"
	 */
	private static final String PATH_SETTINGS_FILE = "resources/properties/settings/settings.properties";
	
	
	
	/**
	 * Questo metodo viene utilizzato per ottenere un file interno al plugin.
	 * 
	 * @param pathFile            Path del file interno al plugin
	 * @return                    Istanza del file interno al plugin
	 * @throws IOException        Eccezione generata dal metodo toFileURL (...) e dal costruttore URL (...)
	 * @throws URISyntaxException Eccezione generata dal metodo toURI (...)
	 */
	public static IFileStore getIFileStoreFromPathFile (String pathFile) throws IOException, URISyntaxException
	{
		URL url     = new URL ("platform:/plugin/" + PluginUtils.PLUGIN_ID + "/" + pathFile); // IOException-->MalformedURLException
		URL fileURL = org.eclipse.core.runtime.FileLocator.toFileURL (url);                   // IOException
		
		return EFS.getLocalFileSystem ().getStore (fileURL.toURI ());                         // URISyntaxException
	}
	
	
	/**
	 * Questo metodo viene utilizzato per ottenere l'output stream di un file.
	 * 
	 * @param pathFile            Percorso del file
	 * @return                    Output stream del file
	 * @throws URISyntaxException Eccezione generata dal metodo toURI ()
	 * @throws IOException        Eccezione generata dal costruttore FileOutputStream (...)
	 */
	public static OutputStream getOutputStreamFromPathFile (String pathFile) throws URISyntaxException, IOException
	{
		Bundle bundle = Platform.getBundle (PluginUtils.PLUGIN_ID);
		URL fileURL   = bundle.getEntry (pathFile);
		    
		return new BufferedOutputStream (new FileOutputStream (new File (FileLocator.resolve (fileURL).toURI ())));
	}
	
	
	/**
	 * Questo metodo apre un un "File Browser". Esso viene utilizzato per chiedere all'utente dove voler salvare un certo file.
	 * 
	 * @param container       "Container" da cui ottenere la "Shell" per aprire il "File Browser"
	 * @param filePath        Una volta determinato il percorso del file, esso verra' memorizzato in questa variabile     
	 * @param fileExtensions  Estensioni consentite dal "File Browser"
	 * @param lang            "File" di lingua da utilizzare
	 * @return                Percorso del file
	 */
	@SuppressWarnings("static-access")
	public static String openFileBrowser (Composite container, String filePath, String[] fileExtensions, Locale lang)
	{
  		FileDialog dialog = new FileDialog (container.getDisplay ().getCurrent ().getActiveShell (), SWT.SAVE);
  		
  		if (filePath != null)
  			dialog.setFilterPath ((new File (filePath)).getParent ());
  		else {
  			String filterPath = "/";
  			String platform = SWT.getPlatform ();
  			
  			if (platform.equals ("win32") || platform.equals ("wpf"))
  				filterPath = "c:\\";
  			
  			dialog.setFilterPath (filterPath);
  		}
  		
  		dialog.setFilterExtensions (fileExtensions);
  		filePath = dialog.open ();
  		
  		if (filePath == null)
  			return null;
  		
  		
  		final File file = new File (filePath);
  		
  		if (file.exists ()) {
  			MessageDialog m = new MessageDialog (
  				container.getDisplay ().getDefault ().getActiveShell(),
  				GenericUtils._l (lang, "confirm_overwrite"),
				null,
				GenericUtils._l (lang, "the_file") + " '" + file.getName() + "' " + GenericUtils._l (lang, "overwrite_file"),
				MessageDialog.QUESTION, 
				new String[] {GenericUtils._l (lang, "no"), GenericUtils._l (lang, "yes")}, 
				1
  			);
  				
  			return (m.open () == 1) ? filePath : "";
  		}
  		
  		return filePath;
  	}
	
	
	/**
	 * Metodo che aggiunge messaggi alla view
	 * 
	 * @param level              "Livello" del messaggio" (info/warning/error) (prima colonna della view
	 * @param message            Messaggio da inserire nella seconda colonna della view
	 * @throws PartInitException Eccezione generata dalla prima istruzione del metodo
	 */
	public static void addTextToView (String level, String message) throws PartInitException
	{
		BPMNtoPetriNetView view = (BPMNtoPetriNetView) 
					PlatformUI.getWorkbench ().getActiveWorkbenchWindow ().getActivePage ().showView (PluginUtils.PLUGIN_MAIN_VIEW);
		
		view.addItem (level, message);
	}
}

/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package utils;

import java.util.ArrayList;

/**
 * In questa classe sono dichiarati metodi di utility inerenti all'utilizzo di ArrayList.
 * 
 * @author Marco
 */
public class ArrayListUtils 
{
	/**
	 * Questo metodo conta le occorrenze di una stringa in un ArrayList di tipo "String".
	 * 
	 * @param array ArrayList su cui contare le occorrenze
	 * @param s	    Stringa da cercare nell'ArrayList
	 * @return      Numero di occorrenze della stringa nell'ArrayList
	 */
	public static int countOccurrencesString (ArrayList<String> array, String s)
	{
		int counter = 0;
		
		for (int k = 0, size = array.size (); k < size; k++)
			if (array.get (k).equals (s))
				counter++;
		
		return counter;
	}
}

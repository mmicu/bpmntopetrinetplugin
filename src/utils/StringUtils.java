/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package utils;

/**
 * In questa classe sono dichiarati metodi di utility inerenti all'utilizzo delle stringhe.
 * 
 * @author Marco
 */
public class StringUtils 
{
	/**
	 * Questo metodo controlla che la stringa sia una "keyword". Una "keyword" comincia con il carattere '$'.
	 * Essa viene utilizzata nel file "rules.xml"
	 * 
	 * @param s Stringa da controllare
	 * @return  "true" se la stringa e' una "keyword", "false" altrimenti
	 */
	public static boolean isKeyword (String s)
	{
		return (s.length () > 0 && !StringUtils.isExpression (s) && s.charAt (0) == '$');
	}
	
	
	/**
	 * Questo metodo controlla che la stringa sia un'espressione. Le espressioni vengono utilizzate nel file "rules.xml".
	 * 
	 * @param s Stringa da controllare
	 * @return  "true" se la stringa e' un'espressione, "false" altrimenti
	 */
	public static boolean isExpression (String s)
	{
		if (StringUtils.isSet (s))
			return false;
		
		String[] tokens = { "(", ")", "+", "-", "*", "/" };
		
		
		s = StringUtils.__trim (s);
		for (int k = 0, size_s = s.length (); k < size_s; k++)
			for (int j = 0, size_t = tokens.length; j < size_t; j++)
				if (s.charAt (k) == tokens[j].charAt (0))
					return true;
		
		return false;
	}
	
	
	/**
	 * Questo metodo controlla che la stringa sia un'intervallo. Gli intervalli vengono utilizzate nel file "rules.xml".
	 * Esempio di intervallo: [1..2], [10..20] ecc.
	 * 
	 * @param s Stringa da controllare
	 * @return  "true" se la stringa e' un'espressione, "false" altrimenti
	 */
	public static boolean isSet (String s)
	{
		return (s.length () > 0 && s.charAt (0) == '[');
	}
	
	
	/**
	 * Questo metodo viene utilizzato per trasformare una stringa in un intero.
	 * 
	 * @param s Stringa da trasformare in intero
	 * @return  Numero intero corrispondente alla stringa
	 */
	public static int getIntByString (String s)
	{
		return Integer.parseInt (s);
	}
	
	
	/**
	 * Questo metodo viene utilizzato per eliminare tutti gli spazi in una stringa.
	 * Esempio: " ciao a   tutti   " --> "ciaoatutti"
	 * 
	 * @param s Stringa da cui eliminare gli spazi
	 * @return  Stringa senza spazi
	 */
	public static String __trim (String s)
	{
		return s.replace (" ", "");
	}
	
	
	/**
	 * Questo metodo controlla se almeno uno dei caratteri contenuti nell'array e' presente nella stringa
	 * 
	 * @param s      Stringa da controllare
	 * @param arrayS Array di caratteri
	 * @return       "true" se almeno uno dei caratteri contenuti nell'array e' presente nella stringa, "false" altrimenti
	 */
	public static boolean containsChar (String s, String[] arrayS)
	{
		for (int k = 0, size_s = s.length (); k < size_s; k++)
			for (int j = 0, size_array = arrayS.length; j < size_array; j++)
				if (s.charAt (k) == arrayS[j].charAt (0))
					return true;
		
		return false;
	}
	
	
	/**
	 * Metodo che trasforma una stringa secondo le convenzioni di dot:
     * An ID is one of the following (http://www.graphviz.org/doc/info/lang.html):
     *    - any string of alphabetic ([a-zA-Z\200-\377]) characters, underscores ('_') or digits ([0-9]), not beginning with a digit;
     *    - a numeral [-]?(.[0-9]+ | [0-9]+(.[0-9]*)? );
     *    - any double-quoted string ("...") possibly containing escaped quotes (\")1;
     *    - an HTML string (<...>).
     *
     *
	 * @param s Stringa
	 * @return  Stringa che rispetta le restrizioni di dot
	 */
	public static String getDOTString (String s)
	{
	    String res = "";
	    
	    for (int k = 0, size_s = s.length (); k < size_s; k++) {
	        if (k == 0 && s.charAt (k) >= '0' && s.charAt (k) <= '9')
	            continue;
	        
	        else if (k > 0 && !(s.charAt (k) >= '0' && s.charAt (k) <= '9') && !(s.charAt (k) >= 'a' && s.charAt (k) <= 'z') &&
	                 !(s.charAt (k) >= 'A' && s.charAt (k) <= 'Z') && s.charAt (k) != '_' && s.charAt (k) != '<' && s.charAt (k) != '>')
	            continue;
	            
	        else
	            res += s.charAt (k);
	    }
	    
	    return res;
	}
}

/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import mapping.transformation.Transformation;
import command.FormatCommand;
import command.Dot;
import mapping.data.structure.BPMNElement;
import mapping.data.structure.PEP;
import mapping.data.structure.PEP2DOT;
import mapping.data.structure.Pair;
import mapping.exceptions.ExceptionMapping;

/**
 * In questa classe sono dichiarati metodi generali di utility.
 * 
 * @author Marco
 */
public class GenericUtils 
{
    /**
     * Percorso del file "settings.properties"
     */
    private static final String PATH_SETTINGS_FILE = "resources/properties/settings/settings.properties";
    
    
	/**
	 * Questa funzione calcola il valore di un espressione.
	 * Esempio di espressione: (19 + 1) * 2
	 *                         1 + 5 - (2 * 2)
	 *                         1 + 1
	 *                         
	 * @param expression Espressione da calcolare
	 * @return           Valore restituito dall'espressione, -1 se l'espressione e' errata (ad esempio se inserisco lettere nell'espressione)
	 */
	public static int evaluateExpression (String expression)
	{
		ScriptEngineManager manager = new ScriptEngineManager ();
	    ScriptEngine engine = manager.getEngineByName ("js");
	    Object result = null;
	    String resultToS = null;
	    
	    try {
			result = engine.eval (expression);
			resultToS = result.toString ();
		} 
	    catch (ScriptException e) {
			return -1;
		}
	    
	    // L'espressione, anche se tra interi, restituisce un numero con la virgola. Quindi, dal risultato estraggo solo la parte intera.
	    // Esempio: 1 + 1, restituisce 2.0
	    return (resultToS.indexOf (".") != -1) ? Integer.parseInt ((String) resultToS.subSequence (0, resultToS.indexOf ("."))) : -1;
	}
	
	
	/**
	 * Metodo che sostituisce una keyword con il suo valore reale.
	 * Ad esempio se ho l'espressione (1 + $n_incoming) * 2 --> (1 + 1) * 2, supponendo che $n_incoming = 1
	 * 
	 * @param expression Espressione da cui cercare e sostituire le keyword
	 * @param b          Elementi BPMN utilizzati per calcolare il valore delle keyword (ad esempio il valore di $n_incoming)
	 * @param KEYWORD_N  Keyword possibili
	 * @return           Espressione di partenza a cui sostituiamo la keyword con il suo valore
	 */
	public static String replaceKeywordWithValue (String expression, BPMNElement b, String[] KEYWORD_N)
	{
		// KEYWORD_N = { "$n_incoming", "$n_outgoing" };
		String operators[] = { "(", ")", "+", "-", "*", "/"};
		boolean isKeyword = false, subKeyword = false;
		String keywordI = "", newExpression = "";
		
		expression = StringUtils.__trim (expression);
		
		for (int k = 0; k < expression.length (); k++) {
			if (expression.charAt (k) == '$') {
				keywordI = "$";
				isKeyword = true;
			}
			
			// Il carattere in esame e' uno di questi: { "(", ")", "+", "-", "*", "/"}
			else if (isKeyword && StringUtils.containsChar (expression.substring (k, k + 1), operators)) {
				subKeyword = true;
				if (keywordI.equals ("$n_incoming"))
					newExpression += b.getSequenceFlowIncoming ().size ();
				else if (keywordI.equals ("$n_outgoing"))
					newExpression += b.getSequenceFlowOutgoing ().size ();
				
				newExpression += expression.charAt (k);
			}
			
			// Il carattere in esame verra' concatenato alla keyword
			else if (isKeyword && !StringUtils.containsChar (expression.substring (k, k + 1), operators)) {
				// Nel caso in cui sto analizzando l'ultimo carattere in esame, salvo la keyword 
				if (k == (expression.length () - 1)) {
					subKeyword = true;
					keywordI += expression.charAt (k);
					
					if (keywordI.equals ("$n_incoming"))
						newExpression += b.getSequenceFlowIncoming ().size ();
					else if (keywordI.equals ("$n_outgoing"))
						newExpression += b.getSequenceFlowOutgoing ().size ();
					
					continue;
				}
	
				keywordI += expression.charAt (k);
			}
			else
				newExpression += expression.charAt (k);
			
			if (subKeyword) {
				keywordI   = "";
				isKeyword  = false;
				subKeyword = false;
			}
		}
			
		return newExpression;
	}
	
	
	/**
	 * Con questo metodo ottengo l'estremo inferiore e l'estremo superiore dell'intervallo.
	 * Esempio: set = "[$n_incoming + 1..12 + $n_outgoing]" --> return [$n_incoming + 1, 12 + $n_outgoing]
	 * 
	 * @param set Intervallo
	 * @return
	 */
	public static ArrayList<String> getIntervalsSet (String set)
	{
		ArrayList<String> intervals = new ArrayList<String> ();
		boolean concat = false;
		String app = "";
		
		set = StringUtils.__trim (set);
		
		for (int k = 0; k < set.length (); k++) {
			if (set.charAt (k) == '[') {
				concat = true;
				
				continue;
			}
			
			if (concat && set.charAt (k) != '.' && set.charAt (k) != ']')
				app += set.charAt (k);
			
			if ((set.charAt (k) == '.' && (k + 1) <= set.length () && set.charAt (k + 1) == '.') || set.charAt (k) == ']') {
				intervals.add (app);
				app = "";
			}
			
		}
		
		return intervals;
	}
	
	
	/**
	 * Valuto le "espressioni" dichiarate all'interno dei collegamenti PT e TP.
	 * Nel caso in cui ho un "set" [x..y] |result| = 2, altrimenti |result| = 1.
	 * x e y possono essere:
	 *   - numeri;
	 *   - keyword;
	 *   - espressioni contenenti numeri e keyword.
	 * 
	 * @param link
	 * @param b    ArrayList degli elementi BPMN (utilizzato per prelevare il valore delle keyword come $n_outgoing)
	 * @return     ArrayList che contiene i valori delle espressioni
	 */
	public static ArrayList<Integer> getRangeFromValueAttribute (String link, BPMNElement b)
	{
		ArrayList<Integer> result = new ArrayList<Integer> ();
		
		
		String keywordPlaceI = link;
		int nIterationPlaceInt = 0;
		int nIterationPlaceInt_2 = 0;
		
		
		// Keyword place e' un'espressione
		if (StringUtils.isExpression (keywordPlaceI)) {
			nIterationPlaceInt = GenericUtils.evaluateExpression (
				      				GenericUtils.replaceKeywordWithValue (keywordPlaceI, b, null)
								 );
		}
		
		// Keyword place e' un insieme
		else if (StringUtils.isSet (keywordPlaceI)) {
			//System.out.println (keywordPlaceI + " is set!");
			ArrayList<String> intervals = GenericUtils.getIntervalsSet (keywordPlaceI);
			
			//for (int j = 0; j < intervals.size (); j++)
				//System.out.println ("Intervals[" + j + "] = " + intervals.get (j));
			
			if (intervals.size () == 2) {
				if (NumberUtils.isNumber (intervals.get (0)))
					nIterationPlaceInt = StringUtils.getIntByString (intervals.get (0));
				
				if (NumberUtils.isNumber (intervals.get (1)))
					nIterationPlaceInt_2 = StringUtils.getIntByString (intervals.get (1));
				
				
				if (StringUtils.isExpression (intervals.get (0)))
					nIterationPlaceInt = GenericUtils.evaluateExpression (
		      				GenericUtils.replaceKeywordWithValue (intervals.get (0), b, null)
						 );
				
				if (StringUtils.isExpression (intervals.get (1)))
					nIterationPlaceInt_2 = GenericUtils.evaluateExpression (
		      				GenericUtils.replaceKeywordWithValue (intervals.get (1), b, null)
						 );
				
				if (StringUtils.isKeyword (intervals.get (0)))
					nIterationPlaceInt = GenericUtils.evaluateExpression (
		      				GenericUtils.replaceKeywordWithValue (intervals.get (0), b, null)
						 );
				
				if (StringUtils.isKeyword (intervals.get (1)))
					nIterationPlaceInt_2 = GenericUtils.evaluateExpression (
		      				GenericUtils.replaceKeywordWithValue (intervals.get (1), b, null)
						 );
			}
		}
		
		// Keyword place e' un numero
		else if (NumberUtils.isNumber (keywordPlaceI)) {
			nIterationPlaceInt = StringUtils.getIntByString (keywordPlaceI);
		}
		
		// Keyword place e' una keyword (variabile)
		else if (StringUtils.isKeyword (keywordPlaceI)) {
			nIterationPlaceInt = GenericUtils.evaluateExpression (
      				GenericUtils.replaceKeywordWithValue (keywordPlaceI, b, null)
				 );
		}
		
		// Decremento di 1 i range perche' gli array dei "Places" e delle "Transitions" partono da 0
		result.add (nIterationPlaceInt - 1);
		
		if (nIterationPlaceInt_2 != 0)
			result.add (nIterationPlaceInt_2 - 1);
		
		return result;
	}
	
	
	/**
	 * Metodo che genera il nome di una nuova piazza per la rete di Petri
	 * 
	 * @param prefix   Prefisso della piazza da creare
	 * @param petriNet Rete di Petri utilizzata per controllare che la piazza creata non sia presente
	 * @return         Nome della nuova piazza
	 */
	public static String generateNamePlaceForPetriNet (String prefix, PEP petriNet)
	{
		ArrayList<String> p = petriNet.getOnlyPlace ();
		int nIterations = 1000;
		
		for (int k = 0; k < nIterations; k++) {
			String app = prefix + NumberUtils.generateInteger (1, 10000);
			
			if (!p.contains (app))
				return app;
		}
		
		return null;
	}
	
	
	/**
	 * Metodo che genera il nome di una nuova transizione per la rete di Petri
	 * 
	 * @param prefix   Prefisso della transizione da creare
	 * @param petriNet Rete di Petri utilizzata per controllare che la transizione creata non sia presente
	 * @return         Nome della nuova transizione
	 */
	public static String generateNameTransitionForPetriNet (String prefix, PEP petriNet)
	{
		ArrayList<String> t = petriNet.getT ();
		int nIterations = 1000;
		
		for (int k = 0; k < nIterations; k++) {
			String app = prefix + NumberUtils.generateInteger (1, 10000);
			
			if (!t.contains (app))
				return app;
		}
		
		return null;
	}
	
	
	/**
	 * Applico il mapping
	 * 
	 * @param pathBPMN                       Percorso del file BPMN
	 * @param pathPetriNet                   Percorso del file della rete di Petri (formato .ll_net)
	 * @param pathPetriNetDOT                Percorso del file della rete di Petri (formato .dot)
	 * @param pathPetriNetSVG                Percorso del file della rete di Petri (formato .svg)
	 * @return                               Risultato del comando dot se viene esportata la rete di Petri nel formato svg, "null" altrimenti
	 * @throws IOException                   Eccezione generata dal metodo new Transformation (...).applyTransformation ()
	 * @throws ExceptionMapping              Eccezione generata dal metodo new Transformation (...).applyTransformation ()
	 * @throws ParserConfigurationException  Eccezione generata dal metodo new Transformation (...).applyTransformation ()
	 * @throws SAXException                  Eccezione generata dal metodo new Transformation (...).applyTransformation ()
	 * @throws InterruptedException          Eccezione generata dal metodo dot.execute (...)
	 */
	public static ArrayList<Integer> applyMapping (String pathBPMN, String pathPetriNet, String pathPetriNetDOT, String pathPetriNetSVG) throws IOException, ExceptionMapping, ParserConfigurationException, SAXException, InterruptedException
	{
		PEP petriNet = new Transformation (pathBPMN).applyTransformation (); // IOException, ExceptionMapping, ParserConfigurationException, SAXException
		ArrayList<Integer> res = new ArrayList<Integer> ();
		int exitValueDot = -2;
		
		FileUtils.writeIntoFile (pathPetriNet, petriNet.getStandardNotationWithRealNames ());
		
		if (pathPetriNetDOT != null && pathPetriNetSVG != null) {
			FileUtils.writeIntoFile (pathPetriNetDOT, PEP2DOT.getDotFormat (petriNet));
			FormatCommand d = Dot.execute (pathPetriNetDOT, pathPetriNetSVG); // InterruptedException
			
			exitValueDot = d.getExitValue ();
		}
		
		res.add (petriNet.getOnlyPlace ().size ());
        res.add (petriNet.getT ().size ());
        res.add (exitValueDot);
		
		return res;
	}
	
	
	/**
	 * Restituisce data/ora secondo il "dateFormat" passato come parametro
	 * 
	 * @param dateFormat Esempio: "dd.MM.yyyy ~ HH:mm:ss"
	 * @return
	 */
	public static String now (String dateFormat) 
	{
	    return new SimpleDateFormat (dateFormat).format (Calendar.getInstance ().getTime ());
	}
	
	
	/**
	 * Metodo che restituisce l'indice dell'elemento BPMN secondo l'id passato come parametro
	 * 
	 * @param b   ArrayList che contiene gli elementi BPMN
	 * @param id  Id da confrontare con l'id degli elementi BPMN dell'ArrayList
	 * @return    Indice dell'elemento BPMN se l'id corrisponde, "-1" altrimenti
	 */
	public static int getIndexBPMNElementById (final ArrayList<BPMNElement> b, String id)
	{
		int size_b = b.size ();
		
		for (int k = 0; k < size_b; k++)
			if (b.get (k).getId ().equals (id))
				return k;
		
		return -1;
	}
	
	
	/**
	 * Ottengo il valore di "answer" nel messaggio di output di Cna.
	 * Esempio di messaggio di output di Cna:
	 * answer          : YES, a deadlocked marking is reachable
	 * clauses         : 42
	 * event variables : 14
	 * reductions      : bin 4-tree
	 * sccs            : []
	 * trace           : 'tDefaultProcess_StartEvent_1:e0' 't1DefaultProcess_ExclusiveGateway_1:e1' 't3DefaultProcess_ExclusiveGateway_1:e3' 't1DefaultProcess_Task_4:e6' 't2DefaultProcess_Task_4:e9'
	 * variables       : 27
	 * 
	 * @param cnaMessage Messaggio di output di Cna
	 * @return           Valore di "answer" nel messaggio di output di Cna
	 */
	public static String getAnswerCNA (String cnaMessage)
	{
		String res     = "";
		boolean concat = false;
		
		for (int k = 0; k < cnaMessage.length (); k++) {
			if (cnaMessage.charAt (k) == ':') {
				concat = true;
				k++; // Salto lo spazio -> : <messaggio>
				continue;
			}
			
			if (cnaMessage.charAt (k) == '\n')
				break;
			
			if (concat)
				res += cnaMessage.charAt (k);
		}
		
		return res;
	}
	
	
	/**
	 * Ottengo il valore di "trace" nel messaggio di output di Cna.
	 * Esempio di messaggio di output di Cna:
	 * answer          : YES, a deadlocked marking is reachable
	 * clauses         : 42
	 * event variables : 14
	 * reductions      : bin 4-tree
	 * sccs            : []
	 * trace           : 'tDefaultProcess_StartEvent_1:e0' 't1DefaultProcess_ExclusiveGateway_1:e1' 't3DefaultProcess_ExclusiveGateway_1:e3' 't1DefaultProcess_Task_4:e6' 't2DefaultProcess_Task_4:e9'
	 * variables       : 27
	 * 
	 * @param cnaMessage Messaggio di output di Cna
	 * @return           Valore di "trace" nel messaggio di output di Cna
	 */
	public static String getTraceCNA (String cnaMessage)
	{
		String res     = "";
		boolean concat = false;
		String[] lines = cnaMessage.split ("\n");
		
		for (int k = 0; k < lines.length; k++) {
			for (int j = 0; j < lines[k].length (); j++) {
				if ((j + 5 < lines[k].length ()) && (lines[k].substring (j, j + 5).equals ("trace"))) {
					j += 4; // cnaMessage[k] = 't' quindi salto la substring 'race'
					concat = true;
					continue;
				}

				if (concat && lines[k].charAt (j) != ' ')
					res += (res.length () == 0 && lines[k].charAt (j) == ':') ? "" : lines[k].charAt (j);
			}
			
			if (res.length () > 0)
				break;
		}
		
		return res;
	}
	
	
	/**
	 * Ottengo l'oggetto PEP da un file codificato nel formato PEP
	 * 
	 * @param file          File PEP (ll_net)
	 * @return              Oggetto PEP
	 * @throws IOException  Eccezione generata da new BufferedReader (...)
	 */
	public static PEP getPEPformatFromPEPfile (String file) throws IOException
	{
	    PEP petriNet = new PEP ();
	    boolean placeBlock, transitionsBlock, tpBlock, ptBlock;
	    
	    // Pattern piazza/transizione (PL/TR)
	    String patternPlaceTransition = "([0-9]+)\"(.+)\""; // Per estrarre l'id e nome della piazza e della transizione
	    Pattern rPlaceTransition = Pattern.compile (patternPlaceTransition);
	    
	    // Pattern collegamento (TP)
	    String patternTP = "([0-9]+)<([0-9]+)";
        Pattern rTP = Pattern.compile (patternTP);
        
        // Pattern collegamento (PT)
        String patternPT = "([0-9]+)>([0-9]+)";
        Pattern rPT = Pattern.compile (patternPT);
        
        // Traccio id elemento/nome elemento. Esempio 1"p1DefaultProcess_StartEvent, id = 1, nome = p1DefaultProcess_StartEvent
        ArrayList<Pair<Integer, String>> id_name = new ArrayList<Pair<Integer, String>> ();
	    
	    placeBlock = transitionsBlock = tpBlock = ptBlock = false;
	    
	    BufferedReader in = new BufferedReader (new FileReader (file));

	    while (in.ready ()) {
            String s = in.readLine ();
            
            //System.out.println (s);
            
            switch (s) {
            case "PL":
                placeBlock = true;
                transitionsBlock = tpBlock = ptBlock = false;
                break;
            case "TR":
                transitionsBlock = true;
                placeBlock = tpBlock = ptBlock = false;
                break;
            case "TP":
                tpBlock = true;
                placeBlock = transitionsBlock = ptBlock = false;
                break;
            case "PT":
                ptBlock = true;
                placeBlock = transitionsBlock = tpBlock = false;
                break;
            default:
                // Sono all'interno del blocco "PL"
                if (placeBlock) {
                    Matcher m = rPlaceTransition.matcher (s);
                    
                    // Esempio di s --> 1"p1DefaultProcess_StartEvent_1/1"9@9M1
                    if (m.find ()) {
                        String id = m.group (2).replace ("/", "").replace ("*", "");
                        
                        petriNet.addP (id, s.matches (".*M1.*"));
                        
                        id_name.add (Pair.pair (new Integer (m.group (1)), id));
                    }
                }
                // Sono all'interno del blocco "TR"
                else if (transitionsBlock) {
                    Matcher m = rPlaceTransition.matcher (s);
                    
                    // Esempio di s --> 8"t1DefaultProcess_StartEvent_1"9@9
                    if (m.find ()) {
                        String id = m.group (2).replace ("/", "").replace ("*", "");
                        
                        petriNet.addT (id);
                        
                        id_name.add (Pair.pair (new Integer (m.group (1)), id));
                    }
                }
                // Sono all'interno del blocco "TP" o "PT
                else if (tpBlock || ptBlock) {
                    Matcher m = (tpBlock) ? rTP.matcher (s) : rPT.matcher (s);
                    
                    // Esempio di s --> 1"p1DefaultProcess_StartEvent_1/1"9@9M1
                    if (m.find ()) {
                        int idTransition = new Integer ((tpBlock) ? m.group (1) : m.group (2));
                        int idPlace      = new Integer ((tpBlock) ? m.group (2) : m.group (1));
                        
                        String transition = null, place = null;
                        
                        for (int k = 0, size = id_name.size (); k < size; k++) {
                            if (id_name.get (k).getFirstElement () == idTransition)
                                transition = id_name.get (k).getSecondElement ();
                            if (id_name.get (k).getFirstElement () == idPlace)
                                place = id_name.get (k).getSecondElement ();
                            
                            if (transition != null && place != null)
                                break;
                        }
                        
                        if (transition != null && place != null) {
                            if (tpBlock)
                                petriNet.addTP (transition, place);
                            else
                                petriNet.addPT (place, transition);
                        }
                    }
                }
            }
	    }
	    
	    in.close ();
	    
	    return petriNet;
	}
	
	
	/**
	 * Ottengo gli elementi dal trace di deadlock. 
	 * Esempio di trace: 't1DefaultProcess_StartEvent_1:e0''t2DefaultProcess_StartEvent_1:e1''t1DefaultProcess_Task_1:e2''t2DefaultProcess_Task_1:e3'
	 * 
	 * @param trace Trace di deadlock
	 * @return      Elementi contenuti nel trace
	 */
	public static ArrayList<String> getElementsTrace (String trace)
	{
	    ArrayList<String> res = new ArrayList<String> ();
	    boolean concat = false, colon = false;
	    String app = "";
	    
	    for (int k = 0, size = trace.length (); k < size; k++) {
	        if (trace.charAt (k) == '\'') {
	            concat = concat ? false : true;
	            colon  = false;
	            
	            continue;
	        }
	        
	        if (concat) {
	            if (trace.charAt (k) != ':') {
	                if (!colon)
	                    app += trace.charAt (k);
	            }
	            else {
	                res.add (app);
	                app = "";
	                colon = true;
	            }
	        }
	        
	    }
	    
	    return res;
	}
	
	
	public static boolean isADOmodel (InputStream in) throws ParserConfigurationException, SAXException, IOException
	{
	    class BreakParsingException extends SAXException 
	    {
            private static final long serialVersionUID = 1L;
	    }
	    
	    SAXParserFactory factory = SAXParserFactory.newInstance ();
        SAXParser saxParser = factory.newSAXParser ();
	    
	    DefaultHandler handler = new DefaultHandler () 
        {
            public void startElement (String uri, String localName, String qName, Attributes attributes) throws SAXException 
            {
                if (qName.equals ("adonis:modelstate") || qName.equals ("adonis:ATTRIBUTE") || qName.equals ("adonis:modelversion"))
                    throw new SAXException (new BreakParsingException ());
            }
            
            
            public void endElement (String uri, String localName, String qName) throws SAXException 
            {
                if (qName.equals ("adonis:modelstate") || qName.equals ("adonis:ATTRIBUTE") || qName.equals ("adonis:modelversion"))
                    throw new SAXException (new BreakParsingException ());
            }
        };
        
        try {
            saxParser.parse (in, handler);
        }
        catch (SAXException e) {
            return (e instanceof BreakParsingException) ? true : false;
        }
        
        return false;
	}
	
	
	/**
     * Questo metodo viene utilizzato per ottenere il valore di una "proprieta'".
     * Esempio: key=value, voglio ottenere il valore "value" dando in input a questo metodo il valore "key" (oltre al percorso del file).
     * 
     * @param pathFile     Percorso del file "properties"
     * @param key          Valore "proprieta'"
     * @return             Valore della "proprieta'"
     * @throws IOException Eccezione generata dal metodo load ()
     */
    public static String getValueProperty (String pathFile, String key) throws IOException
    {
        InputStream input = FileUtils.getInputStreamFromPathFile (pathFile);
        Properties prop = new Properties ();
        
        prop.load (input); // IOException
        
        //input.close ();
        
        return prop.getProperty (key);
    }
    
    
    /**
     * Questo metodo viene utilizzato per ottenere il valore della "proprieta' language" all'interno del file "settings.properties".
     * 
     * @return             Valore della "proprieta' language" all'interno del file "settings.properties"
     * @throws IOException Dovuta al metodo getValueProperty (...)
     */
    public static String getLanguage () throws IOException
    {
        return GenericUtils.getValueProperty (GenericUtils.PATH_SETTINGS_FILE, "language");
    }
    
    
    /**
     * Questo metodo viene utilizzato come "shortcut" per ottenere un messaggio in una delle lingue del plugin.
     * 
     * @param lang   "File" di lingua da utilizzare
     * @param value  Valore della "key"
     * @return       Messaggio contenuto nella "key"
     */
    public static String _l (Locale lang, String value)
    {
        return ResourceBundle.getBundle ("lang", lang).getString (value);
    }
    
    
    /**
     * Metodo che controlla l'esistenza dell'eseguibile di cunf
     * 
     * @return "true" se l'eseguibile di cunf esiste, "false" altrimenti
     * @throws IOException Eccezione generata dal metodo GenericUtils.getValueProperty (...)
     */
    public static boolean existsPathCunf () throws IOException
    {
        return FileUtils.fileExists (GenericUtils.getValueProperty (GenericUtils.PATH_SETTINGS_FILE, "path_cunf"));
    }
    
    
    /**
     * Metodo che controlla l'esistenza dell'eseguibile di dot
     * 
     * @return "true" se l'eseguibile di dot esiste, "false" altrimenti
     * @throws IOException Eccezione generata dal metodo GenericUtils.getValueProperty (...)
     */
    public static boolean existsPathDot () throws IOException
    {
        return FileUtils.fileExists (GenericUtils.getValueProperty (GenericUtils.PATH_SETTINGS_FILE, "path_dot"));
    }
    
    
    /**
     * Metodo che controlla l'esistenza dell'eseguibile di cna
     * 
     * @return "true" se l'eseguibile di cna esiste, "false" altrimenti
     * @throws IOException Eccezione generata dal metodo GenericUtils.getValueProperty (...)
     */
    public static boolean existsPathCna () throws IOException
    {
        return FileUtils.fileExists (GenericUtils.getValueProperty (GenericUtils.PATH_SETTINGS_FILE, "path_cna"));
    }
    
    
    /**
     * Metodo che controlla l'esistenza del percorso utilizzato per i file temporanei
     * 
     * @return "true" se il percorso utilizzato per i file temporanei esiste, "false" altrimenti
     * @throws IOException Eccezione generata dal metodo GenericUtils.getValueProperty (...)
     */
    public static boolean existsPathTempFile () throws IOException
    {
        return FileUtils.pathExists (GenericUtils.getValueProperty (GenericUtils.PATH_SETTINGS_FILE, "path_temp_file"));
    }
    
    
    /**
     * Metodo che controlla che tutte le impostazioni siano corrette. In questo caso controlliamo l'esistenza
     * degli eseguibili e il percorso utilizzato per i file temporanei
     * 
     * @return "true" se le impostazioni sono corrette, "false" altrimenti
     * @throws IOException Eccezione generata dai metodi exists*
     */
    public static boolean correctSettings () throws IOException
    {
        return GenericUtils.existsPathCunf () && GenericUtils.existsPathCna () && 
               GenericUtils.existsPathDot () && GenericUtils.existsPathTempFile ();
    }
}

/*
 * BPMNtoPetriNet
 * Copyright (c) 2014, Marco, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import config.GlobalConfiguration;

/**
 * In questa classe sono dichiarati metodi di utility inerenti all'utilizzo dei file.
 * 
 * @author Marco
 */
public class FileUtils 
{
	/**
	 * Metodo utilizzato per scrivere su file.
	 * 
	 * @param pathFile      File su cui scrivere
	 * @param content       Contenuto da scrivere nel file
	 * @throws IOException  Eccezione generata dal metodo "createNewFile ()", write (...) e close ().
	 *                      Oltre al costruttore FileWriter (...)
	 */
	public static void writeIntoFile (String pathFile, String content) throws IOException
	{
		File f = new File (pathFile);
			
		if (!f.exists ())
			f.createNewFile ();
			
		BufferedWriter bw = new BufferedWriter (new FileWriter (f.getAbsoluteFile ()));
		bw.write (content);
		bw.close ();
	}
	
	
	/**
	 * Metodo utilizzato per ottenere il nome di un file dal suo percorso. 
	 * Esempio: "/Users/lol/a.txt" --> "a.txt"
	 * 
	 * @param pathFile Path del file da cui ricavare il nome del file
	 * @return         Nome del file se esso esiste, null altrimenti
	 */
	public static String getFileNameByPath (String pathFile) 
	{
		File f = new File (pathFile);
		
		return (!f.exists ()) ? null : f.getName ();
	}
	
	
	/**
	 * Metodo utilizzato per ottenere l'intero percorso di un file. 
	 * Esempio: "/Users/lol/a.txt" --> "/Users/lol"
	 * 
	 * @param pathFile Path del file da cui ricavare l'intero percorso
	 * @return         Path del file
	 */
	public static String getPathFileByFilePath (String pathFile)
	{
		File file           = new File (pathFile);
		String absolutePath = file.getAbsolutePath ();
		
		return absolutePath.substring (0, absolutePath.lastIndexOf (File.separator));
	}
	
	
	/**
	 * Metodo utilizzato per ottenere l'estensione di un file.
	 * Esempio "ciao.txt" --> "txt"
	 * 
	 * @param file Nome del file di cui vogliamo ricavare l'estensione
	 * @return     Estensione del file
	 */
	public static String getExtension (String file)
	{
		int k = file.lastIndexOf ('.');

		return (k >= 0) ? file.substring (k + 1) : "";
	}
	
	
	/**
	 * Metodo utilizzato per ricavare tutti i file (con una certa estensione) di una directory.
	 * 
	 * @param pathDir    Directory su cui cercare i file
	 * @param extension  Estensione che i file devono rispettare (Esempio: extension="txt" se voglio cercare file del tipo "*.txt"
	 * @return           File contenuti nella directory
	 */
	public static ArrayList<File> getAllFileByDirName (String pathDir, String extension) 
	{
		ArrayList<File> files = null;
	    File dir = new File (pathDir);
	    File[] list = dir.listFiles ();
	    
	    if (list != null && list.length > 0) {
	    	files = new ArrayList<File> ();
		    for (File f : list) {
		    	if ((f.isFile ()) && (FileUtils.getExtension (f.getName ()).toLowerCase().equals (extension.toLowerCase ())))
		            files.add (f);
		    	else if (f.isDirectory ())
		    		getAllFileByDirName (f.getAbsolutePath (), extension);
		    }
	    }
	    
	    return files;
	}
	
	
	/**
	 * Metodo utilizzato per ricavare tutti i file (con una o piu' estensioni) di una directory.
	 * 
	 * @param pathDir     Directory su cui cercare i file
	 * @param extensions  Estensioni che i file devono rispettare (Esempio: extension="txt" se voglio cercare file del tipo "*.txt")
	 * @return            File contenuti nella directory
	 */
	public static ArrayList<File> getAllFileByDirName (String pathDir, String[] extensions) 
    {
        ArrayList<File> res = new ArrayList<File> ();
        
        for (int k = 0, size_e = extensions.length; k < size_e; k++) {
            ArrayList<File> app = FileUtils.getAllFileByDirName (pathDir, extensions[k]);
            
            for (int j = 0, size_app = app.size (); j < size_app; j++)
                res.add (app.get (j));
        }
        
        return res;
    }
	
	
	/**
	 * Metodo utilizzato per eliminare il contenuto di una directory.
	 * 
	 * @param dir Percorso della cartella su cui andremo a cancellare i file
	 */
	public static void deleteContentDir (File pathDir)
	{
		File[] files = pathDir.listFiles ();
		
	    if (files != null) {
	        for (File f : files) {
	            if (f.isDirectory ())
	                FileUtils.deleteContentDir (f);
	            else
	                f.delete ();
	        }
	    }
	}
	
	
	/**
	 * Metodo che controlla se un file esiste o meno.
	 * 
	 * @param pathFile Percorso del file da controllare
	 * @return         "true" se il file esiste, "false" altrimenti
	 */
	public static boolean fileExists (String pathFile)
	{
		return new File (pathFile).exists () && new File (pathFile).isFile ();
	}
	
	
	/**
	 * Metodo che controlla l'esistenza di un percorso.
	 * 
	 * @param path Percorso da controllare
	 * @return     "true" se il percorso esiste, "false" altrimenti 
	 */
	public static boolean pathExists (String path)
	{
		return new File (path).exists () && new File (path).isDirectory ();
	}
	
	
	/**
     * Questo metodo viene utilizzato per ottenere l'input stream di un file.
     * 
     * @param pathFile     Percorso del file
     * @return             Input stream del file
     * @throws IOException Eccezione generata dal metodo getInputStream (...) e dal costruttore FileInputStream (...)
     */
    public static InputStream getInputStreamFromPathFile (String pathFile) throws IOException
    {
        return FileUtils.getInputStreamFromPathFile (pathFile, false);
    }
    
    
    /**
     * Questo metodo viene utilizzato per ottenere l'input stream di un file.
     * 
     * @param pathFile     Percorso del file
     * @param isBPMN       Indica se voglio ottenere l'input stream del file BPMN. Un file BPMN non e' dichiarato localmente
     *                     al plugin, quindi posso ottenere l'input stream del file utilizzando il costruttore FileInputStream (...).
     * @return             Input stream del file
     * @throws IOException Eccezione generata dal metodo getInputStream (...) e dal costruttore FileInputStream (...)
     */
    public static InputStream getInputStreamFromPathFile (String pathFile, boolean isBPMN) throws IOException
    {
        if (isBPMN)
            return new FileInputStream (new File (pathFile));
        
        return (GlobalConfiguration.PLUGIN_ACTIVE) ? new URL ("platform:/plugin/BPMNtoPetriNetPlugin/" + pathFile).openConnection ().getInputStream ()
                                           : new FileInputStream (new File (pathFile));
    }
}
